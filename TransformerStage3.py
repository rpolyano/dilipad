from __future__ import division
from Util import Util
import codecs
import os
import re
import math
from operator import itemgetter
from lxml import etree as Etree
from xml.sax.saxutils import escape
import traceback
import sys
from datetime import datetime as dt
from datetime import timedelta
from collections import Counter
from operator import itemgetter

class TransformerStage3(object):
    """
    Add additional information to make the file valid with respect to the PM schema 
    Adds "meta" and "docinfo" elements -> self.add_meta() and self.docinfo()
    Also most of the namespace-handling is taken care of in this class. -> self.set_namespace()
    It performs some last modifications of the structure and the attributes.
    """
    CONST_NAMESPACES = {"pm":"http://www.politicalmashup.nl",\
            "dc":"http://purl.org/dc/elements/1.1/",\
            "xsd":"http://www.w3.org/2001/XMLSchema-datatypes",\
            "owl":"http://www.w3.org/2002/07/owl#",\
            "pmd":"http://www.politicalmashup.nl/docinfo",\
            "rdf":"http://www.w3.org/1999/02/22-rdf-syntax-ns#",\
            "html":"http://www.w3.org/1999/xhtml",\
            "dcterms":"http://purl.org/dc/terms/",\
            "xsi":"http://www.w3.org/2001/XMLSchema-instance",\
            "pmx":"http://www.politicalmashup.nl/extra",\
            "openpx":"https://openparliament.ca/extra",\
            "dp":"http://dilipad.history.ac.uk"}

    DP_ATTRIBUTES = ["time", "question-num", "question-author"]
    DP_TAGS = ["table", "tr", "td", "subtopic" ]

    def __init__(self,f,directory,output_directory):
        parser = Etree.XMLParser(remove_blank_text=True)
        self.tree = Etree.parse(os.path.join(directory,f),parser)
        ##self.root_proceedings = Etree.Element("proceedings")
        self.output = codecs.open(os.path.join(output_directory,f),"w",encoding="utf-8")
        self.f = f
        # -> set namesaces
        self.root = Etree.Element("root",nsmap=TransformerStage3.CONST_NAMESPACES)

    def transform(self):
        """
        Main function. 
        """
        self.add_docinfo()
        self.add_meta()
        self.filter_outside_topic()
        self.rest_functions()
        self.arrange_topic_hierarchy()
        self.add_required_attributes()
        self.arrange_votes()
        self.add_ids()
        self.fix_empty_stage_dirs()
        self.set_namespace()
        self.output.write(Etree.tostring(self.root, pretty_print=True,method="xml",encoding="unicode"))

    def set(self,element,name,value,ns):
        """
        Set attribute.
        """
        return element.set("{%s}%s"%(TransformerStage3.CONST_NAMESPACES[ns],name),value)

    def add_ids(self):
        """
        Set the pm:id attributes according to the pm format.
        Start with the highest level and change ids recursively -> see self.recursive_ids().
        """
        dateid = str(Util.convert_date_filename(self.f)).split()[0] # R -> We are using the file name, which is a human readable date as an id.
        print dateid
        start = self.tree.xpath("/proceedings")[0]
        start.set("{http://www.politicalmashup.nl}id","ca.proc.d.%s" % dateid) # FIXME: Is full namespace necessary?
        self.recursive_ids(start,"ca.proc.d.%s" % dateid)

    def recursive_ids(self,start,begin_tag=""):
        """
        Recursively adds id attributes which show the place of an elements in the tree.
        """
        children = start.getchildren()
        for i,child in enumerate(children):
            child.set("id",begin_tag + "." + str(i + 1)) # R -> every tag has an id which is just its index in the (conversationally
                                                            # ordered) tree
            if not len(child.getchildren()) == 0:
                # -> stop if there are no children elements left
                self.recursive_ids(child,begin_tag=begin_tag + ".%s" % (i + 1))

    def rest_functions(self):
        """
        Removes text from stage-direction and puts it in a p tag 
        """
        stagedirections = self.tree.xpath('.//stage-direction')
        for st in stagedirections:
            try:
                if not len(st.text) == 0: 
                    #print st.text
                    p = Etree.SubElement(st,"p")
                    p.text = st.text
                    st.text = None
            except:
                pass
    def filter_outside_topic(self):
        """
        Delete all elements which aren't nested in one of the topic elements.
        We should be carefull with this, and probably change this function later.
        At the moment I am not sure if we are not losing important information by simply deleting everything outside the topic element.
        """
        proceedings = self.tree.xpath("/proceedings")[0]
        children = proceedings.getchildren()
        for child in children:
            if not child.tag == "topic":
                child.getparent().remove(child)

    def make_element(self,parent,name,ns=""): #TODO: Use this
        """
        Make an element and assign it a place in the tree.
        Takes care of namespaces.
        """
        relevant_ns = None
        if ns == "":
            tag = name
        else:
            tag = "{%s}%s" % (TransformerStage3.CONST_NAMESPACES[ns],name)
            relevant_ns = {ns:TransformerStage3.CONST_NAMESPACES[ns]}

        return Etree.SubElement(parent, tag, nsmap = relevant_ns)

    def make_source(self,source,url,linktype):
        """
        Make source element as part of "meta" element.
        See "add_meta" method below.
        """
        subsource = self.make_element(source,"source","dc")
        self.set(subsource,"used-source","true","pm")
        self.create_link(subsource,url=url,linktype=linktype,source=url)

    def create_link(self,links,url="",linktype="",source="",description=""):
        """
        Make link for docinfo.
        See "add_meta" method below.
        Not too important at the moment.
        """
        link = self.make_element(links,"link","pm")
        if not linktype == "": self.set(link,"linktype",linktype,"pm")
        if not source == "": self.set(link,"source",source,"pm")
        if not description == "": self.set(link,"description",description,"pm")
        if "MP profil" in description:
            try:
                opener = urllib2.build_opener(urllib2.HTTPRedirectHandler)
                request = opener.open(url)
                url = request.url
            except Exception:
                pass
        link.text = url

    def add_docinfo(self):
        """
        Make docinfo element.
        """
        docinfo = self.make_element(self.root,"docinfo","pmd")
        comment = self.make_element(docinfo,"comment","pm")
        comment.text = "Data is valid with respect to the Relax NG schema http://schema.politicalmashup.nl/memberX.html. This is the open version of http://schema.politicalmashup.nl/member.html"

    def add_meta(self):
        """
        Add all required information to the meta element.
        """
        meta = Etree.SubElement(self.root,"meta")
        ##self.set(meta,"id",self.key+".meta","pm") # TO DO: change later
        dateid = str(Util.convert_date_filename(self.f)).split()[0]

        self.set(meta,"id","ca.proc.d.%s" % dateid,"pm") # TO DO: change later
        identitifier = self.make_element(meta,"identifier","dc")
        identitifier.text = ""
        format = self.make_element(meta,"format","dc")
        format.text = "text/xml"
        Type = self.make_element(meta,"type","dc")
        Type.text = "Members"
        contributor = self.make_element(meta,"contributor","dc")
        contributor.text = "http://www.politicalmashup.nl"
        coverage = self.make_element(meta,"coverage","dc")
        country = self.make_element(coverage,"country")
        self.set(country,"ISO3166-1","CA","dcterms")
        country.text = "Canada"
        creator = self.make_element(meta,"creator","dc")
        creator.text = "http://dilipad.history.ac.uk"
        language = self.make_element(meta,"language","dc")
        plang = self.make_element(language,"language","pm")
        self.set(plang,"ISO639-2","eng","dcterms")
        plang.text = "English"
        publisher = self.make_element(meta,"publisher","dc")
        publisher.text = "http://www.parl.gc.ca/parlinfo/"
        rights = self.make_element(meta,"rights","dc")
        rights.text = "http://www.parl.gc.ca/parlinfo/"
        date = self.make_element(meta,"date","dc")
        date.text = dt.isoformat(dt.now()).split('T')[0] + "+01:00"
        title = self.make_element(meta,"title","dc")
        title.text = "Canadian House of Commons Proceedings"
        description = self.make_element(meta,"description","dc")
        description.text = "Canadian House of Commons Proceedings"
        source = self.make_element(meta,"source","dc")
        ##self.make_source(source,"http://www.politicalmashup.nl","trusted")
        self.make_source(source,"http://www.parl.gc.ca/parlinfo","trusted")
        self.make_source(source,"na","trusted")
        self.make_element(meta,"subject","dc")
        relation = self.make_element(meta,"relation","dc")
        relation.text = "All pm:party-ref attributes appearing in http://www.politicalmashup.nl documents refer to http://resolver.politicalmashup.nl/[pm:party-ref] ."

    def set_namespace(self):
        """
        Set namespaces for all the elements, except for the "subtopic" element, which isn't part of the PM schema.
        I created the "subtopic" elements to be able to replicate the original structure of the proceedings.
        The "subtopic" elements belongs to the Dilipad namespace.
        """ 
        proceedings = self.tree.xpath("/proceedings")[0]
        proceedings.tag = Etree.QName("http://www.politicalmashup.nl",proceedings.tag)
        elements = proceedings.xpath('.//*')
        ##self.set(proceedings,"id","ca.p.0","pm")
        for e in elements:
            if e.tag in self.DP_TAGS:
                e.tag = Etree.QName("http://dilipad.history.ac.uk",e.tag)
            else:
                e.tag = Etree.QName("http://www.politicalmashup.nl",e.tag)
        ##self.set(e,"id","ca.p.0","pm")
            for a in e.attrib.keys():
                a_value = e.get(a)
                ns = "{http://www.politicalmashup.nl}"
                if a in TransformerStage3.DP_ATTRIBUTES:
                    ns = "{http://dilipad.history.ac.uk}"
                e.set(ns + a,a_value)
                e.attrib.pop(a)
        ##e.set('{http://www.politicalmashup.nl}%s'%"id","ca.p.0")
        self.root.append(proceedings)

    def arrange_topic_hierarchy(self):
        """
        Code all topical elements between the main "topic" and the lowest "scene" (the scene elements without nested scences) as subtopic.
        """
        scenes = self.tree.xpath('.//scene')
        for scene in scenes:
            if scene.find('scene') != None:
                scene.tag = "subtopic"
                scene.set('type',"topic")

    def arrange_votes(self):
        vote_elements = self.tree.xpath('.//vote_element')
        for v in vote_elements:
            #parent = v.getparent()
            #position = parent.index(v)
            v.tag = "stage-direction"
            v.set("type","vote")
            title = v.xpath('vote/@title')[0]
            old = v.xpath('vote')[0]
            old.getparent().remove(old)
    def fix_empty_stage_dirs(self):
        """
        Removes stage directions without content
        """
        empties = self.tree.xpath('.//stage-direction[not(p)]')
        for empty in empties:
            if empty.text == None or len(empty.text) == 0:
                empty.getparent().remove(empty)

    def add_required_attributes(self):
        """
        Make sure all elements have the required attributes.
        """
        scenes = self.tree.xpath('.//scene')
        for scene in scenes:
            scene.set('type','topic')