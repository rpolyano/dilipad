from __future__ import division
from Util import Util
import codecs
import os
import re
import math
from operator import itemgetter
from lxml import etree as Etree
from xml.sax.saxutils import escape
import traceback
import sys
from datetime import datetime as dt
from datetime import timedelta
from collections import Counter
from operator import itemgetter

class TransformerStage2(object):
    CONST_TITLES = [u"Mr.",u"Miss",u"Hon.",u"The Hon.",u"The hon.",u"Mrs.",u"The Right Hon.",u"The right Hon.",u"Right Hon.","Ms."] #FIXME: Never used?

    """
    Second stage in the markup process. The first class provided basic markup which should be well-formed and readable by the LXML Etree.parse.
    Here we add information and transform basic XML to a structure closely resembling the PM schema (but not yet valid with respect to the PM schema).
    """
    def __init__(self,f,directory,output_directory):
        """
        Read and initialize document.
        """
        parser = Etree.XMLParser(remove_blank_text=True)
        self.tree = Etree.parse(os.path.join(directory,f),parser)
        #self.mp_ns= "http://www.politicalmashup.nl"
        #self.root = Etree.Element('{%s}proceedings' % (self.mp_ns), nsmap={None: self.mp_ns})
        #self.root = Etree.Element("proceedings",nsmap=self.nsmap)
        self.output = codecs.open(os.path.join(output_directory,f),"w",encoding="utf-8")

    def change_element_name(self,element="",new_name="",delete_tags=False,element_to_delete=""):
        """
        This method redefines the tag.name of certain elements by querying the XML tree and changing the element.name.
        Method is used in "transform" method below.
        Use "delete_tags" to delete other elements nested in the main "element" (Only used once, not sure if it's useful to keep this option)
        """
        query = ".//{}".format(element)
        elements = self.tree.xpath(query)
        for el in elements: #TODO: Clean this
          if delete_tags == True:
            # -> this tweak was used to handle intervention elements, maybe we shoud fin a better and more general solution?
            Etree.strip_tags(el,element_to_delete)
            p = Etree.SubElement(el,"p")
            p.text = el.text
            el.text = ""
          el.tag = new_name
    def make_attributes(self,element_type,element_selected,attribute_name,normalisation=""):
        """
        Adds attributes to a node.
        This mainly is mainly used to set elements such "speaker_entity" as the attribute (pm:speaker) of the "speech" element (just one example).
        It takes multiple arguments: first the element ("element_type") to which the attribute should be added.
        Secondly is queries for a subelement ("element_selected"), adds it to to the parent as an attribute value for the attribute with name "attribute_name".
        "Normalisation" refers to the method "normalize_text" and changes or cleans the string to specfic format (see below: normalize_text). 

        """
        query_1 = ".//{}".format(element_type)
        query_2 = ".//{}".format(element_selected)
        elements = self.tree.xpath(query_1)
        for el in elements:
            try:
                el_sel = el.xpath(query_2)
                if len(el_sel) == 0:
                    continue
                el_sel = el_sel[0]
                el_sel.text = Util.normalize_text(el_sel.text,normalisation)
                el.set(attribute_name,el_sel.text)
                el.remove(el_sel)
            except Exception,err:
                pass
                #print traceback.format_exc()


    def make_attribute(self,element_type="",attribute_name="",tag_name=""): #TODO: Rename this
        """
        Replaces all <element_type> with <tag_name attribute_name="<element_type>.text"> i.e. renames element_type to tag_name and
        sets its text as an attribute called attribute_name
        """ 
        query_1 = ".//{}".format(element_type)
        elements = self.tree.xpath(query_1)
        for el in elements:
            try:
                el_new = Etree.Element(tag_name)
                # -> make new element
                el_new.set(attribute_name,el.text.strip()) 
                # -> add attribute to new element
                el.getparent().replace(el,el_new)
            except Exception  as err:
              #print err
              pass
              #print traceback.format_exc()

    def add_metadata(self):
        """
        Restructure data using the above "_make attribute" methods.
        Is used in the "transform" method below.
        """
        ##self._make_attributes("speech","speaker_entity","speaker",normalisation="name")
        self.make_attributes("speech","speaker_entity","speaker") # R -> <speech><speaker_entity>...</speaker_entity></speech> becomes <speech
                                                                   # speaker="..."></speech>
        self.make_attributes("speech","time_stamp", "time", "time")
        self.make_attributes("stage-direction", "question_num", "question-num")
        self.make_attributes("stage-direction", "question_author", "question-author")
        self.set_stage_dir_types()
        self.make_attributes("scene","topic_title","title")
        self.make_attributes("subtopic","intermediate_topic_title","title")
        self.make_attributes("topic","main_topic_title","title")
        self.make_attribute("topic_title","title","scene")
        self.make_attribute("vote_heading","title","vote")
        ##self._make_attribute("vote_element","vote_heading","title")

    def set_stage_dir_types(self):
        """
        Sets the type parameter for stage-directions that need it.
        """
        stage_dirs = elements = self.tree.xpath(".//stage-direction")
        for stage_dir in stage_dirs:
            if "question-num" in stage_dir.attrib:
                stage_dir.set("type","written_question")

    def modify_topic_hierarchy(self):
        """
        Replicating the original topic hierarchy is something we need to discuss in more detail later.
        This method is an ad hoc solution for deleting empty scenes at the end of topic element.
        Is used in the "transform" method below.
        """
        topics = self.tree.xpath('.//topic')
        for topic in topics:
            scenes = topic.xpath('.//scene')
            for i,scene in enumerate(scenes): 
                if scene.getchildren() == []:
                    try:
                        scene.append(scenes[i + 1])
                    except Exception as e:
                        print "Error %s for next scene of %s" % (e,scene.get('title'))
                        scene.getparent().remove(scene)

    def delete_empty_paragrahps(self,tagname): 
        # -> TO DO MISTAKE HERE
        elements = self.tree.xpath('.//{}'.format(tagname))
        for element in elements:
            if element.text == None or element.text == "" or len(element.text) == 0: 
                element.getparent().remove(element)

    def fix_loose_time_stamps(self):
        timestamps = self.tree.xpath(".//time_stamp");
        for timestamp in timestamps:
            timestamp.tag = "stage-direction"
            timestamp.text = Util.normalize_text(timestamp.text, "time")
            timestamp.set("type", "timestamp")

    

    def make_tables_orphans(self):
        tables = self.tree.xpath(".//speech/table")
        for table in tables:
            grandparent = table.getparent().addnext(table)

    def transform(self):
        """
        Main method, integrates most of the above methods.
        """
       
        self.add_metadata()
        self.fix_loose_time_stamps()
        #self.make_tables_orphans()
        self.change_element_name("intervention","stage-direction",delete_tags=True,element_to_delete="intervention_element")
        self.delete_empty_paragrahps("p")
        #self.delete_empty_paragrahps('stage-direction')
        self.change_element_name('subtopic',"scene")
        self.modify_topic_hierarchy()
        #self.tree = self.change_namespace()
        #self.change_namespace()
        self.output.write(Etree.tostring(self.tree, pretty_print=True,method="xml",encoding="unicode"))