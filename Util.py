# -*- coding: utf-8 -*-
from datetime import datetime as dt
import re
class Util(object):
    """Misc utilty methods to be shared amongst classes"""
    CONST_UNICODE_DOT = unichr(0x2022) # - small filled black dot
    CONST_UNICODE_SQUARE = unichr(0x25A0) # - large filled black square
    CONST_UNICODE_DIAMOND = unichr(0x2666) # - large filled black diamond
    CONST_UNICODE_OPEN_SINGLE_QUOTE = unichr(0x2018) # - curved opening quote/apostrophe
    CONST_UNICODE_CLOSE_SINGLE_QUOTE = unichr(0x2019) # - curved closing quote/apostrophe
    CONST_UNICODE_LONG_DASH = unichr(0x2014) # - long dash/hyphen
    CONST_UNICODE_SHORT_DASH = unichr(0x2013) # - slightly longer dash/hyphen
    CONST_UNICODE_CLOSE_DOUBLE_QUOTE = unichr(0x201D) # - curved closing quotes
    CONST_UNICODE_OPEN_DOUBLE_QUOTE = unichr(0x201C) # - curved opening quotes

    @staticmethod
    def normalize_text(text, mode):
        """
        Manipulate the string of the attribute value.
        mode can be 'name' or 'unicode' or 'xml' or 'time'
        """
        if mode == "name":
            if "Acting" in text: return text[:-1].strip()
            if "(" in text:
                text = text.split("(")[0]
            text = text.replace(':',"").replace("moved","")
            for t in self.titles:
                if text.startswith(t):
                    text = text.replace(t,"")
            return " ".join((text.strip().split()))
        elif mode == "unicode":
            text = text.replace(Util.CONST_UNICODE_DOT, "[DOT]")
            text = text.replace(Util.CONST_UNICODE_SQUARE, "*") #TODO: Figure out if this is a good idea
            text = text.replace(Util.CONST_UNICODE_DIAMOND, "") #FIXME: If we decide to use these as anchors this needs to not happen
            text = text.replace(Util.CONST_UNICODE_OPEN_SINGLE_QUOTE, "'")
            text = text.replace(Util.CONST_UNICODE_CLOSE_SINGLE_QUOTE, "'")
            text = text.replace(Util.CONST_UNICODE_OPEN_DOUBLE_QUOTE, "\"")
            text = text.replace(Util.CONST_UNICODE_CLOSE_DOUBLE_QUOTE, "\"")
            text = text.replace(Util.CONST_UNICODE_LONG_DASH, "-")
            text = text.replace(Util.CONST_UNICODE_SHORT_DASH, "-")
            return text
        elif mode == "xml":
            return u"".join(c for c in text if self.valid_xml_char_ordinal(c))
        elif mode == "time":
            return re.compile(ur"(?:\[DOT\])\s?\(([0-9HIl]{2})([0-9HIl]{2})\s?\)").sub(ur"\1:\2:00", text);
        return text;

    @staticmethod
    def valid_xml_char_ordinal(self,c):
        """
        True if character c is XML compatible, False otherwise
        """
        codepoint = ord(c)
        # conditions ordered by presumed frequency
        return (0x20 <= codepoint <= 0xD7FF or codepoint in (0x9, 0xA, 0xD) or 0xE000 <= codepoint <= 0xFFFD or 0x10000 <= codepoint <= 0x10FFFF)
    
    @staticmethod
    def date(date):
        """
        reads date in string (utf-8) format and converts it to a datetime object
        """
        if date == "present":
            date = dt.date(dt.now())
            yyyy,mm,dd = date.year,date.month,date.day
        elif len(date.split("-")) == 3:
            yyyy,mm,dd = date.split("-")
        else:
            yyyy,mm,dd = date.split("-")[0],"01","01"
        return dt(int(yyyy),int(mm),int(dd))

    @staticmethod
    def convert_date_filename(doc_date):
        """
        Convert dates of the filenames to the format yyyy-mm-dd.
        """
        print doc_date
        months = {"jan":"01","feb":"02","mar":"03","apr":"04","may":"05","jun":"06","jul":"07","aug":"08","sep":"09","oct":"10","nov":"11","dec":"12"}
        doc_date = doc_date.split('/')[-1].split('.')[0].split('_')
        day,month,year = "","",""
        month = months[doc_date[0].lower()[:3]]
        day = doc_date[1]        
        year = doc_date[2]
        return Util.date("-".join((year,month,day)))
