The House met at 1.25 p.m.
Prayers
[Translation]
Madam Deputy Speaker: I wish to inform the House that pursuant to order made Wednesday, June 16,1993,1 have summoned the House today for the sole purpose of giving royal assent to some bills.
* * *
MESSAGE FROM THE SENATE
Madam Deputy Speaker: I have the honour to inform the House that a message has been received from the Senate informing this House that the Senate has passed the following bills without amendment: Bill C-121, an act to amend the Canada Shipping Act and to amend another act in consequence thereof; Bill C-123, an act respecting the management of certain property seized or restrained in connection with certain offences, the disposition of certain property on the forfeiture thereof and the sharing of the proceeds of disposition therefrom in certain circumstances; Bill C-62, an act respecting telecommunications; Bill C-122, an act to amend the Customs Thriff (textile tariff reduction); Bill C-109, an act to amend the Criminal Code, the Crown Liability and Proceedings act and the Radiocommunication Act; Bill C-103, an act to provide for repeal of the Land Titles Act and to amend other Acts in relation thereto; Bill C-101, an act to amend the Canada Labour Code and the Public Service Staff Relations Act; Bill C-110, an act respecting the Northumberland Strait Crossing; Bill C-115, an act to implement the North American Free Trade Agreement; Bill C-126, an act to amend the Criminal Code and the Young Offenders Act; Bill C-128, an act to amend the Criminal Code and the Customs Tariff (child pornography and corrupting morals); Bill C-106, an act
to amend certain petroleum related acts in respect of Canadian ownership requirements and to confirm the validity of a certain regulation.
* * *
• (1330) [English]
THE ROYAL ASSENT
Madam Deputy Speaker: Order. I have the honour to inform the House that a communication has been received as follows:
Government House Ottawa
Mr. Speaker,
I have the honour to inform you that the Honourable Beverley McLachlin, Puisne Judge of the Supreme Court of Canada, in her capacity as Deputy Governor General, will proceed to the Senate chamber today, the 23rd day of June, 1993 at 1.15 p.m., for the purpose of giving Royal Assent to certain bills.
Yours sincerely,
Judith A. LaRocque Secretary to the Governor General
[Translation]
A message was delivered by the Gentleman Usher of the Black Rod as follows:
Madam Speaker, the Honourable Deputy to His Excellency the Governor General desires the immediate attendance of this honourable House in the Chamber of the honourable the Senate.
Accordingly, the Speaker with the House went up to the Senate Chamber.
• (1335)
And being returned:
Madam Deputy Speaker: I have the honour to inform the House that when the House went up to the Senate chamber the Deputy to His Excellency the Governor General was pleased to give, in Her Majesty’s name, the royal assent to the following bills:
20978
Royal Assent
Bill C-72, an act to establish the National Round TBble on the Environment and the Economy—Chapter No. 31.
Bill C-107, an act to amend the Explosives Act—Chapter No. 32.
Bill C-124, an act to amend the Currency Act—Chapter No. 33.
Bill C-125, an act to correct certain anomalies, inconsistencies, archaisms and errors in the Statutes of Canada, to deal with other matters of a non-controversial and uncomplicated nature in those Statutes and to repeal certain provisions of those Statutes that have expired or lapsed or otherwise ceased to have effect—Chapter No. 34.
Bill C-89, an act to amend the Investment Canada Act—Chapter No. 35.
Bill C-121, an act to amend the Canada Shipping Act and to amend another Act in consequence thereof—Chapter No. 36.
Bill C-123, an act respecting the management of certain property seized or restrained in connection with certain offences, the disposition of certain property on the forfeiture thereof and the sharing of the proceeds of disposition therefrom in certain circumstances—Chapter No. 37.
Bill C-62, an act respecting telecommunications—Chapter No. 38.
Bill C-122, an act to amend the Customs Tariff (textile tariff reduction)—Chapter No. 39.
Bill C-109, an act to amend the Criminal Code, the Crown Liability and Proceedings Act and the Radiocommunication Act-Chapter No. 40.
Bill C-103, an act to provide for the repeal of the Land Titles Act and to amend other Acts in relation thereto—Chapter No. 41.
Bill C-101, an act to amend the Canada Labour Code and the Public Service Staff Relations Act—Chapter No. 42.
Bill C-110, an act respecting the Northumberland Strait Crossing—Chapter No. 43.
Bill C-115, an act to implement the North American Free Trade Agreement —Chapter No. 44.
Bill C-126, an act to amend the Criminal Code and the Young Offenders Act—Chapter No. 45.
Bill C-128, an act to amend the Criminal Code and the Customs 'Iariff (child pornography and corrupting morals)—Chapter No. 46.
Bill C-106, an act to amend certain petroleum-related Acts in respect of Canadian ownership requirements and to confirm the validity of a certain regulation—Chapter No. 47.
[English]
It being 1.36 p.m., pursuant to order made Wednesday, June 23, 1993, this House stands further adjourned to the call of the Chair until Monday, September 20, 1993 at 11 a.m.
The House adjourned at 1.36 p.m.
[.Editor’s note: The thirty-fourth Parliament was dissolved on Wednesday, September 8, 1993, by proclamation of His Excellency the Governor General.]
