The House met at 12.35 p.m.
Government House Ottawa
Prayers
[English]
The Acting Speaker (Mr. Paproski): I wish to inform the House that, in accordance with the order made Monday, January 21,1991,1 have recalled the House this day for the sole purpose of granting Royal Assent to certain bills.
* * *
MESSAGE FROM THE SENATE
The Acting Speaker (Mr. Paproski): I have the honour to inform the House that messages have been received from the Senate informing this House that the Senate have passed the following bills, without amendment: Bill C-37, an act to establish the Canadian Heritage Languages Institute; Bill C-63, an act to establish the Canadian Race Relations Foundation; Bill C-260, an act to amend the Canada Pension Plan (spousal agreement); Bill C-69, an act to amend certain statutes to enable the restraint of government expenditures; Bill C-84, an act respecting privatization of the national petroleum company of Canada; Bill C-40, an act respecting broadcasting and to amend certain acts in relation thereto and in relation to radiocommunication; Bill C-223, an act respecting a Day of Mourning for Persons Killed or Injured in the Workplace; Bill C-88, an act to provide for the membership of Canada in the European Bank for Reconstruction and Development; and Bill C-81, an act to implement the United Nations Convention on Contracts for the International Sale of Goods.
* * *
THE ROYAL ASSENT
The Acting Speaker (Mr. Paproski): I have the honour to inform the House that a communication has been received which is as follows:
February 1st, 1991
Mr. Speaker,
I have the honour to inform you that the Right Honourable Antonio Lamer, Chief Justice of the Supreme Court of Canada, in his capacity as Deputy Government General, will proceed to the Senate Chamber today, the 1st day of February, 1991, at 12.20 p.m., for the purpose of giving Royal Assent to certain Bills.
Yours sincerely,
Judith A. LaRocque Secretary to the Governor General
A message was delivered by the Gentleman Usher of the Black Rod as follows:
Mr. Speaker, the Right Honourable Deputy to the Governor General desires the immediate attendance of this honourable House in the chamber of the honourable the Senate.
Accordingly, the Acting Speaker (Mr. Paproski) with the House went up to the Senate chamber.
And being returned:
The Acting Speaker (Mr. Paproski): I have the honour to inform the House that when the House went up to the Senate chamber the Right Honourable the Deputy to the Governor General was pleased to give, in Her Majesty’s name, the Royal Assent to the following bills:
Bill C-72, an act to establish the Canadian Polar Commission and to amend certain acts in consequence thereof—Chapter No. 6.
Bill C-37, an act to establish the Canadian Heritage Languages Institute—Chapter 7.
Bill C-63, an act to establish the Canadian Race Relations Foundation—Chapter 8.
Bill C-69, an act to amend certain statutes to enable restraint of government expenditures—Chapter 9.
Bill C-84, an act respecting the privatization of the national petroleum company of Canada —Chapter 10.
Bill C-40, an act respecting broadcasting and to amend certain acts in relation thereto and in relation to radiocommunication — Chapter 11.
Bill C-88, an act to provide for the membership of Canada in the European Bank for Reconstruction and Development—Chapter 12.
17570
The Royal Assent
Bill C-81, an act to implement the United Nations Convention on Contracts for the International Sale of Goods —Chapter 13.
Bill C-260, an act to amend the Canada Pension Plan (spousal agreement)—Chapter 14.
Bill C-223, an act respecting a day of mourning for persons killed
or injured in the workplace—Chapter 15.
Pursuant to Order made Monday, January 21, 1991, this House stands further adjourned to the call of the
Chair.
The House adjourned at 12.55 p.m.
17571
HOUSE OF COMMONS
