The House met at 5 p.m.
Prayers
[Translation]
Mr. Speaker: I wish to inform the House that pursuant to order adopted on Thursday, September 17, 1992, I have recalled the House today for the sole purpose of giving royal assent to certain bills.
* * *
MESSAGE FROM THE SENATE
Mr. Speaker: I have the honour to inform the House that a message has been received from the Senate informing this House that the Senate has passed the following bills without amendment: Bill C-306, an act to amend the Department of Forestry Act and to make related amendments to other acts; Bill C-351, an act to change the name of the electoral district of Timiskaming; Bill C-55, an act to amend certain acts in relation to pensions and to enact the Special Retirement Arrangements Act and the Pension Benefits Division Act.
* * *
ROYAL ASSENT
Mr. Speaker: I have the honour to inform the House that a communication has been received as follows:
Rideau Hall
Ottawa
Sir,
I have the honour to inform you that the Honourable Beverly
McLaghlin, Puisne Judge of the Supreme Court of Canada, in her
capacity as Deputy Governor General, will proceed to the Senate chamber today, the 29th day of September, 1992, at 5.00 p.m., for the purpose of giving royal assent to certain bills.
Yours sincerely,
Anthony P. Smyth
Deputy Secretary, Policy, Program and Protocol
• (1710)
A message was delivered by the Gentleman Usher of the Black Rod as follows:
Mr. Speaker, the Honourable Deputy to the Governor General desires the immediate attendance of this honourable House in the Chamber of the honourable the Senate.
Accordingly, Mr. Speaker with the House went up to the Senate chamber.
And being returned:
Mr. Speaker: I have the honour to inform the House that when the House went up to the Senate Chamber the Deputy Governor General was pleased to give, in Her Majesty’s name, the royal assent to the following bills:
Bill C-306, an act to amend the Department of Forestry Act and to make related amendments to other acts—Chapter 44.
Bill C-351, an act to change the name of the electoral district of Timiskaming—Chapter 45.
Bill C-55, an act to amend certain acts in relation to pensions and to enact a Special Retirement Arrangements Act and the Pension Benefits Division Act—Chapter 46.
Bill S-7, an act to amend an act to incorporate the Royal Society of Canada.
It being 5.15 p.m., pursuant to order adopted on Thursday, September 17, 1992, this House stands adjourned to the call of the Chair, but until no later than Monday, November 16, 1992.
The House adjourned at 5.15 p.m.
13361
HOUSE OF COMMONS
