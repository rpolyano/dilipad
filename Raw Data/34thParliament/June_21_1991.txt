The House met at 12.10 p.m.
Prayers
[Translation]
Mr. Speaker: I wish to inform the House that pursuant to order made on Thesday, June 18, 1991,1 recalled the House today for the sole purpose of granting royal assent to certain bills.
[English]
MESSAGE FROM THE SENATE
Mr Speaker: I have the honour to inform the House that a message has been received from the Senate informing this House that the Senate has passed Bill C-24, an act for granting to Her Majesty certain sums of money for the Government of Canada for the financial year ending the March 31, 1992.
[Translation]
Furthermore, messages have been received from the Senate informing this House that the Senate has passed the following bills without amendment:
[English]
Bill C-2, an act to amend the National Energy Board Act; Bill C-6, an act respecting the exporting, importing, manufacturing, buying or selling of or other dealing with certain weapons, and Bill C-9, an act to facilitate combatting the laundering of proceeds of crime.
[Translation]
ROYAL ASSENT
Mr. Speaker: I have the honour to inform the House that a communication has been received as follows:
Rideau Hall Ottawa
I have the honour to inform you that the Hon. Charles D. Gonthier, Puisne Judge of the Supreme Court of Canada, in his capacity as Deputy Governor General, will proceed to the Senate Chamber today, the 21st day of June, 1991, at 12.10 p.m., for the purpose of giving Royal Assent to certain bills.
Yours sincerely,
Judith A. LaRocque Secretary to the Governor General
ROYAL ASSENT
[Translation]
A message was delivered by the Gentleman Usher of the Black Rod as follows:
Mr. Speaker, the Honourable Deputy to the Governor General desires the immediate attendance of this honourable House in the Chamber of the honourable the Senate.
Accordingly, Mr. Speaker with the House went up to the Senate Chamber.
And being returned:
Mr. Speaker: I have the honour to inform the House that when the House went up to the Senate Chamber the Deputy Governor General was pleased to give, in Her Majesty’s name, the Royal Assent to following bills:
Bill C-9, an Act to facilate combatting the laundering of proceeds of crime—Chapter 26, 1991.
Bill C-2, an Act to amend the National Energy Board Act-Chapter 27, 1991.
Bill C-6, an Act respecting the exporting, importing, manufacturing, buying or selling of or other dealing wiht certain weapons—Chapter 28 1991.
Bill C-24, an Act for granting to Her Majesty certain sums of money for the government of Canada for the financial year ending the 31st March, 1992—Chapter 29, 1991.
Mr. Marcel Prud’homme (Saint-Denis): Mr. Speaker, perhaps you would allow members still present in the House to express their appreciation for the excellent job you did this year, in very difficult circumstances, and also the hope that you will get a good rest and come back to us, exhibiting the same good humour and you have shown during the past months.
Sir,
Royal Assent
[English]
On behalf of all members, we wish you a good rest. I know you are going to work hard in British Columbia; it is my favourite province as you know. We wish you the best summer and we thank you for your kindness and great patience this year under duress and very difficult circumstances.
[Translation]
Mr. Speaker: Of course I would like to thank the hon. member for Saint-Denis for his very generous comments.
[English]
I wish him and all other members of this House a very happy and useful break over the summer months. Thank you all.
Mrs. Sheila Finestone (Mount Royal): Mr. Speaker, I think that it will be important from the distaff side of this House and from all members to echo the sentiments expressed by my colleague from Saint-Denis and to tell
you that we look with a great deal of pride and pleasure on the ability that you have shown in a very raucous and difficult time in this House. To your Ihble Officers, to all the young people who served you as Pages, I know that you spoke in our name, and may I speak in the name of members of this House and tell you how much pleasure we have had in working with you. We look forward to a far calmer great Canada as Canada Day comes forward. May we all live in a country coast-to-coast in prosperity and in tolerance together.
[Translation]
Mr. Speaker: The hon. member for Mount Royal is most kind.
[English]
I appreciate very much her comments.
It being 12.30 p.m., pursuant to order made on Tbesday, June 18, 1991, this House stands further adjourned until Monday, September 16th, 1991, at two o’clock.
The House adjourned at 12.30 p.m.
HOUSE OF COMMONS
