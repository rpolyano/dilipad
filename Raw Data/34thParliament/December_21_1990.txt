The House met at 11.30 a.m.
THE ROYAL ASSENT
Prayers
[Translation]
MESSAGE FROM THE SENATE
Mr. Speaker: I have the honour to inform the House that a message has been received from the Senate informing this House that the Senate has passed the following bills: Bill C-90, an Act to amend the Bank Act (extension) and Bill C-92, an Act to amend the Northwest Territories Act.
* * *
THE ROYAL ASSENT
Mr. Speaker: I have the honour to inform the House that a communication has been received as follows:
Rideau Hall Ottawa,
December 21,1990 Sir,
I have the honour to inform you that the Hon. Peter C. Cory, Puisne Judge of the Supreme Court of Canada, in his capacity as Deputy Governor General, will proceed to the Senate Chamber today, the 21st day of December, 1990, at 11.30 a.m., for the purpose of giving Royal Assent to certain bills.
Yours sincerely,
Judith A. LaRocque Secretary to the Governor General
[Translation]
A message was delivered by the Gentleman Usher of the Black Rod as follows:
Mr. Speaker, the Honourable Deputy to the Governor General desires the immediate attendance of this honourable House in the chamber of the honourable the Senate
Accordingly, Mr. Speaker with the House went up to the Senate chamber.
And being returned:
Mr. Speaker: I have the honour to inform the House that when the House went up to the Senate Chamber the Deputy Governor General was pleased to give, in Her Majesty’s name, the Royal Assent to the following bills:
Bill C-90, an Act to amend the Bank Act (extension)— Chapter 47; and
Bill C-92, an Act to amend the Northwest Territories Act —Chapter 48.
[English]
Mr. Speaker: It being 11.45 o’clock a.m., pursuant to Order made Wednesday, December 19,1990, this House stands further adjourned until Monday, February 18, 1991, at 2 o’clock p.m.
At 11.48 a.m. the House adjourned, pursuant to special order.
HOUSE OF COMMONS
