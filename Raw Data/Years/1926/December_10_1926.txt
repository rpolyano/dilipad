The House met at three o’clock.
OPENING OF THE SESSION
Mr. Speaker read a communication from the Governor General’s Secretary announcing that His Excellency would proceed to the Senate chamber at three p.m. on this day for the puipose of formally opening the session of the Dominion parliament.
A message was delivered by Major A. R. Thompson, Gentleman Usher of the Black Rod, as follows:
Mr. Speaker, His Excellency the Governor General desires the immediate attendance of this honourable House in the chamber of the honourable the Senate.
Accordingly the House went up to the Senate chamber.
[Mr. Speaker.]
Then the Hon. Rodolphe Lemieux, Speaker-elect, said:
May it please Your Excellency,
The House of Commons have elected me as their Speaker, though I am but little able to fulfil the important duties thus assigned to me. If in the performance of these duties I shovdd at any time fall into error, I pray that the fault may he imputed to me, and not to the Commons whose servant I am.
The Honourable the Speaker of the Senate, addressing the Honourable the Speaker of the House, then said:
Mr. Speaker, I am commanded by His Excellency the Governor General to assure you that your words and actions will constantly receive from him the most favourable construction.
Then His Excellency the Governor General
was pleased to open parliament by a speech from the throne.
And the House being returned to the Commons chamber.
Mr. SPEAKER:	I have the honour to
state that the House having attended on His Excellency the Governor General in the Senate chamber, I informed His Excellency that the choice of Speaker had fallen upon me, and, in your names and on your behalf, I made the usual claim for your privileges, which His Excellency was pleased to confirm to you.
OATHS OF OFFICE
Right Hon. W. L. Mackenzie King (Prime Minister) moved for leave to introduce Bill No. 1, respecting the Administration of Oaths of Office.
Motion agreed to. and bill read the first time.
GOVERNOR GENERAL’S SPEECH
Mr. SPEAKER:	I have the honour to
inform the House that when the House did attend His Excellency the Governor General this day in the Senate chamber, His Excellency was pleased to make a speech to both houses of parliament. To prevent mistakes, I have obtained a copy, which is as follows:
Honourable Members of the Senate:
Members of the House of Commons:
I desire on this occasion to assure you of the great satisfaction it affords me to be associated with you in the important tasks upon which you are about to enter at this, the first session of the Sixteenth Parliament of Canada, and to avail myself of your assistance and advice in discharging the duties which His Majesty the King has entrusted to me as his representative.
Once again we have cause to be thankful for a bountiful harvest and other assurances of continued prosperity. It is gratifying to note that during the year the foreign trade of Canada has shown further marked improvement and that immigration has substantially increased.
• Select Standing Committees
The necessity of making adequate provision for the public services has compelled me to summon you at an earlier date than would otherwise have been necessary. In order to provide for present and immediate future needs, and to regularize expenditures already made, you will be asked forthwith to vote the necessary supply for the current financial year. It is not proposed to proceed with the ordinary business of the session until the reassembling of parliament in the new year.
Those government measures which passed the House of Commons at the last session of parliament, but which failed to become law, will be reintroduced. Amendments to the Canada Grain Act will also be submitted for your consideration.
With a view to expediting public business generally, it is proposed to afford opportunity for an early consideration of amendments to the rules of the House of Commons.
My government has continued to give special attention to the fuel problem and measures providing for assistance to works constructed for the production of domestic coke from Canadian coal will be submitted.
The report of the commission appointed under the Inquiries Act to examine and report upon conditions in the maritime provinces will be presented immediately and your attention will be invited to its recommendations. Measures dealing with the matters referred to in the report of the commission are now under consideration by my government, and certain legislation in respect thereto will be introduced.
Good progress has been made with work on the Hudson Bay railway and it is planned to continue construction at as early a date as possible next year. It has been decided to submit the study of conditions at the port to the careful examination of an outstanding British authority on tidal and estuarial conditions affecting harbours.
Canadian National branch line construction on the basis of a definite three-year programme having proved entirely successful, that method of dealing with necessary railway expansion will be continued, and another three-year programme will be submitted for your consideration.
You will also be asked to approve an agreement with the holders of Grand Trunk Pacific perpetual debentures.
Members of my government have just returned to Canada from attending the meetings of the Imperial conference. The report of the proceedings of the conference, together with its recommendations, will be placed before you for consideration. It will, I believe, be recognized that the joint labours of the governments represented at the conference have gone far to set forth the relations of the members of the British Commonwealth of Nations to one another and to foreign countries and to ensure a ready appreciation of the full measure of self-government now attained in all that relates to their domestic and external affairs. In the prolonged consideration given to specific matters of joint concern, the conference has done much to ensure the free and effective co-operation in common ends of the governments and peoples of the British Empire.
The recent appointment of a minister plenipotentiary accredited by His Majesty to represent the interests of Canada in the United
States marks an important stage in the development of the international relations of the Dominion.
The diamond jubilee of confederation will be appropriately celebrated during the coming year. I am pleased to inform you that His Koyal Highness the Prince of Wales has graciously accepted the invitation of my government to visit Canada, circumstances permitting, in connection with the celebration. My government has also extended an invitation to the Prime Minister of Great Britain; the Prime Minister has accepted the invitation and has expressed the hope that when the time arrives he may find it possible to be present.
Members of the House of Commons:
The estimates for the current fiscal year which have not hitherto been voted by parliament will, as already mentioned, be submitted for your approval forthwith. Estimates for the financial year 1927-28 will be submitted for your consideration when parliament reassembles.
Honourable Members of the Senate:
Members of the House of Commons:
In inviting your careful consideration of the important matters which will engage your attention, I pray that Divine Providence may guide and bless your deliberations.
Right Hon. W. L. MACKENZIE KING (Prime Minister) moved:
The the speech of His Excellency, the Governor General, to both houses of parliament be taken into consideration on Monday next, and that this order have precedence over all other business, except the introduction of bills, until disposed of.
SELECT STANDING COMMITTEES Right Hon. W. L. MACKENZIE KING (Prime Minister) moved:
That a special committee be appointed to prepare and report, with all convenient speed, lists of members to compose the select standing committees of this House under yule 10, said committee to be composed of Messieurs King (Kootenay), P. F. Casgrain H. A. Stewart (Leeds) and R. B. Hanson.
Mr. ROBERT GARDINER (Acadia): Before you put the motion Mr. Speaker, I desire respectfully to call the attention of the Prime Minister to the fact that there are in this House two or three groups who have no representative on that committee. I can quite understand how, in the rush of business, after the Prime Minister’s recent return from Great Britain, he has overlooked this situation, and I merely draw it to his attention, and would respectfully ask him to give these groups some representation on that committee.
Mr. MACKENZIE KING:	I desire to
point out to my hon. friend that the committee is a very small one, as he will observe, and I have not the slightest doubt that each member of the committee will be quite ready
Estimates
to confer with my hon. friend and with the representatives of other groups in the House in preparing the report. Speaking for the government, I may say that I have every desire to treat fairly all sections of the House.
Motion agreed to.
INTERNAL ECONOMY COMMISSION
Right Hon. W. L. MACKENZIE KING (Prime Minister) presented the following message from his Excellency the Governor General:
The Governor General transmits to the House of Commons a certified copy of an approved Minute of Council appointing the Honourable W. R. Motherwell, Minister of Agriculture, the Honourable J. H. King, Minister ■of Health and Minister of Soldiers’ Civil Reestablishment, the Honourable J. C. Elliott, Minister of Public Works; and the Honourable Fernand Rinfret, Secretary of State, to act with the Speaker of the House of Commons, as commissioners for the purposes and under the provisions of the eleventh chapter of the Revised Statutes of Canada, 1906, intituled ‘An Act respecting the House of Commons.”
MARITIME CLAIMS COMMISSION
REPORT LAID ON THE TABLE BY PRIME MINISTER
Right Hon. W. L. MACKENZIE KING (Prime Minister):	I beg to lay on the
table a copy of a report on the maritime claims, requested by the hon. leader of the opposition yesterday. I move, seconded by the Hon. Mr. Lapointe, that a thousand copies in English and five hundred copies in French of the report of the Royal Commission on Maritime Claims be printed forthwith, and that rule 74 be suspended in relation thereto.
Motion agreed to.
IMPERIAL CONFERENCE
SUMMARY OF PROCEEDINGS TABLED BY PRIME MINISTER
Right Hon. W. L. MACKENZIE KING (Prime Minister):	I desire to lay on the
table a summary of proceedings of the Imperial conference 1926, including the report of the committee on inter-imperial relations. The appendices to the summary, containing reports of other committees and sub-committees, will be laid on the table as soon as the compilation is completed.
I move that a thousand copies in English and five hundred copies in French of the summary of the imperial proceedings of the conference of 1926, laid upon the table of the House this day, be printed forthwith, and that rule 74 be suspended in relation thereto.
Motion agreed to. fMr. Mackenzie King.]
SUPPLY
Hon. J. A. ROBB (Minister of Finance): As intimated in the address just received from His Excellency, the Governor General, it will be necessary to provide for carrying on the public service for the month of December, and I therefore move:
That this House will on Monday next resolve itself into a committee to consider the supply to be granted to His Majesty, and that rule 76 in relation thereto be suspended.
Motion agreed to.
Hon. J. A. ROBB (Minister of Finance) moved:
That this House will on Monday next resolve itself into a committee to consider ways and means for the raising of the supplies to be granted to His Majesty, and that rule 76 in relation thereto be suspended.
Motion agreed to.
ESTIMATES FOR 1926-27
A message from His Excellency the Governor General transmitting estimates for the financial year ending March 31, 1927, was presented by Hon. J. A. Robb (Minister of Finance), read by Mr. Speaker to the House, and referred to the committee of Supply.
Mr. ROBB:	I have just placed on the
table the estimates for the financial year ending March 31, 1927, exactly as presented to parliament last session. I now desire to place on the table, so that it will be available in the distribution office for hon. members, explanations of estimates, governor general’s warrants, and interim supply.
I also desire to lay on the table, for the information of the House a statement prepared in the Department of Finance showing in months and in the aggregate, the amount of governor general’s warrants issued since dissolution for the various departmental services.
On motion of Mr. Mackenzie King, the House adjourned at 4.08 p.m.
