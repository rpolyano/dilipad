The house met at 2.30 p.m.
HOUSE OF COMMONS
PROPOSED MOTION RESPECTING HOUSE VOTE ON BILL C-193
Right Hon. L. B. Pearson (Prime Minister):
Mr. Speaker, a motion has been submitted for the order paper in the following terms. I move, seconded by Mr. Martin (Essex East):
That this house does not regard its vote on February 19th in connection with third reading of Bill C-193—
Some hon. Members: Oh, oh.
Mr. Starr: What does this government regard?
Mr. Pearson: My hon. friends seem a little excited.
Mr. Starr: You are more excited than we are.
Mr. Speaker: Order, please.
Mr. Pearson: Perhaps, Mr. Speaker, I might be allowed to read this motion which has been submitted to the order paper. It continues:
—in connection with third reading of Bill C-193 which had carried in all previous stages, as a vote of non-confidence in the government.
Without going into the substance of this motion at this time, and in a purely procedural way I ask for unanimous consent—
Some hon. Members: No.
Mr. Pearson: —that notwithstanding the 48 hour regulation this motion may be considered at once by this house.
Some hon. Members: Hear, hear.
Hon. Robert L. Stanfield (Leader of the Opposition): Mr. Speaker, we take the position that this government has no right to put any business before this house—
Some hon. Members: Hear, hear.
Mr. Stanfield: —including a motion of confidence. It was defeated on a bill to raise
taxes, a fundamental part of the government’s program, a defeat clearly involving a loss of confidence of the house.
Some hon. Members: Hear, hear.
Mr. Stanfield: The constitution and the constitutional practice must be respected. It is fundamental to our constitution that such a loss of a fundamental vote of confidence involves resignation or a recommendation for dissolution.
Some hon. Members: Hear, hear.
Mr. Stanfield: The government, to try to place any business or any substantive motion before this house, would make a mockery of the vote that took place on Monday evening.
Some hon. Members: Hear, hear.
Mr. Stanfield: There is no precedent for a government defeated on such a fundamental matter as this to then try to do further business with the house, as it tried to do on Monday evening following that defeat—actually being defeated twice on that occasion —and again now trying to do so by putting this motion before the house.
I say, sir, that the government is flouting the constitution. It is flouting parliament. It has no right to present such a motion.
Some hon. Members: Hear, hear.
Mr. Stanfield: We therefore cannot agree to waive notice. To do so would be to make light of the vote that took place on Monday night. Furthermore since we believe and are convinced the government has no right to make such a motion, we certainly cannot agree to waive notice.
Some hon. Members: Hear, hear.
Mr. T. C. Douglas (Burnaby-Coquiilam):
Mr. Speaker, the members of the New Democratic party take the position that while the government may have a constitutional right to ask the house for a vote of confidence, it has no moral right—
Some hon. Members: Oh, oh.
6904	COMMONS
Motion Respecting House Vote
Mr. Douglas: —to ask this house for a vote of confidence in view of the fact that one of its major fiscal measures was defeated in this house last Monday night.
Some hon. Members: Hear, hear.
Mr. Douglas: In our system of parliamentary government the most important right is the right to impose taxes. This government has been denied that right by a majority of members voting on Bill No. C-193.
Some hon. Members: Hear, hear.
Mr. Douglas: However, Mr. Speaker, the defeat of that measure and the ineptitude of the government in dealing with it are merely symptomatic of the ineptitude of the government’s handling of the entire Canadian economy.
Some hon. Members: Hear, hear.
Mr. Douglas: The vote which was held on Monday night is merely the culmination of a long process of disintegration of a government which is not only inept but so badly divided that it cannot make any decisions on major matters of importance.
Some hon. Members: Hear, hear.
Mr. Douglas: This party therefore takes the position that the government’s only real option is to seek a mandate from the electorate and ask the voters of this country—
An hon. Member: Oh, oh.
Mr. Starr: If you had any principles that is what you would do.
Mr. Douglas: —whether they are willing to have a government impose a 5 per cent surtax on personal incomes at the same time they are handing back millions of dollars to corporations as the result of a refund tax that was imposed on these people. As the Prime Minister said last night, the government needs the money; but the Canadian people have a right to know whether the government needs it so badly that it should turn over tens of millions of dollars in the form of reimbursement to the corporations of this country. That is the issue on which this government should go to the electorate. That is the question on which members of the government should ask the voters for a mandate.
With reference to the motion which the Prime Minister has asked that we give consent to having debated today, I should say
[Mr. Douglas.]
DEBATES	February 21, 1968
that while we think the government has no moral right to ask for a vote of confidence, we do not deny its constitutional right. Had the government brought in a straight motion of confidence we might well have agreed to proceed with the debate today. However, the motion which the Prime Minister has sent to the Clerk’s office is not a motion of confidence; it is a motion asking this house to repudiate a decision it has already made.
Some hon. Members: Hear, hear.
Mr. Douglas: It is a motion asking this house to turn back the clock.
An hon. Member: Right.
Mr. Douglas: It is a motion which is so unprecedented in its arrogance that we have the right to ask for time to study it and assess its implications—
Some hon. Members: Oh, oh.
Mr. Douglas: —because I suggest to this house that this motion could be the prelude to the government seeking to reinstate Bill C-193, which otherwise it would be unable to do under the rules of the house.
Some hon. Members: Hear, hear.
Mr. Douglas: In my opinion this is the first step in an attempt to do something which the rules of the house prevent them from doing. This is an attempt to reimpose a tax which the House of Commons by vote denied the government the right to impose.
Some hon. Members: Hear, hear.
Mr. Douglas: Therefore, Mr. Speaker, we are compelled to ask ourselves whether we are now to have a repetition of the pipe line debate; whether we are to have another attempt to wipe the slate clean and start all over again; whether we are to have an attempt by the government to rewrite history and make the House of Commons reverse a decision that has been made in order to allow the government to then force another decision on this House of Commons and force a tax upon the Canadian people.
Therefore we cannot consent to proceeding with this motion today. If the government insists on placing it on the order paper of course it will come up on Friday. In our opinion the government ought to do what is the proper thing to do; that is, having been defeated, the government has the right to go to the Governor General and ask for
dissolution of parliament. If it is not prepared to do that, this house ought to adjourn until the motion the Prime Minister is going to move can be debated.
Mr. Woolliams: That’s right.
Mr. Douglas: I do not think this house has any right to proceed with any business until this matter has been settled. It seems to me, therefore, that the government should have a little time to make its decision as to whether it wants to seek the dissolution of parliament or wants to proceed with the motion the Prime Minister has read.
With that in mind, Mr. Speaker, it seems to me there is little value in trying to continue with the business of the house. The house ought to adjourn until Friday. If at that time the government wishes to proceed with this motion we cannot prevent it from doing so. In the meantime, I hope the government will take the decision of last Monday night as an indication that it has lost the confidence of this house.
Some hon. Members: Hear, hear.
[Translation]
Mr. Real Caouelle (Villeneuve):	Mr.
Speaker, I deplore the interventions of some Conservative members when the Prime Minister (Mr. Pearson) rose to read the motion he is introducing today and which reads as follows:
That this house does not regard its vote on February 19 in connection with the third reading of Bill C-193 which it carried on all its previous stages as a vote of non-confidence in the government.
Mr. Speaker, as the previous speaker just said, only the government, and nobody else, is to blame for the situation in which it finds itself at present. In fact, on the evening of the vote, the government did not have to move the third reading of Bill No. C-193; it did so, and the motion was defeated by a majority vote of the house.
If we believe that, in a democracy, parliament is the real sovereign of the country, we may say, in the present circumstances, that the government has been defeated by a parliamentary majority and that it should resign.
Some hon. Members: Hear, hear.
Mr. Caouelle: The motion of the Prime Minister is to the effect that, on third reading, the bill had passed all the previous
Motion Respecting House Vote stages. It should be added that it was on division, since the various motions and the clauses of the bill were passed by the house on division, precisely because the government had a majority. We objected to Bill No. C-193 at the resolution stage. We are still opposed to it, and if the motion for third reading has been rejected by a vote in the house, I see no other way out for the government than to go to the people and ask for their opinion on the way in which the business of the country is to be dealt with.
I said earlier that the government alone is responsible for the situation in which it now finds itself. That is a fact. Monday evening the Deputy Speaker read the following motion:
Mr. Sharp, seconded by Mr. Benson, moves that Bill No. C-193, an act to amend the Income Tax Act, be now read a third time and do pass.
The vote was taken. Much importance is being attached to the word “now”. At that time the hon. Minister of Finance (Mr. Sharp) could have objected to third reading and deferred it for a day or two, or even a week. This was not done. Instead, the Minister of Finance said arrogantly: Yes, let the motion be put to a vote. We voted, and the government was defeated. Under the circumstances there is but one way out, and that is to go to the people.
With regard to the motion, we all know it is to be debated in 48 hours. The Conservatives are opposed to debating it today, as well as the New Democratic party.
As far as the Ralliement Creditiste is concerned, we would have allowed the government to discuss the matter today, in order not to waste any more public funds, through costly adjournments today and tomorrow. The Canadian people will be paying for that. We will probably be adjourning in a few moments to meet again only on Friday. In the meantime the Canadian people will be footing the bill.
Mr. Speaker, we would therefore have no objection to discussing the motion now, to throw light on the situation, to tell the government what we think, and to let the Canadian people know what is going on.
I feel that if all the members assumed their responsibilities we would allow the motion to be debated immediately to clear up the situation and straighten things out.
6906	COMMONS
Motion Respecting House Vote [English]
Mr. A. B. Patterson (Fraser Valley): Mr.
Speaker, last evening In a statement to the Canadian people the Prime Minister outlined what he anticipated would be the procedure today and the position of the government with respect to this crisis in which they find themselves.
In the course of that presentation to the Canadian people reference was made to the fact that this is a situation which minority governments face constantly, but I suggest that this has nothing to do with a minority government situation, in view of the fact that when the vote was taken the other evening a great percentage of government supporters were not present in the house. Therefore had the government seen to it that their members were present and voting, there would have been no situation develop as we find it today.
Some hon. Members: Hear, hear.
Mr. Patterson: Therefore I cannot agree that this situation has anything whatsoever to do with the minority position in which the government finds itself. The Prime Minister also referred to the fact that the bill had previously passed all stages and had been supported by the House of Commons, and therefore brought into question the position that was being taken that the vote on third reading was actually a vote of non-confidence.
However, Mr. Speaker, it has been brought to my attention this morning that the vote on third reading was a heavier vote than any that were taken during the committee stage or any other stage of the bill. Therefore it seems to me that the logic hardly carries through, because in the heaviest vote that was taken the government was defeated. So it appears to us that the vote was in effect a clear repudiation of the economic program of the government—
Some hon. Members: Hear, hear.
Mr. Patterson: —in view of the fact that the bill was based upon the budget resolutions which were presented to the house last fall.
Had the Prime Minister seen fit to introduce a straight motion of confidence today I think the situation would have been much easier to understand. However, he has seen fit to couch the motion in such terms that it actually implies a repudiation of the decision taken the other evening on third reading.
[Mr. Caouette.]
DEBATES	February 21. 1968
We would not have objected to waiving the 48-hour requirement on this motion in order to get it before the house and find a solution to the problem. However, in saying that may I point out that the government could not in any way expect to get a favourable vote even had the 48-hour requirement been eliminated.
Therefore it seems to me that the only course the government can follow, since apparently it is not going to get the consent of the house to waiving the 48-hour limitation, is to move that the house adjourn until we have an opportunity to discuss the motion which has been introduced by the Prime Minister.
Mr. Pearson: Mr. Speaker, since there does not seem to be unanimous consent to go ahead with this important measure now, naturally it has to be reserved until Friday when we can proceed with it. I hope that tomorrow we will be able to go ahead with our constitutional debate as planned.
Some hon. Members: No.
Mr. Pearson: This is a very important discussion on constitutional changes.
Mr. Starr: You should have kept that in mind last Monday.
Mr. Pearson: Hon. gentlemen opposite are not interested in the constitution. They are interested in throwing out this government and having an election. Mr. Speaker, I move the adjournment of the house.
Mr. Speaker: I understand the right hon. leader of the government has moved the adjournment of the house.
Mr. Stanley Knowles (Winnipeg North Centre): Mr. Speaker, I rise on a point of order. Before the Prime Minister moved the adjournment of the house he made a suggestion as to what the house might deal with tomorrow. In other words he was implying that the adjournment he moved would be effective only until tomorrow rather than until Friday.
My point of order is that the Prime Minister is asking that the house deal with a matter of government business tomorrow before we have dealt with the question as to whether or not the government has the confidence of the house.
I suggest to you, Mr. Speaker, that this is out of order, that the house should not be asked to vote on a motion which implies that we come back tomorrow to deal with another matter before we have dealt with the question of the confidence of the house in the government.
In support of my contention that at this moment the government does not have the confidence of the house may I refer to the Prime Minister’s proposed motion. It is a matter of constitutional understanding in this country that we do not proceed with business unless the government has the confidence of the house, and the motion—
Mr. Speaker:	Order, please. The hon.
member is debating the constitutional position now. This is not what is before the house, although I must bring to the attention of the house that I have not received a written motion with a seconder. The Prime Minister should put this in the hands of the Chair before the motion can be put.
Mr. Starr: They don’t do anything properly.
Mr. Knowles: May I continue with my point of order, Mr. Speaker?
Mr. Speaker: Order, please. I suggest that perhaps we might allow the hon. member for Winnipeg North Centre to finish his point of order pending receipt of the motion.
Mr. Diefenbaker: We will not be throttled.
Mr. Knowles: My point of order is that the government has no right to place before the Chair a motion which implies that we shall deal with other business. The government has admitted by its motion that there is a question as to whether it has the confidence of the house. We contend that it does not have that confidence. A motion that we adjourn which merely means that we adjourn until tomorrow implies that tomorrow we will deal with other business before we have settled the question of confidence. I suggest that the motion is therefore clearly out of order.
Mr. Diefenbaker: Mr. Speaker—
Mr. Pearson: May I repeat my motion? I move, seconded by the Secretary of State for External Affairs (Mr. Martin), that this house do now adjourn.
27053—435
Motion Respecting House Vote
An hon. Member: You are not a dictator.
Mr. Speaker: Is the right hon. gentleman speaking to the point of order?
Right Hon. J. G. Diefenbaker (Prince Albert): Yes, Mr. Speaker, I am on a point of order. This is a most unusual proceeding. A government, having been defeated, now endeavours to get a second chance. I am reminded of 1956—
Some hon. Members: Oh, oh.
Mr. Speaker: Order, please. I thought the right hon. gentleman was about to remind me of the rule that there could be no debate.
Mr. Diefenbaker: No; I want the opportunity of rising. Let that be clear.
Mr. Speaker: The matter is very simple. A motion to adjourn the house is always in order and has been submitted by the Prime Minister. The only thing I can do is put the motion to the house, although if the house wants to give consent I certainly have no objection to hearing what the right hon. gentleman may want to say.
Some hon. Members: No.
Mr. Diefenbaker: With or without consent—
Some hon. Members: Oh, oh.
Mr. Diefenbaker: I rise to a question of order.
An hon. Member: What is the question?
Mr. Diefenbaker: What is the question? Is parliament to be throttled again as it was in 1956 by the same party? The particular point of order is this. In 1956 the government endeavoured to wipe out one day, Friday. What they are trying to do on this occasion is wipe out a decision of parliament, which is an unjust, unfair and unconstitutional proceeding.
Mr. Speaker: Is it the pleasure of the house to adopt the motion?
Mr. Knowles: Mr. Speaker, I ask for a ruling on my point of order.
Mr. Speaker: I heard the point of order, and I submit to the hon. member that he is raising a constitutional question upon which I cannot rule. I cannot accept the point of order. I have to put the motion to the house.
Mr. Horner (Acadia): What is the motion?
Motion Respecting House Vote
Mr. Speaker: Order, please. I will read the motion again.
Mr. Baldwin: I rise on a question of privilege, Mr. Speaker.
Mr. Speaker: I will hear the hon. member for Peace River on a question of privilege, but some members did not hear the motion so I will put it again. The Prime Minister, seconded by the Secretary of State for External Affairs moved:
This house do now adjourn.
This was the motion I read a moment ago.
Mr. G. W. Baldwin (Peace River): Any
such motion must be taken without prejudice to the right of myself and other hon.
DEBATES
members to say to the government that it is no longer the government so far as the House of Commons is concerned and cannot put any motion.
Mr. Speaker: All those in favour of the motion will please say yea.
Some hon. Members: Yea.
Mr. Speaker: All those opposed will please say nay.
Some hon. Members: Nay.
Mr. Speaker: In my opinion the yeas have it. I declare the motion carried.
Motion agreed to and the house adjourned at 3.06 p.m.
HOUSE OF COMMONS
