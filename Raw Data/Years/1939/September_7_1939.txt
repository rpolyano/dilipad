FIFTH (SPECIAL WAR) SESSION-EIGHTEENTH PARLIAMENTOPENING
Speaker: The Hon. Pierre-Francois Casgrain
The parliament which had been prorogued from time to time to the second day of October, 1939, met this day at Ottawa, for the dispatch of business.
The house met at three o’clock, the Speaker in the chair.
Mr. Speaker read a communication from the Governor General’s secretary, announcing that His Excellency the Governor General would proceed to the Senate chamber at three p.m. on this day, for the purpose of formally opening the session of the dominion parliament.
A message was delivered by Major A. R. Thompson, Gentleman Usher of the Black Rod, as follows:
Mr. Speaker: His Excellency the Governor General desires the immediate attendance of this honourable house in the chamber of the honourable the Senate.
Accordingly the house went up to the Senate chamber.
And the house being returned to the Commons chamber:
OATHS OF OFFICE
Right Hon. W. L. MACKENZIE KING (Prime Minister) moved for leave to introduce Bill No. 1, respecting the administration of oaths of office.
Motion agreed to and bill read the first time.
GOVERNOR GENERAL’S SPEECH
Mr. SPEAKER: I have the honour to inform the house that when the house did attend His Excellency the Governor General this day in the senate chamber His Excellency was pleased to make a speech to both houses of parliament. To prevent mistakes I have obtained a copy, which is as follows: Honourable Members of the Senate:
Members of the House of Commons:
As you are only too well aware, all efforts to maintain the peace of Europe have failed. The United Kingdom, in honouring pledges given as 87134—1
a means of avoiding hostilities, has become engaged in war with Germany. You have been summoned at the earliest moment in order that the government may seek authority for the measures necessary for the defence of Canada, and for co-operation in the determined effort which is being made to resist further aggression, and to prevent the appeal to force instead of to pacific means in the settlement of international disputes. Already the militia, the naval service and the air force have been placed on active service, and certain other provisions have been made for the defence of our coasts and our internal security under the War Measures Act and other existing authority. Proposals for further effective action by Canada will be laid before you without delay.
Members of the House of Commons:
You will be asked to consider estimates to provide for expenditure which has been or may be caused by the state of war which now exists.
Honourable Members of the Senate:
Members of the House of Commons:
I need not speak of the extreme gravity of this hour. There can have been few, if any, more critical in the history of the world. The people of Canada are facing the crisis with the same fortitude that to-day supports the peoples of the United Kingdom and other of the nations of the British commonwealth. My ministers are convinced that Canada is prepared to unite in a national effort to defend to the utmost liberties and institutions which are a common heritage.
Right Hon. W. L. MACKENZIE KING (Prime Minister) moved:
That the speech of His Excellency the Governor General to both houses of parliament be taken into consideration on Friday next.
Motion agreed to.
EUROPEAN WAR
TABLING OF DOCUMENTS RELATING TO OUTBREAK
OF WAR—EMERGENCY ORDERS IN COUNCIL
Right Hon. W. L. MACKENZIE KING (Prime Minister):	With the consent of 'the
house I desire to lay on the table documents relating to the outbreak of war, September, 1939, copies of which, in English and French, are being distributed this afternoon.
I desire also to lay on the table copies of emergency orders in council passed since August 25, 1939, to date. The house I think will be interested in having immediately a
REVISED EDITION
Emergency Orders in Council
statement indicating the purpose of the various orders, which I shall give, perhaps omitting the numbers of the orders.
Order relating to the issue of special warrant for $8,918,930 for expenditures for naval service, militia service and air service.
Regulation regarding calling out militia under section 63 of the Militia Act.
Regarding purchase of aircraft, spares and accessories up to $7,500,000.
Regarding control of shipping.
Regarding warrant for $1,453,000 making provision for thirty days for militia personnel, transportation, rations, engineer services and purchase of stores.
Regarding approval of financial regulations and instructions for the Canadian field force covering pay and allowances, etc.
Regarding employment of parts and personnel of the auxiliary active air force and the reserve air force.
Warrant of $150,000 regarding air raid precautions.
Constitution of subcommittees of council.
I should like to say with regard to this particular order that while committees were named and personnel selected, with reference to what at the time seemed the best arrangement to make, the order is not to be construed as necessarily restricting the personnel of each committee to the names which appear in the list. It will be obviously desirable from time to time to change the personnel of the different committees.
The designation of the committees themselves will indicate the purposes for which they have been formed.
Warrant for $536,600 to cover expenses in connection with transfer of units of the Royal Canadian Air Force to east coast and calling out for training of Auxiliary Air Force, for a period of thirty days.
Proclamation regarding meeting of parliament on September 7, 1939.
Regarding proclamation concerning existence of apprehended war.
Placing on active service the reserve naval forces of Canada.
Placing on active service the permanent naval forces.
Regarding warrant for $5,345,590 to bring up the permanent active air force to full peace establishment.
Establishment of censorship regulations.
Placing active militia on war establishment.
Establishing the defence of Canada regulations.
Regarding engagement of ex-members of the Royal Canadian Mounted Police force.
[Mr. Mackenzie King.]
Appointment of the commissioner of the Royal Canadian Mounted Police as registrar general of alien enemies.
Constitution of prize courts.
Regulations regarding pensions.
Warrant for $50,000 to cover employment of extra civilian personnel for emergency duty.
Regarding censorship in respect of cable, radio, telegraph and telephone companies or circulation of prohibited matter.
Regarding expression “ Canadian active service force ” to be used instead of “ Canadian field force.”
Censorship regulations 1939.
Regarding calling out of units, formations and detachments of the auxiliary active air force.
Application by the government of the United Kingdom of the war risks insurance scheme to British ships registered in Canada.
Regarding postal censorship.
Placing on active service depots of corps of the active militia.
Authorization to call out officers and airmen of the reserve air force as required.
Regulations regarding trading with the enemy, 1939.
Setting up the censorship coordination committee.
Regarding members of the naval forces, the militia, or the Royal Canadian Air Force, being retained as civil servants if required by their department.
Appointment of Walter S. Thompson as chairman of the censorship coordination committee.
Establishment of regulations concerning prices of food, fuel and other necessaries of life.
Appointment of the War-time Prices and Trade Board.
Internment of enemy aliens.
Regarding control of shipping.
Regarding employees of the Canadian Broadcasting Corporation, National Harbours Board, Canadian National Steamships, Trans-Canada Air Lines, railway and telegraph companies to be retained as civil servants, if deemed necessary by departmental heads.
Calling out for active service certain units, formations and detachments of the auxiliary active air force.
Transfer of Canadian government ships to naval services, non-application of Government Vessels Discipline Act.
Constitution of dependents and allowance board.
Appointment of cable and trans-oceanic radio censorship personnel with remuneration rates.
The Ministry
Censorship regulations; application of same in regard to circulation of prohibited matter and press censorship.
Censorship regulations; application of same in regard to the operations, offices, works or property of radiotelegraph or radiotelephone stations, radio broadcasting station or any other class of radio station.
Mr. MANION: Am I right in understanding that this is a complete list of the emergency orders in council?
Mr. MACKENZIE KING: Yes.
Mr. MANION: My right lion, friend has read a complete list?
Mr. MACKENZIE KING: Yes. Of course, there have been many other orders passed in the last week; but these are the orders which refer to the emergency situation.
Mr. MANION: The ones my right hon. friend has read?
Mr. MACKENZIE KING: Yes.
ROYAL VISIT
MESSAGE FROM HIS MAJESTY THE KING EXPRESSING APPRECIATION AND THANKS
Right Hon. W. L. MACKENZIE KING (Prime Minister): Mr. Speaker, I am pleased to inform the house that I have the gracious permission of the king to table a copy of a letter which I received from His Majesty upon the return to England of the king and queen, after their majesties’ visit to Canada and the United States. I assume it will be the wish of hon. members that His Majesty’s letter should appear in Hansard, and if the house agrees I will ask the Clerk so to instruct the Editor of Debates. It is my intention to have the letter itself placed in the Canadian Archives.
The king’s letter is as follows:
Buckingham Palace
13th July, 1939.
My dear Prime Minister:
Since my return to England, I have been fully occupied with work which had accumulated in my absence; I fear you must have had a similar experience when you got back to Ottawa.
But I do not wish to let more time elapse without telling you how deeply grateful I am to you, and to your colleagues in my Canadian government, for all the care and forethought that you bestowed on the preparations for my recent visit. Both the queen and I realize what heavy responsibilities such a tour as ours lays on the shoulders of ministers, and we appreciate highly the manner in which those responsibilities were discharged. Its unquestioned success was very largely due to the skill with which it was planned; and though it could not, in the time at our disposal, be anything but strenuous in 87134—14
character, we were sensible throughout that every possible consideration had been given both to our safety and to our comfort.
It was a great satisfaction to me to have an opportunity from time to time of meeting so many of my Canadian ministers, and I feel that my knowledge of the country as a whole has been considerably enlarged by the conversations that I had with them on many occasions.
To you personally I am particularly grateful for your helpful advice and support while you were in attendance on me; I need hardly say that I found your mature experience of Canadian affairs of very great value.
The gold bowl, given to us by the Canadian government, has now arrived here safely; I should be glad if you would, on some suitable occasion, convey to your colleagues the cordial thanks of the queen and myself for this present, which, apart from its beauty of design and craftsmanship, is a delightful memento of our ■ long journey.
Before the summer is over you will, I hope, be able to get some real rest, for you have had an especially busy and exacting year. I send you my best wishes for a pleasant holiday.
Believe me,
Yours very sincerely,
George R.I.
The Right Honourable
W. L. Mackenzie King, LL.D.,
Prime Minister of Canada.
THE MINISTRY
APPOINTMENT OF MINISTER OF JUSTICE AS ACTINQ SECRETARY OF STATE
Right Hon. W. L. MACKENZIE KING (Prime Minister):	Mr. Speaker, my next
duty is to announce changes in the ministry since our last meeting. As hon. members are aware the post of Secretary of State became vacant upon the death of the Hon. Fernand Rinfret, and I should like to table the order in council appointing my colleague, the right hon. the Minister of Justice (Mr. Lapointe) as acting Secretary of State to hold the position until another Secretary of State will be appointed.
THE MINISTER OF FINANCE—RESIGNATION OF MR. DUNNING AND APPOINTMENT OF MR. RALSTON
Right Hon. W. L. MACKENZIE KING (Prime Minister):	Mr. Speaker, as hon.
members are aware the Hon. Mr. Dunning some time ago asked me for reasons of health to accept his resignation as Minister of Finance. At the time I hoped very much it might not become necessary to accept Mr. Dunning’s resignation, and that after a change and a rest Mr. Dunning would possibly be able to take up again the duties ,f his department.
The Ministry
However, almost immediately after Mr. Dunning’s resignation was tendered fearing that a situation might arise which would make it imperative to fill the post of Minister of Finance at short notice, I got in touch with my former colleague, Colonel Ralston, to ask him if he would join the ministry and take Mr. Dunning’s portfolio. Colonel Ralston said to me that he had not contemplated returning to public life and would not like to enter the ministry immediately. He volunteered however that- in the event of a.n emergency arising I could count upon him to accept any post the government might wish to offer him. Upon the outbreak of the present emergency, I again got in touch with Colonel Ralston and, as hon. members are aware, he came forthwith to' Ottawa and yesterday was sworn as Minister of Finance in the present administration.
I should like to table the letter which Mr. Dunning wrote me at the time of tendering his resignation and the final reply which I sent to Mr. Dunning yesterday. Without being read, they might be allowed to appear in Hansard.
Ottawa, July 21st, 1939.
My dear Prime Minister:
As you know, I have been endeavouring during the past twelve mouths to recover my health and at the same time carry on the duties and responsibilities of Minister of Finance. During that time you and my colleagues in the cabinet have tried to relieve me as much as possible, and only by reason of that kind assistance have I been able to carry on.
For some time past medical advice has been definite to the effect that I can expect complete recovery only if I free myself from responsibility and work for some time to come. It is evident that I cannot undergo the strain of a general election.
Under the circumstances, I feel it my duty to ask you to accept my resignation as Minister of Finance, effective on a date convenient to you.
In doing so, I wish to thank you and all my colleagues for the kindness and consideration I have received from them during a difficult and trying time.
Yours faithfully,
Charles Dunning.
Ottawa, September 6, 1939.
My dear Dunning:
As you will recall, on July 21st last, you advised me by letter that, owing to the impaired condition of your health, you deemed it your duty to ask me to accept your resignation -as Minister of Finance, effective on a date convenient to myself.
When your letter was received I did not hesitate to say to you that I hoped you would not think of pressing for an immediate acceptance of your resignation, but would take a complete rest and change, to see if, with time, your health might not so improve as to permit
[Mr. Mackenzie King.]
you to continue in the position of Minister of Finance. I have all along hoped that your progress would he sufficiently favourable and rapid as to permit you to reconsider resigning from the ministry.
Were the international situation not what it has become, I would have been prepared to wait some little time longer before finally deciding to act upon your letter. However, with conditions as critical as they are, I feel I must not longer delay in filling the position of Minister of Finance and, at the same time, relieving my colleague, Mr. Ilsley, from continuing to carry on the duties of Minister of Finance in addition to administering the affairs of the Department of National Revenue.
You will be pleased to know that our former colleague, Colonel Ralston, has responded to my urgent request that he should rejoin the ministry and give to the country, as Minister of Finance, the benefit of his exceptional experience and abilities. I have already informed His Excellency the Governor General of my intention to recommend Colonel Ralston for the portfolio mentioned. His Excellency has warmly approved, and I am looking forward to Colonel Ralston being sworn into office this afternoon.
After our close association over many years, and the intimate personal friendship enjoyed with yourself, to say nothing of the invaluable services you have been rendering the government and the country, it is natural that I should feel the deepest regret at the severance of official relations which have been so pleasant and helpful, and which your fine sense of public duty caused you to continue over a period when the condition of your health demanded a complete rest. I am sure the citizens of Canada generally will share the regret of all the members of the cabinet at the loss of your presence at the council table.
I can only hope that, despite the very grave anxieties which have come upon us all since you left on your trip to the old land, you may return much benefited by the change, and that, ere long, your health may he fully restored.
With my warmest regards and wishes, and with an abiding sense of gratitude as well for your loyal cooperation in the affairs of state during the years we have been associated together in the public life of our country.
Believe me, dear Dunning,
Yours very sincerely,
W. L. Mackenzie King.
INTERNAL ECONOMY COMMISSION
Right Hon. W. L. MACKENZIE KING (Prime Minister) presented the following message from His Excellency the Governor General:
The Governor General transmits to the House of Commons a certified copy of an approved minute of council appointing the Honourable T. A. Crerar, Minister of Mines and Resources, the Right Honourable Ernest Lapointe, Minister of Justice, the Honourable W. D. Euler, Minister of Trade and Commerce, and the Honourable J. L. Ilsley, Minister of National Revenue, to act with the Speaker of the House of Commons as commissioners for the purposes and under the provisions of chapter 145 of the revised statutes of Canada, 1927, intituled An Act Respecting the House of Commons.
Tributes to Deceased Members
BUSINESS OF THE HOUSE
CHANGES IN STANDING ORDERS
Right Hon. W. L. MACKENZIE KING (Prime Minister) moved:
That the following changes be made in the standing orders of the house for the duration of the present session:
1.	The house shall meet on every sitting day and the provision of standing order No. 2 relating to the adjournment of the house on Friday shall he suspended.
2.	Standing order No. 6 adjourning the house at six o’clock on Wednesdays shall be suspended and the procedure and order of business on Wednesdays shall in every respect be the same as on other days.
3.	Government notices of motions and government orders shall have precedence over all other business except questions and notices of motions for the production of papers.
4.	Standing order No. 15 relating to the consideration of private and public bills from eight until nine o’clock, p.m., on Tuesdays and Fridays shall be suspended.
5.	Standing orders Nos. 63, 80, 84, 102 and 122 shall be suspended.
These particular standing orders have reference largely to matters of procedure and to other measures that would not be of importance at this special session. Standing order 63 relates to select standing committees. Standing order SO relates to the report of the proceedings for the preceding year of the commissioners of internal economy. Standing order 84 relates to the list of the reports which have to be made to the house. Standing order 102 relates to the introduction of private bills, and standing order 122 relates to the library of parliament. The motion continues:
6.	The provision of standing order No. 46 requiring unanimous consent for a motion in case of urgent and pressing necessity shall be suspended.
7.	Standing orders 69-77, both inclusive, shall be suspended in relation to public bills introduced by private members.
Motion agreed to.
TRIBUTES TO DECEASED MEMBERS
Right Hon. W. L. MACKENZIE KING (Prime Minister): At a time when, everywhere, there is so much anxiety and personal distress, I hesitate to speak of the loss which the membership of this house has sustained in the few months since parliament prorogued. I feel, however, that the members would wish to have on the pages of Hansard some mention of our sense of the loss, alike to parliament and to the country, occasioned by the death, on July 9, of Doctor Alexander MacGillivray Young, and the death, on July 12, of the Honourable Fernand Rinfret.
Doctor MacGillivray Young was the representative of the city of Saskatoon, Saskatchewan. He was a member of this house in two1 previous parliaments and served as a member of parliament altogether for about eight years. Hon. Mr. Rinfret was the representative of the constituency of St. James, Montreal. Mr. Rinfret had been a member of parliament for nineteen years and at the time of his death was Secretary of State for Canada, which position he had also held in a previous administration. He had served as a minister of the crown for nearly eight years.
It must be a comforting thought to the members of their respective families who have been thus bereaved, as it will be to the hon. members of this house with whom they have been in close association over many years, to know that while it was yet day Doctor Young and Mr. Rinfret availed themselves so largely of the opportunity which was afforded them to devote their lives and talents to the service of the state in its halls of parliament, and that on the membership roll of the House of Commons of Canada they have left names which will be long and gratefully remembered.
Hon. R. J. M ANION (Leader of the Opposition): Mr. Speaker, I should like to join wilh the Prime Minister (Mr. Mackenzie King) in expressing to the families of the two deceased members the sincere sympathy of this section of the house. I knew both members well. Mr. Rinfret and I came into the house at the same time, and I knew Doctor Young for very many years. Both were outstanding members of this house who deserved the gratitude of the sections of the country which they represented for the splendid work they did. I want to express to the party to which they belonged the sympathy of this party, and I join with the Prime Minister in sending to the families of the deceased our most sincere sympathy.
Mr. J. S. WOODSWORTH (Winnipeg North Centre): Mr. Speaker, I am sure that the members of our group tvould desire to associate themselves with the expressions of sympathy as given by the Prime Minister (Mr. Mackenzie King) and the leader of the opposition (Mr. Manion). While I did not have a close personal relationship with either of the deceased members, I think those of us who have been in the house a number of years come to realize quite keenly the fine personal relationships which are possible between members of the house even though they differ widely in their opinions.
Mr. J. H. BLACKMORE (Lethbridge): Mr. Speaker, the members of the social credit group desire also to associate themselves with
The Address—Mr. Hamilton
the words of the leaders of the two major parties, and also with the words of my hon. friend, the leader of the Cooperative Commonwealth Federation (Mr. Woodsworth).
On motion of Mr. Mackenzie King the house adjourned at 4 10 p.m.
