PRESENT:
HIS EXCELLENCY
THE GOVERNOR GENERAL IN COUNCIL:
WHEREAS the secretary of state reports that, at the request of the Prime Minister he has convened an informal advisory committee on public records to give consideration to methods for providing adequate conservation of the public records, with particular
reference to those records relating to the wartime activities of the government; and
That, following investigation of the state of the public records, a report has been prepared and considered by the committee and certain recommendations resulting therefrom approved;
Now, therefore, his excellency the governor general in council, on the recommendation of the Secretary of State, is pleased to order and doth hereby order as follows:—
1.	There shall be a committee on public records under the chairmanship of the Secretary of State and consisting of the following officials:—
A representative to be named by the Secretary of State (public archives).
Two representatives to be named by the Minister of National Defence (army and naval services).
A representative to be named by the Minister of National Defence—Air.
A representative to be named by the Minister of Public Works.
A representative to be named by the Minister of Finance.
A representative to be named by the minister of munitions and supply and the minister of reconstruction.
A representative to be named by the Minister of Labour.
A representative to be named by the Secretary of State for External Affairs.
A secretary shall be provided from the privy council office.
2.	The Canadian Historical Association shall be asked to recommend two professional historians to act in an advisory capacity to the committee to serve at no salary but with expenses to be provided by the government.
3.	The duties of the committee shall be to keep under constant review the state of the public records and to consider, advise and concert with departments and agencies of government on the organization, care, housing, and destruction of public records.
4.	The committee shall, as part of their duties, examine and report on the following:
(a)	The preparation by departments and agencies of government of suitable accounts of their wartime activities and,
(b)	The implementing of the approved recommendations of the royal commission on public records of 1914 regarding establishment of a public records office, with particular reference to the integration of the
Questions
public archives therein, and the type of organization which would facilitate the best use of the public records.
5.	When questions specifically affecting the records of a department are being dealt with, a representative from that department shall be present at the meeting.
6.	The primary responsibility for the care and maintenance of records and for seeing that the policies of government in respect to disposition of public records be carried out so as to ensure that material of permanent value be not unwittingly destroyed will rest with departments and agencies of government concerned.
7.	Each department shall assign responsibility for superintendence of its records to one or more senior officers, preferably the departmental secretary if such a position exists, or an official of similar rank. The duties of these officers will be to review periodically the state of the departmental records and to reclassify them with a view to disposal or transfer of those of permanent value but not currently required to the public archives (or public records office, if established) or to other dominion or provincial departments, or by some form of destruction under existing regulations. These officers will also maintain liaison with agencies responsible to the minister. Recommendations respecting contemplated disposal along the above lines shall be submitted, in all cases, for formal approval of the committee on public records.
A. D. P. Heeney,
Clerk of the Privy Council
WHARF REPAIRS, BONAVENTURE COUNTY, QUE.
Question No. 328—Mr. Bourgel:
1.	During the 1957-58 fiscal year, has the Department of Public Works done any work in connection with the wharves at Riviere Caplan, Riviere Caplan (Bourdages), Riviere Caplan (McLelland’s Beach) and St. Charles de Caplan, county of Bonaventure, Quebec?
2.	If so, what are the names of the persons employed for such work and what amount was paid to each one, in each case?
3.	What are the names of the persons who supplied the materials and what amount was paid to each one, in each case?
Answer by: Hon. Howard C. Green (Minister
of Public Works):
1.	Yes.
2.	See attached statement.
3.	See attached statement.
	DEPARTMENT OF		PUBLIC	WORKS		
Location	Name of Employee	Classification Amount Paid Name of Supplier			Material	Amount Paid
Riviere Caplan	Herve Mercier	Foreman	$167.40	Argee Gar ant	Lumber	$341.77
Harbour repairs—■	Emilien Babin	Carpenter	140.00	Willie Manning	Iron	5.50
	Willie Poirier	Labourer	22.80	Argee Garant	Lumber	85.50
	Leonard Poirier	«C	22.80	J. H. Mercier	Nails	45.00
	Felix Bourdages	ft	22.80			
	Roger Arsenault	((	22.80			
	Fortunat Querry	((	15.20			
	Maurice Dion	tf	15.20			
	Gabriel Mercier	ft	53.20			
	Charles N. Babin	if	22.80			
	Simon Poirier	tf	22.80			
	Hubert Bujold	tf	38.00			
	Jos. Dion	if	38.00			
	Ernest Arsenault	ft	7.60			
	Alfred Poirier	ft	7.60			
Riviere-Caplan	Antonio Cormier	Foreman	$241.80	Alfred V. Bourdages	Hauling	$ 7.20
(Bourdages) Wharf repairs	Louis Poirier	Carpenter	112.00	Alfred V. Bourdages	Hauling	32.40
	Louis Loubert	Labourer	148.20	Romeo Bourdages	Timber	73.00
	Alfred V. Bourdages	tf	125.40	Lucien Robichaud	Spikes	8.00
	Heliodore Cyr	ft	125.40	Romeo Bourdages	Stones	8.00
	Pierre Riviere	ft	5.70	La Fraternite Co-Op.	Nails	.76
				Leo Garant	Timber	191.19
				Alfred V. Bourdages	Hauling	4.00
				La Fraternite Co-Op.	Nails	12.50
				Lucien Robichaud	Spikes	20.00
3506	HOUSE OF COMMONS
Questions
Riviere-Caplan (McLellan’s Beach) Landing Repairs—
Everett McLellan	Foreman
Augustin Briere	Carpenter
Raymond Loubert	Labourer
Preston McLellan	ii
Elmer McLelland	it
Roger Arseneault	ti
Raymond Porlier	it
Charles Belanger	ti
Romuald Bourdages	ti
Irenee Briere	it
Maurice Dion	it
Russell McLellan	it
Leopold N. Bourdages	ii
Leonidas Poirier	ti
St-Charles-de-Caplan Wharf Repairs—
Paul N. Poirier Joseph N. Poirier Stephane Lepage Irenee Poirier Valmont Cousin
Foreman
Carpenter
Labourer
it
Eugene Doucet Charles Dion Alban Bujold Robert Henry Jean-Marie Babin Honore Chretien
; 192.20	Rimouski District Office	Bolts	$ 89.60
173.60	Rimouski Transport	Freight	8.45
49.40	J. A. Gendron	Tools	11.70
49.40	Herve Mercier	Stones	126.00
41.80	Rene Briere	Stones	120.00
41.80	Roland Babin	Stones	30.00
76.00	J. Paul Briere	Stones	30.00
53.20	Vincent McLellan	Timber	84.00
64.60	Garage Briere & Manning	Iron, etc.	42.25
26.60	Argee Garant	Timber	582.40
19.00	Argee Garant	Rent of	
53.20		Bulldozer	32.00
34.20			
15.20			
p 223.20	Lionel Miousse	Rent of saw	$ 25.20
100.80	Valmore Cyr & Fils	Hardware	24.50
53.20	J. A. Gendron	Hardware	10.65
68.40	Garage Bourdages	Gas and Oil	2.05
11.40	Paul Poirier	Freight and	
		Express	9.44
11.40	Charles E. Henry	Timber	824.50
57.00	Herve Mercier	Timber	139.50
53.20	La Fraternity Co-Op.	Rope	12.98
49.40	Rimouski Transport	Freight	35.96
41.80	Rimouski District Office	Bolts	351.30
26.60	William Doyle Ltd.	Bolts	105.01
MAY 8, 1959	3507
Questions
HOUSE OF COMMONS
Questions
DRAGGER TYPE VESSELS, ATLANTIC PROVINCES------
FEDERAL ASSISTANCE
Question No. 387—Mr. Robichaud:
1.	What Is the total amount, up to March 31, 1959, paid in each of the Atlantic provinces under the assistance in the construction of vessels of the dragger or long liner type?
2.	What is the amount paid during the same period in Gloucester county, New Brunswick?
Answer by: Hon. J. A. MacLean (Minister of
Fisheries):
1.	New Brunswick ................. $447,051.05
Nova Scotia ..................... 882,580.20
Prince Edward Island ............ 109,824.00
Quebec........................... 162,021.75
Newfoundland .................... 166,701.15
2.	$378,092.10.
HOUSE OF COMMONS
