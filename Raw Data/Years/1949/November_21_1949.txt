The house met at eleven o’clock.
AGRICULTURAL PRODUCTS
PRIZE WINNERS AT TORONTO WINTER FAIR
Mr. M. J. Coldwell (Roselown-Biggar): I rise on a question of privilege, Mr. Speaker. The house will be pleased to know that once again an unique distinction has been conferred upon the constituency of Rosetown-Biggar by one of its farm residents, Mr. Albert Kessel, of Valley Centre, who not only won the silver cup for the northwest championship for his grain, but last week was awarded the world championship for his rye at the annual Royal Winter fair in Toronto. I should add that last year, Mr. Kessel won a championship ribbon and first prize for a sheaf of wheat, and a first for oats at the same winter fair.
I might say also that in 1947 Mr. Albert Robbins of Laura, in the constituency of Rosetown-Biggar, won the grand prize for oats and in 1948 a reserve championship for an exhibit of barley at the Royal Winter fair. I am sure all hon. members will join with me in congratulating these Saskatchewan farmers for their successful efforts to improve the products of the Canadian grain growing industry.
WINNER FOR OATS AND BARLEY
Mr. John Decore (Vegreville): Mr. Speaker, according to newspaper reports, one of my constituents won the world championship with his exhibits of oats and barley at the Royal Winter fair. The name of the winner is John T. Eliuk, and in winning this award he became the first such double winner in this fair. Incidentally, Mr. Eliuk was the oat king at the Chicago World fair in 1948. This is not the first time that some of my constituents engaged in farming have obtained world recognition of their ability.
It is a pleasure for me to record the congratulations of this house to Mr. Eliuk upon obtaining such a high distinction as winning this double award. The constituency which I have the honour to represent is noted not only for the richest and biggest oil field in Canada, but also for the type of farmers who are given world recognition for their efforts.
Mr. J. L. Gauthier (Sudbury): Mr. Speaker, since all these farmers are being mentioned this morning, I believe I should draw the 45781—126
attention of this house to the fact that, although my constituency is one of the richest in minerals in Canada, on Friday Mr. Theo-phile Depatie of Hanmer, Ontario, Sudbury district, was crowned potato king of the world.
Mr. Robert Fair (Battle River): I do not
believe it would be fair to let an occasion such as this pass without joining with the other speakers in their congratulatory messages, and I should like to extend congratulations to the district agriculturist from my home town, Mr. E. H. Buckingham, for the part he played in bringing three firsts to Alberta from the Royal Winter fair at Toronto, and in addition, the high singles in those classes. Mr. Buckingham and some of his associates came east with five young people’s teams, and in taking home these three firsts has conferred quite an honour upon my fellow citizens and his associates from the province of Alberta.
Mr. Knowles: The people in my riding are raising Cain over the lifting of rent control.
AGRICULTURE AND COLONIZATION
CHANGE IN PERSONNEL OF STANDING COMMITTEE
Hon. Alphonse Fournier (Minister of Public Works) moved:
That the name of Mr. Argue be substituted for that of Mr. Herridge in the standing committee on agriculture and colonization.
Motion agreed to.
MOTION FOR LEAVE TO SIT WHILE HOUSE IS SITTING
Mr. A. J. Baler (The Battlefords) moved:
That the standing committee on agriculture and colonization be empowered to sit while the house is sitting.
Motion agreed to.
S. S. "NORONIC"
DESTRUCTION BY FIRE ON SEPTEMBER 17----JUDG-
MENT AND REPORT OF COURT OF INVESTIGATION
Right Hon. C. D. Howe (Minister of Trade and Commerce): For the Minister of Transport (Mr. Chevrier), I wish to give the house a summary of the report of the court of investigation into the loss of the steamship Noronic by fire on September 17 last.
The Minister of Transport informed the house that under the provisions of the Canada Shipping Act he had appointed the Hon.
HOUSE OF COMMONS
5.5.	Noronic
Mr. Justice Kellock, of the Supreme Court of Canada, to act as commissioner and to hold a formal investigation into the loss of that steamship. The commissioner, sitting as a court of investigation, was assisted by Captains H. S. Kane and Robert Mitchell as nautical assessors, and Mr. Neil B. Gebbie as engineer assessor.
Counsel appeared for the Department of Transport; for Canada Steamship Lines Limited, and officers; for the Attorney General of Ontario; for the attorney general of the state of Ohio; and for the Seafarers’ International Union. Mr. W. K. Campbell acted as registrar of the court.
The court held hearings in the city of of Toronto on eighteen days in September, October and November, and the final sitting was held at the city of Ottawa on the 21st day of November, 1949.
Many witnesses were called to give evidence, and ample opportunity was given to everyone who had personal knowledge of the circumstances of the disaster to come forward and testify. I think I may say without fear of contradiction that the investigation was searching, impartial and thorough.
The judgment of the court was pronounced this morning in open court as follows:
Court of Investigation
5.5.	Noronic
