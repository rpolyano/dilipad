The Parliament which had been prorogued from time to time to the 4th day of February, 1915, met this day at Ottawa for the despatch of business.
The House met at Three o’clock, the Speaker in the Chair.
Mr. SPEAKER read a communication from the Governor General’s Secretary announcing that His Royal Highness would proceed to the Senate Chamber at 3 p. m. on this day, for the purpose of formally opening the session of the Dominion Parliament.
A message was delivered by Major Ernest J. Chambers, Gentleman Usher of the Black Rod, as follows:
Mr. Speaker, His Royal Highness the Governor General desires the immediate attendance of this honourable House in the Chamber of the honourable the Senate.
Accordingly the House went up to the Senate Chamber.
And the House being returned to the Commons Chamber:
THE GOVERNOR GENERAL’S SPEECH.
Mr. SPEAKER: I have the honour to inform the House that the House having attended His Royal Highness in the Senate Chamber, His Royal Highness was pleased to deliver a Speech to both Houses of Parliament. To prevent mistakes I have obtained a copy, which is as follows:
Honourable Gentlemen of the Senate:
Gentlemen of the House of Commons:
During the months which have elapsed since the outbreak of war, the people of Canada have 1
given most abundant and convincing evidence of their firm loyalty to our Sovereign and of their profound devotion to the institutions ol the British Empire.
Since I last addressed you, a Canadian Expeditionary Force of more than thirty thousand men has been safely despatched across the Atlantic, and after arriving in the British Islands has been engaged in completing the necessary training before proceeding to the front. Notwithstanding the unusually severe weather conditions which have prevailed in the British Islands, the training has proceeded satisfactorily and it is anticipated that the force will very shortly take its place in the field of action.
The earnest and resolute spirit of patriotism which animates the whole Dominion has evoked a magnificent response to the call for service beyond the seas. Large additional forces have been organized from which further contingents are ready to be despatched as soon as the necessary arrangements for receiving them and completing their training can be consummated.
Notwithstanding the inevitable disturbance of trade which was created by the outbreak of war on so vast a scale, the financial and business conditions of the Dominion have shown great stability, and on the whole the country has adapted itself to the new conditions in a very effective way.
My advisers will submit for your consideration measures rendered necessary by the participation of this Dominion in the great task which our empire has undertaken in this war.
Gentlemen of the House of Commons:
The accounts for the last fiscal year will be laid before you immediately and the estimates for the next fiscal year will be submitted without delay. You will be asked to make the necessary financial provision for effective aid in the conduct of the war.
Honourable Gentlemen of the Senate: Gentlemen of the House of Commons:
The strong unity of purpose which inspires His Majesty’s Dominions gives us the firm assurance that the cause for which this war has been undertaken will be maintained to an honourable and successful issue. I commend to your favourable consideration the measures which will be submitted to you for aiding that great purpose, and I pray that the Divine blessing may be vouchsafed to your deliberations.
On motion of Sir George Foster it was ordered that the Speech of His Royal Highness the Governor General to both Houses
REVISED EDITION
of Parliament be taken into consideration on Monday next, and that this order have precedence over all otner business except introduction of Bills until disposed of.
SELECT STANDING COMMITTEES.
Sir GEORGE FOSTER: In the absence of the First Minister, I beg to move:
That a special committee be appointed to prepare and report with all convenient speed lists of members to compose the Select Standing Committees of this House under Rule 10 ; said committee to be composed of Sir Robert Borden, Sir Wilfrid Laurier, Messrs. Reid (Grenville), Casgrain, Pugsley, Stanfield and Pardee.
Motion agreed to.
BILL INTRODUCED.
Bill No. 1, respecting the Administration of Oaths of Office.—Sir George Foster.
VACANCIES.
Mr. SPEAKER: I have the honour to inform the House that during the recess I received communications from several members notifying me that the following vacancies had occurred in the representation, viz.:
Of Hon. Louis Philippe Pelletier, member for the Electoral District of Quebec County, by resignation.
Of Pierre Edouard Blondin, Esq., member for the Electoral District of Champlain, consequent upon his acceptance of an office of emolument, to wit: Minister of Inland Revenue.
Of Hon. Wilfrid Bruno Nantel, member for the Electoral District of Terrebonne, consequent upon his appointment as a member and deputy chief commissioner of the Board of Railway Commissioners.
Of James McKay, Esq., member for the Electoral District of Prince Albert, consequent upon his appointment as a judge of the Supreme Court of Saskatchewan.
Of Thomas Beattie, Esq., Member for the Electoral District of London, by decease.
Of George A. Clare, member for the Electoral District of South Waterloo, by decease.
I accordingly issued my several warrants to the Clerk of the Crown in Chancery to make out new writs of election for the said electoral districts respectively.
[Sir G. E. Foster.]
NEW MEMBERS.
Mr. SPEAKER: I have the honour to inform the House that during the recess the Clerk of the House received from the Clerk of the Crown in Chancery certificates of the election and return of the following members:
Of Hon. Pierre Edouard Blondin, member for the Electoral District of Champlain.
Of Hon. Thomas Chase Casgrain, for the Electoral District of Quebec county.
Of William Gray, Esq., for the Electoral District of London.
Of Frank Stewart Scott, Esq., for the Electoral District of South Waterloo.
Of Joseph A. Descaries, Esq., for the Electoral District of Jacques Cartier.
Of Arthur Bliss Copp, Esq., for the Electoral District of Westmorland.
MEMBERS INTRODUCED.
Hon. Pierre Edouard Blondin, for the Electoral District of Champlain, introduced by Sir George Foster and Hon. C. J. Doherty.
Hon. Thomas Chase Casgrain, for the Electoral District of Quebec county, introduced by Sir George Foster and Hon. C. J. Doherty.
Frank Stewart Scott, Esq., for the Electoral District of South Waterloo, introduced by Sir George Foster and Mr. W. G. Weichel.
On motion of Sir George Foster, the House adjourned at 3.35 p.m. until Monday next.
