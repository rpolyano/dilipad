The SPEAKER took the Chair at Three o’clock.
REPORT PRESENTED.
Report of the Department of Public Print ing and Stationery.—Rt. Hon. Sir Wilfric Laurier.
BROCKVILLE AND NORTH-WEST RAILWAY COMPANY.
may be transmitted to tlie proper I therefore beg to move :
quarter.
Mr. CALVERT, on behalf of the lion, member for Algoma (Mr. Dymeut), I beg Cor leave to introduce Bill (No. 89) entitled an Act to incorporate the Brockville and North-west Railway Company.
Mr. TAYLOR. I beg leave to call to your attention, Mr. Speaker, that the petition for this Bill was only presented to-day.
Mr. SPEAKER. I think everything is all right. The Bill has been reported.
Mr. TAYLOR. The petition was just read now.
Mr. SPEAKER. I have a note from the Examiner of Private Bills that everything in connection with this is correct. I take it that the petition has been duly received.
Motion agreed to, and Bill read the first time.
FIRST READINGS.
Bill (No. 88) respecting the Mutual Fire Insurance Company of the city of Montreal and to change its name to the Montreal Fire Insurance Company.—Mr. LaRiviSre.
Bill (No. 90) respecting the Niagara Welland Power Company, Limited, and to change its name to the Niagara Welland Power Company.—Mr. Guthrie (by Mr. Calvert).
Bill (No. 91) to incorporate the Dominion Gas Improvement Company.—Mr. Belcourt, by Mr. Campbell.
Bill (No. 92) respecting the Dominion Burglary Guarantee Company, Limited, and to change its name to the Dominion Guarantee Company.—Mr. Bickerdike.
Bill (No. 93) to incorporate the Shipping Federation of Canada.—Mr. Bickerdike.
Bill (No. 94) respecting incorporated Companies.—Mr. Bickerdike.
CONCILIATION ACT AMENDMENT.
Mr. PUTTEE moved for leave to introduce Bill (No. 95) to amend the Conciliation Act, 1900.
Mr. INGRAM. Explain.
Mr. PUTTEE. The Bill is similar in all respects to the Bill introduced by myself last session, and explained then. It simply seeks to increase the utility of the Conciliation Act of 1900.
Motion agreed to, and Bill read the first time.
HOME RULE FOR IRELAND.
The PRIME MINISTER (Rt. Hon. Sir Wilfrid Laurier). A few days ago the House adopted a resolution which requires some further proceedings in order that it
That the Address adopted by this House on Tuesday, the 31st ult., to His Most Gracious Majesty the King be engrossed.
Motion agreed to.
The PRIME MINISTER moved :
That an Address be presented to His Excellency the Governor General, praying that he will be pleased to transmit the Address adopted by this House on Tuesday, the 31st ult. to His Most Gracious Majesty the King, in relation to affairs in Ireland, in such manner as His Excellency may see fit, so that the same may be laid at the foot of the Throne.
Motion agreed to.
The PRIME MINISTER moved :
That the said Address be engrossed and be presented to His Excellency by such members of this House as are members of the Honourable the Privy Council.
Motion agreed to.
QUESTIONS.
PISHING RIGHTS IN JAMES BAY OR HUDSON BAY.
Mr. LANCASTER—by Mr. Taylor—asked:
1.	Who are the parties to whom exclusive fishing rights in any portion of James Bay or Hudson Bay were recently granted ?
2.	By whom was the application for such privilege made to the government or any department thereof.
3 AVhat is the date of the first application ?
4. What is the date of the lease or privilege ?
5.	What is the duration of such lease or privilege ?	.
6.	What is the rental to be paid by the lessees.
7.	Has the said lease been transferred. If so, to whom ?
S. When was the transfer of the lease consented to or sanctioned, and by what department ?
Tlie MINISTER OF MARINE AND FISHERIES (Hon. J. Raymond Pr6fon-taine) :
1.	Archibald McNee.
2.	Archibald McNee.
3.	22nd February, 1900.
4.	13th November, 1902.
5.	Twenty-one years.
6.	Ten dollars per annum.
7.	No information.
8.	28th November, 1902. By the Department of Marine and Fisheries.
OROMOCTO SHOALS, N. B., LIGHT-KEEPER. Mr. WILMOT asked :
1.	Has Miles McMonagle been dismissed from the position of keeper of Beacon Light, Oro-moeto Shoals, N.B.? If so, when, and for what cause ? Is it known to the department that Inspector Kelly stated that McMonagle was the most efficient light-keeper on the river St. John ?
The MINISTER OF MARINE AND FISHERIES (Hon. J. Raymond Prefon-taine). Mr. McMonagle was superseded by Mr. Robert Brennan, on the 25tli March last, as he took an active part in the elections. The department is not aware of any report made by Inspector Kelly in regard to Mr. McMonagle being the most efficient light-keeper on the River St. John.
Mr. WILMOT asked :
Who recommended the dismissal of Miles McMonagle, light-keeper, Oromocto, N.B.?
The MINISTER OF MARINE AND FISHERIES (Hon. J. Raymond Prefon-taine). In this department all such recommendations are made by the Minister of Marine and Fisheries, as usual.
AYR POSTMASTER.
Mr. CLARE—by Mr. Taylor—asked :
Did Michael Robson, formerly postmaster of the village of Ayr, resign, or was he dismissed? If dismissed, why ?
The POSTMASTER GENERAL (Hon. Sir Wm. Mulock). Mr. Michael Robson resigned the postmastership of Ayr.
THE TREADGOLD CONCESSION.
Mr. BELL—by Mr. Taylor—asked :
1.	Has any member of the government received the following telegram ?
Oppose any attempt to pass Order in Council or Act of parliament that will in any way confirm Treadgold concession. Are having monster petition signed and reliable data prepared to forward Ottawa asking government to aid in furnishing water for mining purposes. People are a unit in making demand, and will insist upon government protection from monopoly of Treadgold octopus.
THE DAWSON BOARD OF TRADE.
H. C. Macaulay, President.
2.	Is the government aware that a petition, a copy of which is hereinafter given, is being forwarded ?
PETITION.
To the honourable the House of Commons In parliament assembled;
The petition of the undersigned residents of the Yukon Territory humbly showeth :
1.	That by Order in Council of April the 21st, 1»02, certain privileges are granted to Malcolm Orr Ewing, A. N. C. Treadgold and Walter Bar-wick, in connection with the proposed establishment by them of a system of water supply for washing out gold-bearing gravel in the district therein described, including the Klondike river Bonanza, Bear and Hunker Creeks and their tributaries.
2.	That the benefits conferred upon the grantees are of incalculable value and involve an enormous exploitation of the public resources of this territory for the -benefit of a few favoured concessionnaires.
3.	That in the opinion of your petitioners the accumulation of extraordinary powers in the hands of a single corporation, such as Is effected by the above Order in Council will lead to the paralysis of the independent commercial and industrial life of the community
Mr. WILMOT.
and will prove in the highest degree oppressive and injurious to the public welfare, since the grantees are thereby enabled to crush out competition and to reduce to a position of practical servitude the individual miners in the extensive district affected, which includes the richest portion of the Klondike.
4.	That the need of this territory is not the creation of monopolies, but their prevention and the encouragement of the individual miners by securing equal privileges and opportunities to all as far as the law and the administration can provide them.
5.	That for the promotion and development of the mining industry of the Yukon a cheap, abundant and effective water supply, furnished at a minimum of cost by the government at the earliest possible moment, is absolutely essential.
Your petitioners therefore pray ;
(1.) That the Order in Council of April 21st, 1902, may be cancelled completely, and that no special privileges shall hereafter be granted within this territory with respect to wood, mining, water or any other class of rights affecting the general public ; but that all persons shall be restricted in such matters to the rights conferred upon every member of the community by the mining regulations.
(2) That the supply and distribution of water for general mining purposes within this territory shall not be controlled by any private person or corporation, but either that it shall be undertaken by the Dominion government as a public work, or that power shall be given to the commissioner of the Yukon territory in Council to construct such a system and to raise the necessary funds by bonds guaranteed by the Dominion.
And your petitioners will ever pray.
The PRIME MINISTER (Rt. Hon. Sir Wilfrid Laurier) :
1.	Several members of the government have received this telegram, amongst others the Prime Minister, who answered that there would be no attempt to pass an Order in Council or Act of parliament in the manner suggested in the telegram.
2.	The government has been informed that such a petition, or one similar to it, is to be addressed to the government. I may say, in addition, that this matter has been brought to the attention of the government by the hon. member for the Yukon (Mr. Ross) ; but all consideration has been suspended until this petition has been received.
NESBITT, MANITOBA, POSTMASTER.
Mr. ROCHE (Marquette) asked :
1.	Is the government aware that Mr. John Watson, postmaster at Nesbitt, Manitoba, is the president of the South Lambton Liberal Association ; and the Liberal candidate for that constituency in the ensuing elections in that province ?
2.	Will his services he continued by this government while he is engaged in party politics?
The POSTMASTER GENERAL (Hon. Sir Wm. Mulock). The department has no information whatever as to whether Mr. John Watson is the President of the South Brandon Liberal Association or the Liberal
APRIL 8. 1903
candidate for that constituency in tile ensuing elections in that province.
LAND SUBSIDIES TO RAILWAYS.
Mr. DAVIS asked :
1.	What is the total amount of the lands granted in Manitoba and the North-west Territories to railway companies by way of subsidies to date ?
2.	Were any lands granted by way of subsidies to railway companies since 1896 ?
The MINISTER OF RAILWAYS AND CANALS (Hon. A. G. Blair) :
1.	Total area authorized by parliament to be granted by way of railway subsidies, 56,087,072 acres.
Total area earned on account of construction to date, 29,986,826 acres.
2.	No.
MONEY SUBSIDIES TO RAILWAYS.
Mr. DAVIS asked :
What is the total cost (capital account) of the Intercolonial and other government railways ? What is the total amount of money paid by the federal government as subsidies to railways in each of the following provinces :— Prince Edward Island, New Brunswick, Nova Scotia, Quebec, Ontario, Manitoba, North-west Territories, and British Columbia, since confederation ?
The MINISTER OF RAILWAYS AND CANALS (Hon. A. G. Blair) :
The total cost (capital account) of the Intercolonial and other government railways to February 28th, 1903, is as follows : Intercolonial Railway, $69,418,275.54; Prince Edward Island Railway, $5,112,435.27.
The total amount of money paid by the federal government as subsidies to railways in each of the following provinces since confederation to the 28th of February, 1903, is as follows :
Prince Edward Island......... Nil.
Nova Scotia..................$ 1,872,503 83
New Brunswick.................. 1,292,783	57
Quebec........................ 10,091,552	63
Ontario....................... 18,750,228	48
Manitoba....................... 3,259,150	00
North-west Territories..	..	7,457,500	00
British Columbia..	..	-..	..	9,682,251	00
CAPITAL EXPENDITURE ON PUBLIC WORKS.
Mr. DAVIS asked :
What is the total amount of money spent by the federal government (capital account) on public works of all descriptions, not including canals, from the year 1892 to 1902, respectively, in the provinces of Ontario, Quebec, Nova Scotia, New Brunswick, Prince Edward Island, Manitoba, North-west Territories and British Columbia ?
The MINISTER OF PUBLIC WORKS (Hon. James Sutherland). I would ask the Iron, member to move for a return.
MR. S. JONASSON.
Mr. ROCHE (Marquette) asked :
1.	Is Mr. S. Jonasson an employee of the Department of the Interior, in the province of Manitoba, as forest ranger, or homestead inspector ?
2.	If so, is the government aware that he is the Liberal candidate for the constituency of Gimli, in the ensuing provincial elections ?
3.	Will his services be continued by this government while he is engaged in party politics?
The POSTMASTER GENERAL (Hon. Sir William Mulock):
1.	Yes.
2.	The government has no information that Mr. Jonasson is a candidate for Gimli.
3.	If he is a candidate, he will not be continued as a salaried official of the department.
BANANAS IMPORTED.
Mr. ROBINSON (Elgin) asked :
What quantity of bananas were imported into the Dominion during the year 1902 ? What was the value of the bananas imported during the year 1902 ?
The MINISTER OF CUSTOMS (Hon. William Paterson) : 765,767 bunches of bananas, valued at $738,168, were imported into Canada during the fiscal year ended 30th June, 1902.
RAILWAY SUBSIDIES.
Mr."SCOTT asked:
1.	What is the total sum of money which has been paid as subsidies to aid construction of railways in Canada, between Lake Superior and the eastern boundary of British Columbia ?
2.	Was such sum wholly paid on capital account ; and if so, what is the annual interest charge paid by the Dominion on the amount ?
3.	What is the total sum paid by the Dominion, since confederation, to aid railway construction, including the Canadian Pacific Railway ?
4.	What is the interest charge annually ?
The MINISTR OF RAILWAYS AND
CANALS (Hon. A. G. Blair):
1.	The total sum of money paid as subsidies to aid construction of railways in Canada between Lake Superior and the eastern boundary of British Columbia is,— $12,465,341.
2.	Railway subsidies are charged in the public accounts as a special item. It is not designated as a capital account but practically it amounts to such, inasmuch as it is not charged against the ordinary income of tlie year.
3.	The total sum paid by the Dominion since confederation to aid railway construction, including the Canadian Pacific Railway, is $152,922,163.25.
4.	There is no special rate of interest applying to railway subsidies. They are paid out of the general treasury. Moneys are not specifically borrowed for railway sub-
sidies. Loans have been issued from time to time, part of which have gone into railway subsidies. The rates of interest have varied.
B.O. BTSHERIES REPORT.
Mr. EARLE. Before the Orders of the Day are called, I would like to ask the hon. Minister of Marine and Fisheries when we may expect the report of the commission appointed to investigate into the fisheries on the British Columbia coast.
The MINISTER OF MARINE AND BTSHERIES. In a few days.
CATTLE-GUARDS.
Mr. BORDEN (Halifax). Will the hon. Minister of Railways and Canals inform us when the report of the commission respecting cattle-guards may be expected ?
The MINISTER OF RAILWAYS AND CANALS. The tests are going on from day to day as rapidly as possible, but I do not think any report can be made for ten or twelve days. There were sixty-one devices to be tested on Thursday and one or two more have come in since.
Mr. BORDEN (Halifax). I was under the impression that the commission had been disbahded and that some members had retired from it, and that was why I thought a report might be expected.
The MINISTER OF RAILWAYS AND CANALS. The commissioners have .travelled over the principle railways in the United States and Canada and have examined the cattle-guards in use. After their return to Ottawa, the commission disbanded, and Mr. Holt was to carry on the tests. At that time there were only a few guards to be tested, but a number of others have been brought forward and the tests have been continued from day to day.
TRANSPORTATION COMMITTEE.
Mr. BORDEN (Halifax). The announcement is made in the press of the appointment of a commission on the transportation question, mentioned in the speech from the Throne. Has the right hon. gentleman any announcement to make ?
The PRIME MINISTER. Not to-day. The commissioners have not yet been appointed.
RAILWAY SUBSIDIES.
Mr. J. CHARLTON (North Norfolk) moved the third reading of Bill (No. 2) to amend chapter S of the statutes of 1900 authorizing the granting of railway subsidies.
Hon. Mr. HAGGART. The third reading was postponed the other day. Is this Bill intended to apply to any particular road ?
Mr. CHARLTON. It will apply to any road which happens to have a total subsidy Hon. Mr. BLAIR.
of less than $00,000. The object is to place roads of that category on the same level as larger concerns. A road with less than a subsidy of $60,000 might be almost completed, and might be better deserving of the payment of a subsidy than the larger concern; but it is barred by the statute as it exists. It is for the purpose of correcting this discrimination, if we may term it so, against the smaller road that this is introduced.
Hon. Mr. HAGGART. I thought it had some application at the present moment, as the Minister of Railways and Canals said he was seriously contemplating bringing iu a Bill to abolish that clause from the Railway Act.
Mr. CHARLTON. This Bill would not be before the House if the provision of the law had been abolished.
Motion agreed to ; Bill read the third time, and passed.
ELECTORAL CORRUPTION.
House went into committee on Bill (No. 3) to amend the Dominion Elections Act, 1900.—Mr. Charlton.
On section 1.
Mr. CHARLTON. Mr. Chairman, we considered tlie subsections of this section as far as ‘ L ’ when the House was in committee on this Bill before. I would suggest that
you read from subsection * I,.’
Mr. INGRAM. Oh, no.
Mr. CHARLTON. However, it is immaterial to me—read all the subsections.
The PRIME MINISTER (Rt. Hon. Sir Wilfrid Laurier). My hon. friend the Minister of Finance (Hon. Mr. Fielding) made a suggestion the other day which seemed to be received with very general acceptance that a committee should be appointed representing both sides of the House to consider the whole of the election law. The government intend to proceed upon this suggestion after the Easter recess. Perhaps the hon. member for North Norfolk (Mr. Charlton) would allow this matter to stand until the committee is struck, when the whole matter can be considered.
Mr. CHARLTON. I shall be most happy to accede to the request of the Prime Minister (Rt. Hon. Sir Wilfrid Laurier). This disposition of the matter will exactly suit my views. I had thought, however, of saying a few words on the portion of the Bill which was not discussed when we were in committee before, and, with the permission of the committee, I will deal very briefly with the portion after subsection ‘ L.’ The Minister of Justice (Hon. Mr. Fitzpatrick) raised the objection to some portions of the Bill previously discussed that it was merely a repetition, in point of
fact, of provisions already contained in the election law. 1 shall not refer to that now, as we are not to take final action on the Bill at the present time. Subsection ‘ L ’ is designed to deal with the question of political assessments, an evil which has not attained any great magnitude in Canada, but has become a very great evil in the United States, a method which was most effectual in securing election funds, but which inflicted great hardship upon civil service employees. This subsection puts this offence in the same category as the others, and makes the offender subject to fine and imprisonment. At the conclusion of subsection 1. after subsection ‘ L,’ provisions are made with regard to punishment and the provisions of the law in that respect are changed. All of these offences— and subsection 112 is embodied in this list of offences—are made punishable by a maximum term of two years’ imprisonment, and a fine of $500. The penalty is within the discretion of the trial judge. Of course there will be varying degrees of turpitude and variations of guilt in connection with these matters ; and, it must be, I imagine, left to the discretion of the judge. It is the intention of the law that the fine shall be, either entirely or in part payable to the informer ; and reliance is placed upon tins feature of the Bill to secure information and put an end to this nefarious system of corruption by purchase of votes. I think that, perhaps the most important feature of the Bill, the feature that we may most rely upon to end these practices, is section 108a. This is copied, I may say, from the penal code of the state of New York. In that state, many years ago, the evil of bribery of electors was a very great one, and good men there at that time, as good men now In Canada, felt that it was an evil that must in some way be coped with and stamped out. It was found very difficult to reach it. Finally, this provision was adopted, making it the right of any person to inform, and placing before that person the inducement to inform of sharing the fine. When the information is laid, any person can be subpoenaed, and testimony given by the informer, or by any witness, exonerates the person giving it from all legal proceedings. He can plead the evidence given in this regard as a bar to indictment or proceedings at any time in the future. The operation of this section is to place every man who is handling election funds and buying voters, at the mercy of the scalawags and electoral prostitutes with whom he deals ; and the business is so perilous that no man of common sense will engage in it. It has put an end, in the state of New York to this practice. In contrast with this condition of things in the state of New York is the condition existing in some of the smaller states, where no law of this nature •exists. I noticed the other day an article
in one of the American magazines upon political corruption in the state of Rhode Island. There are no laws there for the suppression of bribery, and bribery has become so general that It is deemed by a very large proportion, probably more than half the voters in the state, a legitimate and welcome source of revenue. And public opinion is so strong in favour of the practice that it would be worth a minister’s position as pastor to denounce it from the pulpit. On one occasion in the last election in the state of Rhode Island, at a place called North Smithfield, the ‘ floaters ’—the floating vote—were manipulating and balancing for the highest bids from the managers of the two parties ; and, finally to settle the question, the whole batch was put up at auction at a place near the steps of the city hall and bid off at $25 a head. The price received by the voters in Rhode Island, I think, has been exceeded by some in Canada at some of the late elections. We have not arrived at the stage where a public auction of votes is held, but we have private competition which is as effective. The tendency of things in this country is rapidly approaching that position when public virtue, the sense of the sanctity of the ballot and the deep obligation resting upon the citizens in the exercise of the franchise will be dissipated and gone. We require some legislation in this country, some method by which an end may be put to this practice ; and I rely upon this section 108a to accomplish this work perfectly. In addition to this, we must have better provisions for enforcing the law. It ought not want to be as expensive as it is now. The probability is that the committee that is to be struck may decide upon some official to act as public prosecgtor in the ridings, whose business it will be to look after violations of the electoral law and acts of electoral corruption, in which case the law will be in such a shape that justice can be speedily administered, and we will get rid of the mockery of saw-offs and all these arrangements which make a mockery of the law with regard to electoral corruption. I shall not detain the Honse further in a discussion of this question than to again express my belief that the section incorporated here from the penal code of the state of New York looking at the effect it has had there, will be surely effectual in putting an end to this practice.
Hon. Mr. HAGGART. Do I understand the lion, gentleman to say that lie has adopted some penal clause of the election Act of New York
Mr. CHARLTON. Yes, I will read it to the lion, gentleman. It is copied from the Penal Code of New York.
A person offending against any provision of the next preceding section of this Act
That is, a provision against the purchase or sale of votes.
COMMON’S
—is a competent witness against another person so offending, and may be compelled to attend and testify on any trial, hearing, proceeding, or investigation in the same manner as any other person. The testimony so given shall not be used in any prosecution or proceeding, civil, or criminal against the person testifying. A person testifying shall not therefore be liable to indictment, prosecution or punishment for the offence with reference to which his testimony was given, and may plead or prove the giving of testimony accordingly in bar of such indictment or prosecution.
Now, they have had several years experience of this law in the state of New York, and the result of it in practice is that that class of persons who handle the funds to purchase voters lay themselves absolutely at the mercy of that conscienceless class of men with whom they deal. There is no statute .of limitation to one year or three years, it is an offence that never outlaws.
Mr. FRASER. Does not the lion, gentlemen think that there ought to be a correlative punishment, and that the man who took the bribe should be punished as well as the man who gives it ?
Mr. CHARLTON. The man who takes the bribe is equally liable to punishment, unless he happens to be in the category of persons turning state’s evidence.
Mr. FRASER. I thought he said the man who took a bribe might go and lay the information and go scott free.
Mr. CHARLTON. Any person that has taken a bribe or given a bribe is liable to punishment.
Hon. Mr. HAGGART. I understood that in the Weldon Act, which was embodied in our Election Act, the party who took the In ibe was liable to a punishment as severe as the man who gave it. I do not understand that he could go into court after the lapse of time and make a prosecution.
Mr. CHARLTON. The section as it stands leaves the matter open for any person who has offended in any particular against the law, either by paying bribes or receiving bribes, any person who has violated the law, to enter a prosecution and give evidence. However, those are matters of detail to be looked into after we come to the consideration of the Bill. I do not know what the intention of the government is, whether it will strike a committee now or to do so after the Easter recess. But I desire to express my concurrence with the views expressed by the Prime Minister and by the Minister of Finance as to the method the government deems proper to adopt in dealing with this question.
Progress reported.
MOTION AGREED TO WITHOUT DISCUSSION.
Statement showing the amount expended each year since the 30th June, 1890, on public works Mr. CHARLTON.
of all kinds in Toronto, including Toronto harbour ; showing the nature of each class of work in respect of which such expenditure has been made.—Mr. Grant.
ADJOURNMENT—BUSINESS OF THE HOUSE.
The PRIME MINISTER (Rt. Hon. Sir Wilfrid Laurier). I move that the House do now adjourn. If it is convenient for my lion, friend the leader of the opposition, I intend, when the House resumes next Tuesday, -to take up the Redistribution Bill.
Mr. BORDEN (Halifax). I think that will be quite convenient.
Motion agreed to, and House adjourned at 3.55 p.m.
