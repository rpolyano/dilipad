The Parliament which had been prorogued from time to time to the 1st day of September. 1919. met this day at Ottawa for the despatch of business.
The House met’ at Three o’clock, the Speaker in the Chair.
Mr. Speaker read a communication from Mr. Arthur F. Sladen, Deputy of the Governor General’s Secretary, announcing that His Excellency the Governor General would proceed to the Senate Chamber at 3 p.m., on this day, for the purpose of formally opening the session of the Dominion Parliament.
A message was delivered by Colonel Ernest J. Chambers, Gentleman Usher of the Black Rod, as follows:
Mr. Speaker, His Excellency the Governor General desires the immediate attendance of this honourable House in the Chamber of the honourable the Senate.
Accordingly the House went up to the Senate Chamber.
And the House being returned to the Commons Chamber:
VACANCIES.
Mr. SPEAKER: I have the honour to inform the House that during the recess I received communications from several members, notifying me that the following vacancies had occurred in the representation, viz:
Of William Folger Nickle, Esq:. Member for the Electoral District of Kingston, by resignation;
Of Simon Fraser Tolmie, Esq., Member for the Electoral District of Victoria City, consequent upon the acceptance of an office of emolument under the Crown, to wit: the office of Minister of Agriculture;
Of John Gillaniers Turriff, Esq., Member for the Electoral District of Assiniboia, consequent upon being summoned to the Senate;
Of the Right Honourable Sir Wilfrid Laurier, G.C.M.G., Member for the Electoral District of Quebec, East, by decease;
Of Joseph Read, Esq., Member for the Electoral District of Prince, (P.E.I.), by decease;
Of Hon. Frank Broadstreet Carvell, Member for the Electoral District of Victoria and Carleton, consequent upon the acceptance of an office of emolument under the Crown.
I accordingly issued my several warrants to the Clerk of the Crown in Chancery to make out new writs of election for the said electoral districts respectively.
OATHS OF OFFICE.
Bill No. 1, respecting the Administration of Oaths of Office, introduced and read the first time.—Sir George Foster.
THE GOVERNOR GENERAL’S SPEECH.
Mr. SPEAKER: I have the honour to inform the House that when the House did attend His Excellency the Governor General this day in the Senate Chamber, His Excellency was pleased to make a speech to both Houses of Parliament. To prevent mistakes, I have obtained a copy, which is as follows:
Honourable Gentlemen of the Senate:
Gentlemen of the House of Commons:
In this, his first visit to our Dominion, His Royal Highness the Prince of Wales at once renews happy associations with his comrades of the Canadian army, and at the same time undertakes the important duty of making himself acquainted at first hand with the resources and development of our country, and with the ideals and aspirations of our people. The warm and sincere welcome which everywhere greets him is an assurance that the ties which unite our country with the Motherland and the other dominions in a great community of nations were never closer or firmer than they are to-day.
The urgency of proceeding immediately to the consideration of the Treaty of Peace between the Allied and Associated Powers and Germany, signed at Versailles on the 28th day of June, 1919, has compelled me to summon you to renewed labours which I trust will not be of long duration.
My advisers are of the opinion that this treaty ought not to be ratified on behalf of
REVISED EDITION
Canada without the approval of Parliament. Authenticated copies will be placed before you without delay for your consideration.
In addition you will be aslced to direct Jour attention to other measures, including those rendered immediately necessary by the approaching return of peace and by the terms of the Peace Treaty.
Gentlemen 0/ the House 0/ Commons:
Estimates will be laid before you making such financial provision as may be required in connection with the Peace Treaty and for other purposes.
Honourable Gentlemen of the Senate:
,Gentlemen of the House of Commons:
For more than five years the world has endured the devastation and horror of war forced upon it by an intolerable spirit and purpose of aggression. Fortunately our country has been spared the desolation and ruin which have been inflicted upon many other nations: but our participation in the war has involved heavy burdens and vast sacrifices which our people have borne with an unflinching spirit. With reverent thankfulness we realize that the world emerges victorious from its long struggle against the forces of barbarous militarism and savage aggression. The labours of reconstruction may be difficult and even painful; and we must undertake them with the same united resolve and inflexible purpose as sustained our efforts during the years of conflict. To you and to the great nation whose affairs are committed to your charge. I bid God-speed in all your endeavours.
Sir GEORGE FOSTER (for Sir Robert Borden):	I beg to move, seconded by the
hon. Mr. Reid, that the Speech of His Excellency the Governor General to both Houses of Parliament be taken'into consideration on Tuesday next. .
Motion agreed to.
SELECT STANDING COMMITTEES.
Sir GEORGE FOSTER moved:
That a Special Committee be appointed to prepare and report, with all convenient speed, lists of members to compose the Select Standing Committees of this House under Rule 10, said committee to be composed of Messieurs Reid (Grenville), McKenzie, Calder, McCoig, Middlebro and Robb, and that that portion of Rule 10 limiting the number of members of the said committee be suspended in relation thereto.
Motion agreed to.
REPORT.
Joint report of the librarians of Parliament.—Mr. Speaker.
DEPUTY SERJEANT-AT-ARMS.
Mr. SPEAKER: I have the honour to inform .the House that the Serjeant-at-Arms, with my approval has appointed Louis
[Mr. Speaker.]
Charles Panet, Esquire, to be his deputy during the present session of Parliament.
On motion of Sir George Foster, the House adjourned at 3.27 p.m.
