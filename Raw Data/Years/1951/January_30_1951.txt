FOURTH SESSION—TWENTY-FIRST PARLIAMENT-OPENING
The parliament which had been prorogued on the twenty-ninth day of January, 1951, met this day at Ottawa for the dispatch of business.
The house met at three o’clock, the Speaker in the chair.
Mr. Speaker read a communication from the Governor General’s Secretary, announcing that His Excellency the Governor General would proceed to the Senate chamber at three o’clock on this day, for the purpose of formally opening the session of the dominion parliament.
A message was delivered by Major C. R. Lamoureux, Gentleman Usher of the Black Rod, as follows:
Mr. Speaker, His Excellency the Governor General desires the immediate attendance of this honourable house in the chamber of the honourable the Senate.
Accordingly, Mr. Speaker with the house went up to the Senate chamber.
And the house being returned to the Commons chamber:
OATHS OF OFFICE
Right Hon. L. S. Si. Laurent (Prime Minister) moved for leave to introduce Bill No. 1, respecting the administration of oaths of office.
Motion agreed to and bill read the first time.
SPEECH FROM THE THRONE
Mr. Speaker: I have the honour to inform the house that, when the house did attend His Excellency the Governor General this day in the Senate chamber, His Excellency
80709—1
was pleased to make a speech to both houses of parliament. To prevent mistakes, I have obtained a copy, which is as follows:
Honourable Members of the Senate:
Members of the House of Commons:
Since you met in special session in the autumn of last year, there has been a further deterioration in the international situation. The intervention of Chinese forces in active opposition to the United Nations forces in Korea has increased the danger of a general conflagration.
So far the efforts to achieve a peaceful settlement in the Far East have not succeeded. While aggression cannot be condoned and will continue to be resisted, it is the view of my ministers that the door to negotiation at any time a cessation of hostilities in Korea can be arranged must be kept open.
The increased menace in the Far East reinforces the mounting evidence that communist imperialism is determined to dominate the world by force or the fear of force, and that the only hope of maintaining peace with freedom lies in the rapid increase of the combined strength of the free nations. It is equally important that the free nations should make it abundantly clear that they have no aggressive designs and that they are resolved to aid in constructive endeavours to improve the standards of human welfare in underdeveloped countries.
My ministers have endeavoured to uphold these international objectives in the deliberations of the United Nations, at the recent meeting of the prime ministers of the commonwealth countries, and in our diplomatic relations with all nations and governments.
Units of the Royal Canadian Navy and the Royal Canadian Air Force have shared from the outset in the United Nations action in Korea. One battalion of the Canadian Army special force is now in Korea, and the rest of the force is at Fort Lewis Washington, where it is available for service in Korea or for other employment in discharge of our international obligations.
Progress has been made in the organization of an integrated force in Europe under the North Atlantic treaty organization. The Supreme Commander, General Eisenhower, has recently visited Canada to consult with the government and the chiefs of staff. You will be asked early in the session to authorize Canadian participation in this integrated force as part of our program for national defence and security. You will also be asked to approve substantially increased expenditures for defence.
The urgent need of the St. Lawrence seaway and power project in relation to the security of this
HOUSE OF COMMONS
Speech from the Throne continent is becoming increasingly apparent. It is the view of my ministers that the Canadian authorities should be kept in a position to co-operate promptly in undertaking construction of the project once affirmative action has been taken by the appropriate United States authorities.
Your approval will be sought for an appropriate Canadian participation in the Colombo plan and in technical assistance to underdeveloped areas.
The policies of the government are designed to prevent war, but the dangers of the international situation and the magnitude of the defence effort required as a deterrent have, in the opinion of my ministers, created an emergency situation. You will accordingly be asked to approve legislation vesting in the governor in council additional powers to ensure adequate defence preparations to meet the present emergency and to prevent economic dislocation resulting from defence preparations'.
You will also be asked to approve a bill to establish a Department of Defence Production to act as a procurement agency for the defence forces of Canada and also for such defence requirements of our allies as may be met from Canadian production.
Amendments to legislation relating to the armed forces will also be submitted for your approval.
Legislation will be submitted respecting the application of the benefits of the veterans charter to members of the special force. Amendments will be introduced to legislation concerning pensions for veterans and their dependents to relieve difficulties being experienced by certain groups of pensioners provided for thereunder.
Appropriate amendments to the Canadian Citizenship Act will be introduced to prevent the retention of Canadian citizenship by persons who have renounced their allegiance or shown by their conduct that they are not loyal to Canada.
The high level of employment and production within our country give our people increased capacity to meet the demands of national and international security.
The spirit of unity so happily reflected in the conferences between the federal and provincial governments is further evidence of our ability to make an effective national response to the emergency.
Proposals were laid before the provincial governments for new tax agreements and for a contributory old age pensions program along the lines recommended by the joint committee at the last regular session.
The provincial governments are at present giving consideration to these proposals and to proposals for constitutional amendments which may require to be submitted to you before the close of the present session.
You will be asked to consider measures respecting federal grants to municipalities in lieu of taxation of crown property, the abolition of the requirement of the flat in the case of petitions of right, and the bequests of Laurier House and Kingsmere.
You will also be asked to consider a complete revision Of the Indian Act and the Consolidated Revenue and Audit Act.
Other measures to be introduced will include amendments to the Immigration Act, the Post Office Act, the Central Mortgage and Housing Act, the Gold Mining Assistance Act and the Customs Act.
It is anticipated that the reports of the royal commissions on transportation and on national development in the arts, letters and sciences will become available during the course of the session.
[Mr. Speaker.]
Members of the House of Commons:
You will be asked to make provision for national defence and the meeting of our obligations under the United Nations charter and the North Atlantic treaty, as well as for all essential services.
Honourable Members of the Senate:
Members of the House of Commons:
May Divine Providence bless your deliberations and give to our people the fortitude and patience to sustain the trials of these troubled times.
MOTION FOR PRECEDENCE OF DEBATE ON ADDRESS
Right Hon. L. S. St. Laurent (Prime Minister): Mr. Speaker, I beg leave to move, seconded by the Minister of Trade and Commerce (Mr. Howe):
That the speech of His Excellency the Governor General to both houses of parliament be taken into consideration on Wednesday next, and that, unless and until otherwise ordered, this order have precedence over all other business except introduction of bills and government notices of motions until disposed of.
Motion agreed to.
STANDING COMMITTEES
Right Hon. L. S. St. Laurent (Prime Minister) moved:
That a special committee be appointed to prepare and report, with all convenient speed, lists of members to compose the standing committees of this house under standing order 63, said committee to be composed of Messrs. Fournier (Hull), Claxton, Casselman, Shaw and Weir.
Motion agreed to.
INTERNAL ECONOMY COMMISSION
Right Hon. L. S. St. Laurent (Prime Minister) presented the following message from His Excellency the Governor General:
The Governor General transmits to the House of Commons a certified copy of an approved minute of council, appointing the Hon. Alphonse Fournier, Minister of Public Works, the Hon. D. C. Abbott, Minister of Finance, the Hon. J. J. McCann, Minister of National Revenue, and the Hon. F. G. Bradley, Secretary of State of Canada, to act with the Speaker of the House of Commons as commissioners for the purposes and under the provisions of chapter 145 of the Revised Statutes of Canada, 1927, intituled: An act respecting the House of Commons.
HOUSE OF COMMONS
APPOINTMENT OF CHAIRMAN OF COMMITTEES OF THE WHOLE
Right Hon. L. S. St. Laurent (Prime Minister) moved:
That Louis Rene Beaudoin, Esquire, member for the electoral district of Vaudreuil-Soulanges, be appointed deputy chairman of committees of the whole house.
Motion agreed to.
JANUARY 30. 1951
REPORTS AND PAPERS
Report of the joint librarians of parliament. —Mr. Speaker.
Report of the chief electoral officer on byelections held during the year 1950.—Mr. Speaker.
BUSINESS OF THE HOUSE
Mr. St. Laurent: Mr. Speaker, I move that the house do now adjourn until three o’clock tomorrow afternoon.
In view of the fact that under the standing orders there is only one sitting of the house
Business of the House
on Wednesdays, and the further fact that, in accordance with the motion adopted a few minutes ago, the business tomorrow will be the consideration of His Excellency’s speech to both houses of parliament, I suggest that after the motion for an address to His Excellency is made and seconded, we adjourn until Thursday, at which time, in accordance with tradition, the leader of the opposition (Mr. Drew) will be the first to be recognized; I might follow, and then the other leaders, in the order that has become traditional in this house.
Motion agreed to and the house adjourned at 4 p.m.
80709—11
HOUSE OF COMMONS
