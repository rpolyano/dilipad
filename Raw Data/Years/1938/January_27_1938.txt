THIRD SESSION—EIGHTEENTH PARLIAMENT-OPENING
The parliament which had been prorogued from time to time to the twenty-seventh day of January, 1938, met this day at Ottawa, for the dispatch of business.
The house met at three o’clock, the Speaker in the chair.
Mr. Speaker read a communication from the Governor General’s secretary, announcing that His Excellency the Governor General would proceed to the Senate chamber at three p.m. on this day, for the purpose of formally opening the session of the dominion parliament.
A message was delivered by Major A. R. Thompson, Gentleman Usher of the Black Rod, as follows:
Mr. Speaker, His Excellency the Governor General desires the immediate attendance of this honourable house in the chamber of the honourable the Senate.
Accordingly the house went up to the Senate chamber.
And the house being returned to the Commons chamber:
VACANCIES
Mr. SPEAKER: I have the honour to inform the house that during the recess I received communications from several members, notifying me that the following vacancies had occurred in the representation, viz:
Of Colin Alexander Campbell, Esquire, member for the electoral district of Frontenac-Addington, by resignation;
Of Daniel Alexander Cameron, Esquire, member for the electoral district of Cape Breton North-Victoria, by decease;
Of Hon. William Earl Rowe, member for the electoral district of Dufferin-Simcoe, by resignation;
Of Hon. Simon Fraser Tolmie, member for the electoral district of Victoria, B.C., by decease;
51952—1
Of Joseph Achille Verville, Esquire, member for the electoral district of Lotbiniere, by decease;
Of Paul Mercier, Esquire, member for the electoral district of St. Henry, consequent upon the acceptance of an office of emolument under the crown;
Of Right Hon. Sir George Halsey Perley, G.C.M.G., member for the electoral district of Argenteuil, by decease;
Of William Michael Ryan, Esquire, member for the electoral district of St. John-Albert, by decease.
I accordingly issued my several warrants to the chief electoral officer to make out new writs of election for the said electoral districts, respectively.
NEW MEMBERS
Mr. SPEAKER: I have the honour to inform the house that the clerk of the house has received from the chief electoral officer certificates of the elections and return of the following members, viz:
Of Ralph Melville Warren, Esquire, for the electoral district of Renfrew North;
Of Matthew MacLean, Esquire, for the electoral district of Cape Breton North-Victoria ;
Of Angus Neil McCallum, Esquire, for the electoral district of Frontenac-Addington;
Of the Hon. William Earl Rowe, for the electoral district of Dufferin-Simcoe;
Of Robert Wellington Mayhew, Esquire, for the electoral district of Victoria (B.C.);
Of Joseph Napoleon Francceur, Esquire, for the electoral district of Lotbiniere.
NEW MEMBERS INTRODUCED
Ralph Melville AVarren, Esquire, member for the electoral district of Renfrew North, introduced by Right Hon. W. L. Mackenzie King and Hon. Norman McL. Rogers.
Matthew MacLean, Esquire, member for the electoral district of Cape Breton North-Victoria, introduced by Right Hon. W. L. Mackenzie King and Hon. J. L. Ilsley.
REVISED EDITION
Governor General’s Speech
Angus Neil McCallum, Esquire, member for the electoral district of Frontenac-Addington, introduced by Right Hon. W. L. Mackenzie King and Hon. Norman McL. Rogers.
Joseph Napoleon Francoeur, Esquire, member for the electoral district of Lotbiniere, introduced by Right Hon. W. L. Mackenzie King and Right Hon. Ernest Lapointe.
OATHS OF OFFICE
Right Hon. W. L. MACKENZIE KING (Prime Minister) moved for leave to introduce Bill No. 1, respecting the administration of oaths of office.
Motion agreed to and bill read the first time.
GOVERNOR GENERAL’S SPEECH
Mr. SPEAKER: I have the honour to inform the house that when the house did attend His Excellency the Governor General this day in the Senate chamber, His Excellency was pleased to make a speech to both houses of parliament. To prevent mistakes, I have obtained a copy, which is as follows: Honourable Members of the Senate:
Members of the House of Commons:
It affords me much pleasure to greet you upon the resumption of your parliamentary duties.
The interval which has elapsed since the last session witnessed the coronation, in the month of May, of Their Majesties King George the Sixth and Queen Elizabeth. The event was one of special significance to the nations of the British Commonwealth. In the coronation service and ceremonial, recognition was given to the relationship between the sovereign and his peoples in the several dominions, as embodied in the Statute of Westminster.
Members of the government participated in the deliberations of the Imperial Conference which followed immediately after the coronation. The summary of proceedings of the conference will be placed before you at an early date for your consideration. It is the belief of the government that the opportunities afforded for the exchange of views and information on questions of common interest and concern, will serve to further the well-being of all parts of the commonwealth.
It is gratifying to note that, during the past year, there has been a further substantial advance in Canada’s economic recovery. Revenues have reached new levels. Trade -with other countries has materially expanded. There has been a general increase in employment and a marked decrease in the numbers receiving unemployment aid.
The recurrence, in a more acute form, of drought conditions in certain areas of western Canada has unfortunately made it necessary to provide assistance on an unprecedented scale. The government intends to continue its activities under the Prairie Farm Rehabilitation Act.
In view of the success which has attended efforts to assist in the training of unemployed young people, it is proposed to extend the mherne during the coming year.
fMr. Speaker.]
The Department of Agriculture has been reorganized and its services consolidated along lines designed to improve the standard and acceptability of Canadian farm products.
Arrangements are being completed for the inauguration of a national trans-Canada air service.
The National Employment Commission, the royal commission appointed to inquire into conditions in the textile industry, and the commission appointed under the provisions of the Veterans’ Assistance Commission Act, 1936, have concluded their duties. The reports of these commissions will be tabled in due course.
The strains and stresses, which economic and social developments since confederation have placed upon Canada’s governmental structure, have disclosed the necessity for adjustments which will enable it the more effectively to serve provincial and national needs, and to promote and preserve Canadian unity. My ministers are of the opinion that, with exact and adequate information, it should be possible for the appropriate authorities to work out satisfactory solutions. As a first step towards this end, a royal commission of inquiry has been appointed to re-examine the economic and financial basis of Confederation and the distribution of legislative povrers in the light of the new conditions which have arisen in the past seventy years. The commission has already held sittings in many parts of the Dominion.
The cooperation of the provinces has been sought with a view to an amendment of the British North America Act, which would empower the parliament of Canada to enact forthwith a national scheme of unemployment insurance. My ministers hope the proposal may meet with early approval, in order that unemployment insurance legislation may be enacted during the present session of parliament.
Members of the House of Commons will be invited to consider the report of the special committee on Elections and Franchise Acts, and you will be asked to enact such legislation as may be necessary to implement such of the committee’s recommendations as meet with their approval.
A measure will be submitted to extend the authority of the Board of Railway Commissioners.
Legislation will be introduced with a view to furthering the principle of parliamentary control of the export of electrical power.
The international situation generally continues to give much ground for anxiety. My ministers have endeavoured, as opportunity has afforded, to promote international understanding and good-will. They have sought to join the efforts of Canada to those of other countries which are seeking by cooperation and conciliation to effect a settlement of questions and issues which concern the world’s peace.
The administration has followed with deep interest the course of the negotiations being conducted with a view to the conclusion of a trade agreement between the United Kingdom and the United States of America. My ministers are fully alive to the importance of these negotiations, and to Canada’s interest in their outcome.
In August last, the Canadian government approached the government of the United States with a view to extending and revising the trade agreement concluded between them in 1935. Exploratory conversations followed
JANUARY 27, 1938	£
His Majesty King George VI
which have resulted in efforts to effect a new agreement on a broad and comprehensive basis. It is hoped that negotiations may so progress as to render it possible to submit the new agreement to parliament, for its consideration, during the present session.
With a number of other countries, adjustments have been made, during the past year, in our commercial relations in order to facilitate a wider exchange of commodities.
The government is convinced that, in seeking to cooperate with the United Kingdom and other countries in efforts to promote international trade, it is pursuing one of the most effective means of ensuring economic security and progress in Canada, and the betterment of conditions in other parts of the world.
Members of the House of Commons:
The public accounts of the last fiscal year and the estimates for the coming year will be submitted for your consideration.
Honourable Members of the Senate:
Members of the House of Commons:
In inviting your careful consideration of the important matters which will engage your attention, I pray that Divine Providence may guide and bless your deliberations.
Right Hon. W. L. MACKENZIE KING (Prime Minister) moved:
That the speech of His Excellency the Governor General to both houses of parliament be taken into consideration on Monday next, and that this order have precedence over all other business, except the introduction of bills, until disposed of.
Motion agreed to.
HIS MAJESTY KING GEORGE VI
ACKNOWLEDGMENT OF ADDRESS TO HIS MAJESTY ON THE OCCASION OF HIS ACCESSION TO THE THRONE
Mr. SPEAKER: I have the honour to inform the house that I have received a communication from His Majesty King George VI, signed by his own hand, as follows:
Buckingham Palace.
Members of the Senate and of the House of Commons of Canada.
I thank you most sincerely for the assurances of loyalty and support contained in the address which you have presented to me upon the occasion of my accession to the throne. It is the dearest wish of The Queen and myself that our reign may be marked, under Divine Providence, by the blessings of peace and by a steady advancement in the welfare and prosperity of all our peoples: and in our labours to this end we shall be strengthened and encouraged by the prayers and goodwill of the Canadian parliament and people.
George R.I.
12th April, 1937.
ACKNOWLEDGMENT OF ADDRESS TO HIS MAJESTY ON THE OCCASION OF HIS CORONATION
Mr. SPEAKER: I have the honour to inform the house that I have a communication
51052—14
from His Majesty King George VI, signed by his own hand, as follows:
Buckingham Palace.
Members of the Senate and of the House of Commons of Canada:
It is with feelings of deep gratitude that I acknowledge the message of loyalty and congratulation conveyed in your address of the 10th April which was presented to me by my Prime Minister of Canada on the 11th May.
The assurances of loyalty and devotion addressed to us on that occasion -will always be an encouragement to The Queen and myself in the performance of our high task.
We were glad to know that the Speakers of your two houses were present at the solemn ceremony of our coronation. The participation in that ceremony of representatives from our overseas dominions fittingly marked the position of the crown as symbolising the unity and free association of the peoples of the British Commonwealth.
Throughout our reign it will be our constant aim to cherish and maintain, to the best of our powers, the heritage of justice, civil liberty, and ordered freedom which we have received from those who in past generations helped to build up this association of nations; and we rejoice to know that in our endeavours to promote, under Divine guidance, the welfare and happiness of our peoples, we shall be supported by the prayers and affection of the people of Canada.
George R.I.
29th June, 1937.
STANDING COMMITTEES
Right Hon. W. L. MACKENZIE KING (Prime Minister) moved:
That a special committee consisting of Messrs. Casselman, Gray, Mackenzie (Vancouver), Power and Woodsworth, be appointed to prepare and report with all convenient speed, lists of members to compose the standing committees of this house, under standing order 63.
Motion agreed to.
INTERNAL ECONOMY COMMISSION
Right Hon. W. L. MACKENZIE KING (Prime Minister) presented the following message from His Excellency the Governor General:
The Governor General transmits to the House of Commons a certified copy of an approved minute of council appointing the Honourable T. A. Crerar, Minister of Mines and Resources, the Right Honourable Ernest Lapointe, Minister of Justice, the Honourable Charles A. Dunning, Minister of Finance, and the Honourable W. D. Euler, Minister of Trade and Commerce, to act with the Speaker of the House of Commons as commissioners for the purposes and under the provisions of chapter 145 of the revised statutes of Canada, 1927, intituled An Act Respecting the House of Commons.
Auditor General’s Report
LIBRARY OF PARLIAMENT
Mr. SPEAKER: I have the honour to lay before the house the report of the joint librarians of parliament.
On motion of Mr. Mackenzie King the house adjourned at 4.25 p.m.
