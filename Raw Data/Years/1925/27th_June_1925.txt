Resolved, that a message be sent to the House o* Commons to acquaint that House that it is the Deputy Governor’s desire that they attend him immediately in the Senate chamber, and—
Ordered, that the said message be communicated to the House of Commons by one of the clerks at the Table.
A. E. Blount,
Clerk of the Senate.
Right Hon. ARTHUR MEIGHEN (Leader of the Opposition):	It seems necessary, Mr.
Speaker, to remind the government of the undertaking given me yesterday by the Minister of Trade and Commerce (Mr. Low) and the Minister of the Interior (Mr. Stewart, Argenteuil).
Right Hon. GEORGE P. GRAHAM (Minister of Railways and Canals):	Mr.
Speaker, the reply to that question was made out and I see there are several errors in it, so that it could not be very well laid on the table of the House. I promise my right hon. friend that the corrections will be made and he will be sent a copy of it.
Prorogation
Mr. MEIGHEN:	Another hon. member
insisted on its being read into Hansard, which certainly he had a right to ask. The return was to have been read in this morning, as complete as it could be made, with an estimate of Mr. Symington’s fees.
Mr. LOW:	I shall be pleased to lay on
the table of the House now an estimated statement of Mr. Symington’s fees, and any other information completed in the department.
Mr. MEIGHEN:	It is understood that it
is read into Hansard?
Mr. LAPOINTE:	It is a return.
Mr. MEIGHEN: What is the government doing about the question? The government come this morning with a wrongly answered question. Certainly the question as rightly answered should go into Hansard.
Mr. LAPOINTE:	Why?
Mr. MEIGHEN:	Because that was the
undertaking of the government.
Mr. LAPOINTE:	The undertaking was
to lay the information on the table of the House.
Mr. MEIGHEN: Not at all; the Minister of Justice (Mr. Lapointe) was not in his seat. The undertaking was as I have stated it. If we are to end up with a breach of faith on the part of the government, it is well that everybody should know it.
Mr. LAPOINTE:	Why should the right
hon. gentleman have special privileges in this House?
Mr. STEWART (Argenteuil):	My under-
standing was we were to give to the right hon. gentleman—
Accordingly, Mr. Speaker with the House, went up to the Senate chamber.
In the Senate chamber, the Right Honourable the Deputy of His Excellency the Governor General was pleased to give, in his Majesty’s name, the royal assent to the following bills:
BILLS ASSENTED TO
An Act to amend the Northwest Territories Act.
An Act to amend The Bankruptcy Act.
An Act to amend The Civil Service Act, 1918, respecting certain Post Office employees.
An Act to amend The Income War Tax Act, 1917.
An Act to amend an Act to provide Compensation where Employees of His Majesty are killed or suffer injuries while performing their duties.
An Act to amend The Fruit Act.
An Act for the relief of Walter Thomas Pratchett.
An Act for the relief of Samuel James Connor.
An Act for the relief of Andrew Toulouse.
[Mr. Graham.l
An	Act	for	the relief of	Albert Plue Jessop.
An	Act	for	the relief of	Cecil Hunter.
An	Act	to	change the name of “The Dominion
Woman’s Christian Temperance Union” to “Canadian Woman’s Christian Temperance Union.”
An Act respecting Divorce.
An Act to amend The Dairy Industry Act, 1914. An Act to amend The Yukon Quartz Mining Act. An Act to amend the Customs Act.
An	Act	to	provide for	further advances to	the
Quebec Harbour Commissioners.
An	Act	for	the	relief	of	Matthew Wilson Lazenby.
An	Act	for	the	relief	of	Evelyn Laura Herlehy.
An	Act	for	the	relief	of	Lois Kathleen Purdy.
An	Act	for	the	relief	of	George William Quibell.
An	Act	for	the relief of Frederick Ethelbert Shibley..			
An	Act	for	the	relief	of	Alfred Percival Selby.
An	Act	for	the	relief	of	Charles Thomas Bolton.
An	Act	for	the	relief	of	Ada Durward.
An	Act	for	the	relief	of	Edward James Hogan.
An	Act	for	the	relief	oi	' Roger Alexander McGill.
An	Act	for	the	relief	of	John Perron.
An	Act	for	the	relief of William Albert Everingham.		
An	Act	for	the	relief	of	Mary Ella Mackey.
An	Act	for	the	relief	of	' Melvin Grant Cowie.
An	Act	for	the	relief	of	Ella May Stacey.
An	Act	for	the	relief	of	Jessie Harriett MacKey.
An	Act	for	the	relief	of	Edna Fox.
An	Act	for	the	relief	of	James Jackson.
An	Act	for	the	relief i	of William Frederick Hamilton:	
Strangway.
An Act to amend The Railway Act.
An Act to amend an Act respecting the National"
Battlefields at Quebec.
An Act to amend the Prisons and Reformatories Act. An Act for carrying into effect a Treaty signed 6th June, 1924, between His Majesty in respect of Canada and the United States of America, for the suppression-of smuggling operations and for other purposes.
An Act to amend the Excise Act.
An Act respecting certain patents of Accounting and Tabulating Machine Corporation.
An Act for the relief of Walter Roderick Lewis.
An	Act	for	the	relief	of	Irene	Muriel	Corelli.
An	Act	for	the	relief	of	Wilfred Clarence Byron.
An Act for the relief of Jessie Irene Yates.
An	Act	for	the	relief	of	Walter	Lewis	Hawkins.
An	Act	for	the	relief	of	Lucy	Eileen	Johnston.
An Act for the relief of Susan Ellen Taunton Love. An Act for the relief of Caroline Watters.
An Act for the relief of Grace Wilhelmina Harrison. An Act for the relief of Ethel Foster.
An Act respecting The Canadian Pacific Railway Company.
An Act respecting The Essex Terminal Railway Company.
An Act to incorporate Knights of North America. An Act respecting a patent owned by the Concrete Surfacing Machinery Company.
An Act respecting The Calgary and Fernie Railway Company.
An Act for the relief of Mary Ann Tattersall.
An	Act	for	the	relief	of James Deverell.
An	Act	for	the	relief	of Anita Allcock.
An	Act	for	the	relief	of Euphemia Tudor	Slade.
An	Act	for	the	relief	of Marion Roberts	Edmiston.
An	Act	for	the	relief	of William Morgan	Floyd.
An Act for the relief of Harry Iven Jones.
An Act for the relief of Edith Smith.
An Act for the relief of Mary Helen Wallace.
An Act for the relief of Wilbert Newell Hurdman. An Act for the relief of Maude Crawford Ross. An Act for the relief of William Garfield Reed. An Act for the relief of Bertha Matilda Quinn.
An Act respecting the disposal of the Canteen Funds.
Governor General’s Speech
An Act respecting a patent owned by The John E. Russell Company.
An Act respecting a patent owned by The John E. Russell Company.
An Act for the relief of Elizabeth Ethel McSherry.
An Act respecting trade relations with Australia.
An Act to amend The Soldiers' Settlement Act, 1919.
An Act to constitute a Board of Audit.
An Act for the relief of certain Creditors of the Home Bank of Canada.
An Act to amend the Pension Act.
An Act to amend The Criminal Code.
An Act respecting Grain.
An Act to amend The Dominion Elections Aci
An Act to amend the Civil Service Superannuation Act. 1924.
An Act for granting to His Majesty certain sums of money for the public service of the financial years ending respectively the 31st March, 1925, and the 31st March, 1926.
To these bills the royal assent was pronounced by the Clerk of the Senate in the following words:
In His Majesty’s name, the Right Honourable the Deputy of His Excellency the Governor General doth assent to these bills.
Then the Honourable the Speaker of the House of Commons addressed the Right Honourable the Deputy Governor General as follows:
May it please your honour:
The Commons of Canada have voted supplies required to enable the government to defray certain expenses of the public service.
In the name of the Commons I present to Your Honour the following bill:	An Act for granting to
His Majesty certain sums of money for the public service for the financial years ending respectively 31st March, 1925 and 31st March, 1926.
To which bill I humbly request your honour’s assent.
To this bill the Clerk of the Senate, by command pf the Right Honourable the Deputy	of	His	Excellency	the	Governor
General, did thereupon say:
In His Majesty’s name, the Eight Honourable the Deputy of His Excellency the Governor General thanks h:s loyal subjects, accepts their benevolence, and assents to these bills.
After which the Right Honourable the Deputy	of	His	Excellency	the	Governor
General was pleased to close the fourth session of the fourteenth parliament of the Dominion of Canada with the following speech:
Honourable Members of the Senate:
Members of the House of Commons:
In bringing to a close the Fourth Session of the Fourteenth Parliament of Canada. I desire to exprean to you my appreciation of the care and attention given the many important measures which have come before you for consideration.
It is gratifying to observe that the trade of our country is expanding as at no previous period of its history. The favourable balance for the fiscal year ended March 31st exceeded 284 millions of dollars. This expansion will without doubt be further stimulated by the inter-imperial agreement negotiated with our sister Dominion, 'Australia, to which approval has just been given, and by the legislation enacted for the purpose of establishing most favoured nation trade relations with Finland and the Netherlands, including the populous and wealthy Islands of the Dutch East Indies.
Delegates from the British West Indies are at present conferring with my Government concerning reciprocal development of trade and the improvement of communications throughout British America.
Canadian trade, via Canadian ports, has been greatly developed under the policy of allowing additional preference upon commodities when imported through Canadian ports from countries enjoying the British Preference. Practically the entire importations of British goods enjoying a preference now enter Canada through Canadian ports.
To provide more adequate facilities for our increasing ocean-borne traffic, provision has been made for improving the equipment of our national harbours.
A Special Committee of the House of Commons, appointed early in the session to consider a proposal to bring about the lowering of North Atlantic freight rates, has recently reported, confirming the existence of a combine and the necessity for the establishment of an effective control over Ocean Rates. This important subject will continue to engage the attention of my advisers.
The intricate problem of the regulation of railway freight rates throughout Canada has been dealt with in a manner which it is believed will enable the Board of Railway Commissioners to present a rate structure, based upon an equalization of rates as between provinces and localities, that will be fair and just to all parts of Canada, and which should serve further to stimulate both domestic and foreign trade.
A consolidation and revision of the Canada Grain Act has been made, which should prove of direct and substantial benefit to the great agricultural industry of the country.
Rigorous enactments have been passed to aid in the prevention of smuggling and the enforcement of our revenue laws. They have been supplemented by important treaties with the United States respecting the suppression of smuggling, and of traffic in narcotics.
Agreements have also been concluded with the United States for the final demarcation of the international boundary line and the regulation of the level of the Lakp of the Woods.
Amendments to the Industrial Disputes Investigation Act, the Soldiers’ Settlement Act, and the Dominion Elections Act, have been among other important enactments of the session.
Members of the House of Commons:
I thank you for the provision you have made for the public service.
Honourable Members of the Senate:
Members of the House of Commons:
The numerous evidences of increasing prosperity are now happily supplemented by the prospect of a bountiful harvest. For these and other blessings I humbly join with you in thanksgiving to Divine Providence.
This concluded the fourth session of the fourteenth parliament.
END OF VOLUME V.
