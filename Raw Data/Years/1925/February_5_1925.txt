The parliament which had been prorogued from time to time to the fifth day of February, 1925, met this day at Ottawa for the despatch of business.
The House met at three o’clock, the Speaker in the chair.
APPOINTMENT OF CLERK AND CLERK ASSISTANT
Mr. SPEAKER:	I have the honour to
inform the House that Arthur Beauchesne, Esquire, K.C., F.R.S.C., has been appointed Clerk of the House of Commons, in the place of William Barton Northrup, Esquire, M.A., K.C., retired;
And that Thomas Munro Fraser, Esquire, LL.B., has been appointed Clerk Assistant.
FOURTH SESSION—FOURTEENTH PARLIAM ENT—OPENING Mr. Speaker read a communication from Mr. A. F. Sladen, Governor General’s Secretary, announcing that His Excellency the Governor General would proceed to the Senate chamber at three p.m. on this day, for the purpose of formally opening the session of the Dominion Parliament.
A message was delivered by Colonel Ernest J. Chambers, Gentleman Usher of the Black Rod, as follows:
Mr. Speaker, His Excellency the Governor General desires the immediate attendance of this honourable House in the chamber of the honourable the Senate.
Accordingly the House went up to the Senate chamber.
And the House being returned to the Commons chamber:
VACANCIES
Mr. SPEAKER:	I have the honour to
inform the House that during the recess I 1
received communications from several members notifying me that the following vacancies had occurred in the representation, viz.:
Of Joseph Emile Stanislas Emmanuel d’Anjou, Esquire, member for the electoral district of Rimouski, consequent upon the acceptance of an office of emolument under the crown.
Of John Morrissy, Esquire, member for the electoral district of Northumberland (N.B.), by decease.
Of John Armstrong MacKelvie, Esquire, member for the electoral district of Yale, by decease.
Of Charles A. Gauvreau, Esquire, member for the electoral district of Temiscouata, by decease.
I accordingly issued my warrants to the Chief Electoral Officer to make out new writs of election for the said electoral districts respectively.
NEW MEMBERS
Mr. SPEAKER:	I have the honour to
inform the House that during the recess the Clerk of the House has received from the Chief Electoral Officer certificates of the election and return of the following members, viz:
Of William James Hushion, Esquire, for the electoral district of St. Antoine.
Of Sir Eugene Fiset, Kt., for the electoral district of Rimouski.
Of William Bunting Snowball, Esquire, for the electoral district of Northumberland, N.B.
Of Grote Stirling, Esquire, for the electoral district of Yale.
Of Charles Edward Hanna, Esquire, for the electoral district of West Hastings.
Of Jean Francois Pouliot, Esquire, for the electoral district of Temiscouata.
NEW MEMBERS INTRODUCED
William James Hushion, Esquire, member for the electoral district of St. Antoine, introduced by Right Hon. W. L. Mackenzie King and Hon. P. J. A. Cardin.
Sir Eugene Fiset, Kt., member for the electoral district of Rimouski, introduced by Right Hon. W. L. Mackenzie King and Hon. Ernest Lapointe.
REVISED EDITION
Governor General’s Speech
William Bunting Snowball, Esquire, member for the electoral district of Northumberland (N.B.), introduced by Hon. A. B. Copp and Pius Michaud, Esquire.
Charles Edward Hanna, Esquire, member for the electoral district of West Hastings, introduced by Right Hon. W. L. Mackenzie King and Hon. James Murdock.
Jean Francois Pouliot, Esquire, member for the electoral district of Temiscouata, introduced by Right Hon. W. L. Mackenzie King and Hon. Ernest Lapointe.
OATHS OF OFFICE
Right Hon. W. L. Mackenzie King (Prime Minister) moved for leave to introduce Bill No. 1. respecting the Administration of Oaths of Office.
THE GOVERNOR GENERAL’S SPEECH
Mr. SPEAKER:	I have the honour to
inform the House that when the House did attend His Excellency the Governor General this day in the Senate chamber, His Excellency was pleased to make a speech to both houses of parliament. To prevent mistakes, I have obtained a copy, which is as follows:
Honourable Members of the Senate:
Members of the House of Commons:
I have pleasure in welcoming you to the fourth session of the fourteenth parliament.
Since prorogation, the economic situation throughout the world has notably improved. For Canada, the year 1924 was a period of substantial progress. In trade alone, the excess value of exports over imports was more than $260,000,000. The present year opens with prospects of sound and steady development. The financial and trade situation justifies the expectation of an early return to the gold basis.
The problem of the cost of living is the most important that my ministers have in mind at the present time, and every effort is being made to improve conditions with respect thereto. It is apparent that even the most rigid economy in public expenditures will not suffice to solve this pressing problem and the problem of taxation incidental thereto. Their ultimate solution lies largely in increased production and the development of new and wider markets. It is to be borne continually in mind that the existing burden of taxation is due mainly to uncontrollable expenditure in the nature of payments and obligations arising out of the war, and to the encumbered position of the National Railways.
To aid in an increase of production, through the development of our vast natural resources, every effort is being made to attract the right class of immigrants to Canada, and to secure their settlement in the undeveloped areas served by our great transportation systems. In due course steps will be taken to further colonization and settlement in other fertile regions such as those of the Peace river.
The cost of production of raw materials and the necessaries of life has been lessened by the reductions in the tariff and the sales tax effected at the last session. It is becoming increasingly evident, however, that quite as important a factor a3 the customs tariff in their effect upon production and living costs are transportation costs and rates, by land and sea.
I Mr. Speaker.]
It is the opinion of my advisers that the attention of parliament at the present session should be directed more particularly to the desirability of effecting a freer movement of commodities through an equalization of railway freight rates as between provinces and localities, and through a lowering of carrying charges upon shipments by water of the products of the farm, the mine, the forest, the fisheries, and of our manufacturing industries.
Some measure of control of transportation by land and sea is obviously essential to the promotion of interimperial trade, the expansion of export trade generally, and the development of Canadian trade via Canadian ports.
The procedure it may be advisable to follow with respect to railway freight rates will in some measure necessarily depend upon the decision of the Supreme Court in the appeal respecting the Crowsnest pass agreement. With regard to ocean freight rates, action is being taken to overcome the restraints on export trade due to the exactions of the powerful steamship combine known as the North Atlantic Steamship Conference. Your approval will be asked of a measure aimed at affording the government of Canada a control of ocean rates.
It is the intention of the government so to equip our important ports on the St. Lawrence route, and on both the Atlantic and Pacific coasts, as to enable them to meet all requirements of modern navigation.
To secure greater co-operation in the administration of the laws of the two countries respecting smuggling and the prosecution and extradition of persons violating the anti-narcotic laws of either country, treaties between the Dominion of Canada and the United States have been negotiated and signed. They will be submitted for your approval prior to their ratification.
You will be asked to sanction the calling of a conference between the federal and provincial governments to consider the advisability of amending the British North America Act with respect to the constitution and powers of the Senate, and in other important particulars.	_
Your attention will also be invited, during the course of the session, to certain trade agreements, to legislation respecting the handling and marketing of Canadian grain and to other important matters. Members of the House of Commons:
The public accounts for the last fiscal year, and the estimates for the coming year, wil: be promptly submitted. In the preparation of the estimates, regard has been had to the need for continued economy with respect to the public services and public works.
Honourable Members of the Senate:
Members of the House of Commons:
May Divine Providence guide and bless your deliberations.
Right Hon. W. L. MACKENZIE KING (Prime Minister) moved:
That the Speech of His Excellency, the Governor General, to both houses of parliament be taken into consideration on Monday next, and that this order have precedence over all other business, except the introduction of bills, until disposed of.
Motion agreed to.
SELECT STANDING COMMITTEES
Right Hon. W. L. MACKENZIE KING (Prime Minister) moved:
That a special committee be appointed to prepare and report, with all convenient speed, lists of members to compose the select standing committees of this House under rule 10, said committee to be composed of Messieurs. Beland, Copp. Low, Kyte, Papineau, Tolmie. Boys, Johnston (Last Mountain) and
Deceased Members
Halbert, and that that portion of rule 10 limiting the members of the said committee be suspended in relation thereto.
Motion agreed to.
INTERNAL ECONOMY COMMISSION
Right Hon. W. L. MACKENZIE KING (Prime Minister) presented the following message from His Excellency the Governor General:
The Governor General transmits to the House of Commons a certified copy of an approved minute of council appointing the Honourable H. S. Beland, Min-inister of Soldiers’ Civil Re-establishment; the Honourable Thomas A. Low, Minister of Trade and Commerce; the Honourable J. H. King, Minister of Public Works, and the Honourable John E. Sinclair, Minister without portfolio, to act with the Speaker of the House of Commons, as commissioners for the purposes and under the provisions of the eleventh chapter of the Revised Statutes of Canada, 1906, intituled “ An Act respecting the House of Commons."
REPORTS
Report of the Chief Electoral Officer in conformity with the Dominion Elections Act. —Mr. Speaker.
Joint Report of the Librarians of Parliament.—Mr. Speaker.
DEPUTY SERGEANT-AT-ARMS
Mr. SPEAKER: I have the honour to inform the House that the Sergeant-at-Arms, with my approval, has appointed Edouard Taschereau, Esquire, to be his deputy during the present session of parliament.
DECEASED MEMBERS
Right Hon. W. L. MACKENZIE KING (Prime Minister): Mr. Speaker, in the interval which has elapsed since the last session of parliament two hon. members, very highly respected by all in this House, Mr. John Mor-rissy and Mr. Charles Arthur Gauvreau, have been taken from our midst. It is fitting before we enter upon the work of a new session that we should pause to pay a passing tribute to their memory and to express the sympathy which hon. members of this House feel with those who have been thus greatly bereaved.
Both hon. members were for a long time in public life, and both were very highly regarded. The late Mr. Morrissy entered this parliament only three years ago, at the general election of 1921, as the member for Northumberland, N.B. He had, however, been interested in the public affairs of his own province for a long time, having served in no less than three successive administrations of the province of New Brunswick, and having held the important post of Minister of Public li
Works for something like nine years. Mr. Morrissy had a keen liking and a genius for public life. It is said of him that he entered more political contests than any other public man in his province. Out of fourteen political contests he won as many as he lost. That under these circumstances he should at the time of his death have had, as he did have, the universal regard of those among whom he lived, and whose interests he strove so earnestly to serve, is perhaps the highest testimony that could be paid to his sterling integrity and to his fine independence, qualities in him which all of us well knew. It is a further testimony to his name and public example that the constituency which he represented in the provincial legislature should to-day be represented by his only son.
By all of us on this side of the House Mr. Morrissy will always be remembered for his kindly and generous disposition, for his loyal and helpful support, and for his fine public spirit and public service.
Mr. Gauvreau was one of the oldest members of the House, having entered parliament as the representative for Temiscouata in the by-election in 1897. He was returned! to parliament in the general elections of 1900, 1904, 1908, 1911, 1917 and 1921. For the quarter of a century that he thus represented that constituency he faithfully served the interests of his constituents and the people at large.
Mr. Gauvreau was one of the members of parliament who followed the proceedings of the House with a very keen interest. His talents were employed, however, even more in writing than in speaking. He did not take much part in debate; it was characteristic of him that he was prepared to leave the expression of views to others, and if he felt that his views were clearly enunciated that the right word had been said by others, he was content to leave matters there.
By profession Mr. Gauvreau was a journalist and a notary. He took a keen interest in the history of his native province and in literary pursuits generally, and he has left to our country contributions in prose and in verse which entitle his name to an honourable place among the authors of Canada. It is a tribute to his spirit of patriotic service, and to his example that both his sons served with great distinction in the -Canadian naval service during the period of the war.
Mr. Gauvreau will be very much missed in this House, particularly by members who likewise have been privileged to enjoy a long
Deceased Members
association with parliament. He was always active in matters pertaining to affairs of the House and of his party. He was very regular in his attendance, and took a keen interest in the work of committees. He was a faithful attendant at the party caucus, over which he not infrequently presided. There were about Mr. Gauvreau, the qualities which characterize the man of thought, conviction and determination. There was strength in his silent reserve, and a resoluteness of character to which his quiet demeanour lent emphasis. I do not know that anything more worthy could be said of him than that he was as much beloved by the members of this House who knew him, as he was honoured by the unbroken confidence of his constituents over a period of nearly twenty-five years.
Though past the age of sixty, neither of the honourable members whose deaths we deplore to-day had reached the allotted span of three score years and ten. Mr. Morrissy died in his sixty-seventh year; Mr. Gauvreau at the time of his death was in his sixty-fourth year. It is not by number of years but by reason of service that the memories of men are cherished. Whilst we honour the years of our late fellow-members, their names will command for all who knew them a deeper reverence.
Right Hon. ARTHUR MEIGHEN (Leader of the Opposition): It seems inescapable that at each successive opening of parliament necessity arises for a reference to deaths that have occurred among us. On the present occasion those we lament were members of the Liberal persuasion, and the Prime Minister has outlined the chief incidents of their lives and their great services to the country with a familiarity and a knowledge that I could not rival. Suffice it to say that neither gentleman during his identification with this House deserved aught but kindly reference; they were men whose records were honourable and sincere.
Mr. Morrissy's contributions to public service were mainly, of course, in the provincial sphere. His career in the province of New Brunswick is known to all—his long succession of political encounters, his gallant victories, his honourable defeats, his fine domestic life, and I join warmly and sympathetically with the Prime Minister in offering to his widow and his family this testimony of the reverence of the House.
Mr. Gauvreau was the better known among us. He was one of our members of the longest standing. Very seldom is it the lot of anyone to sustain public confidence in a single
CMr. Mackenzie King.]
county for a quarter of a century. He who achieves that result must have sterling qualities; he must not only be able to command respect but, by talents that few possess, be able to preserve it.
It has been said that Mr. Gauvreau, unlike too many of us in Canadian public life, devoted himself with some energy and some success to literature. I have had the pleasure of reading one of his historical works, and can bear testimony not only to its literary quality but to its value in substance. Gladly I accord him a place among Canadian writers.
The services of his family in the war is something also that we record with pride. His two sons offered themselves in the navy in the early hours of their country’s need. Both acquitted themselves creditably and gained a record that is honourable to their family and to their race.
To have been elected seven times a member of this House is itself an achievement greater than anything which might find mention in any eulogy after death. I assure all those who loved Mr. Gauvreau that he gained the esteem of political foes as well as political friends, and that we bear him now in honourable memory by reason of his personal qualities and his public services.
Mr. ROBERT EORKE (Brandon): I would only add a few words to what has been so well expressed by the right honourable Prime Minister, and the right honourable leader of the opposition. Amid life’s activities we are being constantly reminded that we have here no abiding state, and that two respected and esteemed members who have sat with us in this House shall know their places no more.
I did not have the opportunity, being a comparatively new member of the House, of forming a very intimate acquaintance with the two honourable gentlemen who have departed from us; but I knew both sufficiently well to respect and admire their character, their ability and their devotion to their duties in this House. I am sure they did their duty to their constituents and served Canada well; and I think the House, as a unit, will pay every respect to their memory, believing they did their best in the service of Canada and on all occasions acted honestly and uprightly.
We can now only tender our sympathy to the families of the deceased members and express the hope that they may find some consolation in the long and honourable records of those who have gone.
On motion of Mr. Mackenzie King the House adjourned at 4.27 p.m. until Monday, February 9.
Reports and Papers
