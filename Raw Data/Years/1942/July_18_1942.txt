The house met at eleven o’clock.
WAR EXPENDITURES COMMITTEE
Fifth and sixth reports of special committee on war expenditures.—Mr. Fournier (Hull).
SECRET SESSION OF THE HOUSE
Right Hon. W. L. MACKENZIE KING (Prime Minister) moved:
That the secret session of the house to be held this day be not reported in the official report of the House of Commons Debates and that at the conclusion of the secret session a report of its proceedings be issued under the authority of Mr. Speaker.
Motion agreed to.
The house went into secret session.
SUPPLY
The house in committee of supply, Mr. Yien in the chair.
Progress reported.
At the conclusion of the secret session, the following report of its proceedings was issued under the authority of Mr. Speaker:
“A secret session of the House of Commons was held at 11 o’clock a.m. on Saturday the 18th of July, 1942. The sitting was devoted to the question of coastal defence in Canada. The situation was explained by the three Ministers of National Defence.
J. Allison Glen,
Speaker.”
On motion of Mr. Mackenzie King the house adjourned at 6.19 p.m.
