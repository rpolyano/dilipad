Dear Sir,—I beg to acknowledge receipt of your communication of the 29th ultimo, enclosing an excerpt from Hansard of the 27th ultimo, containing some remarks concerning an application for a railway siding to connect with the public works wharf at Sackville.
In reply, I beg to say that some years ago a wharf was built on the northeast side of the harbour of Sackville by the New Brunswick and Prince Edward Island railway, and that the Intercolonial railway has a connection with this wharf. It appears that four or five years ago your department built a wharf on the opposite side of the bay—
He is wrong about that; it is not on the opposite side, hut further down.
-—and made no provision for railway connection with it. In 1913, a petition from the citizens of Sackville was sent to this department by the Sackville Board of Trade asking that rail connections be made with the new wharf. The matter was submitted to the Intercolonial officials, and a report was submitted showing the propositions for such a connection, one at an estimated cost of $10,949, and a second one at an estimated cost of $12,611.20. General Superintendent Brady—
Mr. Brady was on the Board of Management of the Intercolonial railway.
—reporting on the matter statedThere is already a wharf track from our yard to the old wharf at Sackville and we cannot see where we are going to get one cent of additional earnings by spending the above-named amount of money.
This is the particular part to which I would call the minister’s attention.
In fact, we can see where we are going to lose earnings, particularly from freight received from the Cape Tormentine line, which
now haul to Dorchester wharf, which we will not haul dt all as it will be delivered to the new wharf at Sackville and we will not get one cent of earnings out of it.
The whole subject was brought before the minister and he expressed the opinion that in. asmuch as the railway had already provided rail connection with an existing wharf at Sackville, he did not think the Intercolonial railway should be called upon to make, such a further expenditure of $10,000 or $12,000.
The minister will notice that Mr. Brady’s idea was that in shipping lumber and other products coming over the Cape Tormentine line for export, the Intercolonial railway was going to make some money by hauling them past Sackville to Dorchester, and the .people would pay the extra freight. The idea was to force the shippers to pay for hauling the freight 15 miles further. Without arguing the question .at any length, because I think the minister understands the situation, it seems to me an absolutely absurd proposition that the Government of the country, after building a wharf at a cost of $40,000 to accommodate the people of Sackville, should refuse to make any connection between the main line of railway .and that wharf. The only reason given is that on .account of having no railway connection there the shippers will have to pay for carrying the freight 15 miles further, thus enabling the Intercolonial railway to make more money at the expense of the people who should have an opportunity of using the wharf at Sackville. The proposition seems so absurd that it should not require any argument. If the Railway Department does not take up this matter and build a spur line to the wharf, which is only a short distance away,, the $40,000 expended on the wharf, might as well have been used in building a wharf in the woods between Sackville and Dorchester. Under present conditions, the wharf is not worth one cent to the people of Sackville. It is all very well to be economical, but I am afraid that Mr. Brady, in his dealings with the Intercolonial railway, was not so careful in the expenditure of public money in other directions. The expenditure called for is very small, and I can assure the acting minister that it would be of great advantage to the people of that locality. I would like to have from the minister an assurance that he will take the matter up, and see that a connection is made between the wharf and the main line.
Mr. REID: This is the first I have heard of this matter, but I will take it up with the
[Mr. Copd.I
management and see if there is any possibility of getting the connection.
Mr. DESCARRIES:	I would draw the
acting minister’s attention to a demand on the part of the citizens of Lachine in my constituency, to get rid of the nuisance caused by the whistles of vessels going down lake St. Louis to Lachine lock. During the night the vessels pass along the front of the city of Lachine, and each vessel gives four blasts, and as the vessels now are very big, they have big whistles. During the silence of the night, when every citizen has a right to take his rest without being disturbed by anything of an annoying character, suddenly we hear that big noise, and if it so happens that there is a baby, a woman or any other person sick, it is very trying and all are bothered by it. I have very often been asked if it is possible to devise some means to give a signal to the men attending the locks instead of using the whistle. I call the minister’s attention to it, and I hope he will be good enough to study the question and if possible grant the request of the citizens of Lachine. While passing through that part of the Lachine canal which is within the limits of Montreal, these vessels do not sound their whistles. Why should not the same consideration be shown for Lachine, which is a city of great importance now, in order that the citizens may be for ever rid of that annoyance?
Mr. REID: This was brought to my attention the other evening by an hon. member from Montreal, and the same thing applies to Lachine. I will take it up with the officials and ask them to communicate with the officers of the Lachine canal and see if something cannot be done. I will find out what has been done, and I will let my hon. friend know. I understand that this is quite a nuisance, and I will see if something cannot be done to improve the situation. If so it will be done.
Mr. DESCARRIES: There is a big dam at the head of the Lachine canal, and if necessary a man could be placed at the dam with a telephone to give the signal to those attending the locks.
Mr. REID: That is a good suggestion.
Public Works—chargeable to capital—public buildings—Ottawa Eastern Departmental Block —new fireproof roof, etc., $240,000.
Mr. GRAHAM: Has the contract been let for that?
Mr. ROGERS: No. This is for a new roof for the East Block. The roof is not
fireproof, and we felt the need of a fireproof roof when the fire in the main buildings took place. It is regarded by the ■officers as being essential.
Mr. GRAHAM: Will that put the roof •on the building?
Mr. ROGERS: That will put the roof on.
Mr. A. A'. McLEAN: Is the rest of the building fireproof?
Mr. ROGERS: Partly so, but it is not as good as it should be.
Mr. NESBITT: What is the matter with the present roof?
Mr. ROGERS: It is not fireproof at all.
Mr. NESBITT: There is not much use putting a fireproof top on a match. The building itself is not fireproof.
Mr. ROGERS:	The main building is
fairly fireproof. That part built recently is fireproof.
Mr. NESBITT: There is not much use spending money putting a fireproof roof on a building that is not fireproof.
Mr. A. A. McLEAN: It would be worth
the miniser’e while to consult with his officers, I think. It does not sound very logical to put a fireproof roof on a building that ie not fireproof.
Mr. ROGERS: I brought the same point up when the matter was presented the other day with the deputy, but he convinced me that it was the proper thing to do.
Mr. NESBITT: If the roof is sound there is no possible object in putting a fireproof roof on it. The only object would be in case of fire close by, to guard against the danger from cinders, but I hope that we are not going to have another fire.
Mr. ROGERS: We will give it very careful consideration before we decide to spend the money. It has not been decided to spend it yet.
Ottawa Parliament building—restoration—the plans for the said building and the method to be adopted for securing the reconstruction thereof to be subject to the approval of the joint committee appointed by the Prime Minister and the leader of the Opposition, $1,500,000.
Mr. GRAHAM: That committee has been appointed?
Mr. ROGERS: Yes.
Mr. GRAHAM: Who compose it?
228
Mr. ROGERS: The Hon. Dr. Reid, the Hon. Mr. Hazen, the Hon. Mr. Blondin, the Hon. Senator Lougheed, the Hon. Mr. Pugsley, the Hon. Mr. Lemieux, the Hon. Mr. Murphy, and the Hon. Senator Watson. They have held one meeting already, and they are to hold another meeting tomorrow night.
Mr. GRAHAM: You are on it too?
Mr. ROGERS: I am on it.
Mr. NESBITT: You are the committee?
Mr. ROGERS: I am a member of it.
Mr. GRAHAM: This committee is associated with the minister and the work is to be carried on as it agrees?
Mr. ROGERS: That is right.
Mr. GRAHAM: As to whether it will be let by contract and to whom it will be let?
Mr. ROGERS: That is right.
Harbours and rivers—Vancouver harbour improvements—further amount required, $200,000.
Mr. GRAHAM: Is that under contract?
Mr. ROGERS: Yes, that is to finish the False creek work.
Public Works—chargeable to income—public buildings—New Brunswick—West St. John— medical inspection building, site, $2,000.
Mr. GRAHAM: Is that for immigration purposes?
Mr. ROGERS: This is to make provision for the purchase of a site at West St. John for the erection of a medibal inspection building for the Immigration Department. This property is bounded by Clarence, Lancaster and Sutton streets, and comprises ten lots of land. It is reported as being especially cheap.
Ontario—Cayuga public building, heating and plumbing improvements, $3,500.
Mr. NESBITT: Is that a new building?
Mr. ROGERS: This is an old building which is being fitted up with plumbing.
Mr. NESBITT: What is it used for?
Mr. ROGERS:	Public building, post-
office, and so on.
Kingston—Royal Military College—covered drill hall—further amount required, $55,000.
Mr. A. K. MACLEAN:	Is this a new
building?
Mr. ROGERS:	This is a new building
that the Militia Department are pressing for and that is very necessary at Kingston
REVISED
EDITION
in connection with, the taking of the officers’ ■course in cavalry.
Mr. A. K. MACLEAN: Is this the one in regard to which the hon. membeT for Fron-tenac (Mr. Edwards) was trying to get information for so long and about which he was so patient?
Mr. ROGERS: It may be.
Ottawa public buildings—fire escapes, $15,000.
Mr. NESBITT: I suppose that is for those belly bands we have up in our windows?
Mr. ROGERS: No, it is for iron ladders. There is a general idea that we should have fire escapes around our public buildings now. I would like my hon. friend to try one of those belly bands.
Ottawa Royal Mint—repairs and improvements, $10,000.
Mr. A. K. MACLEAN: Are these repairs necessary? The minister has about $2,000,000 in these further Supplementary Estimates for public works. It hardly seems like carrying out the pledge that he has been making to us every other day.
Mr. ROGERS: $1,500,000 is for the main Parliament building.
Mr. A. K. MACLEAN: It does not take long to get away with $1,000,000 on that.
Mr. ROGERS: We have to build the Parliament building. The other items are very small.
Winnipeg post office—sidewalk mail receiver,
$6,200.
Mr. NESBITT: Is that for just one mail receiver?
Mr. ROGERS: That is at the back of the post office in a lane which is our property. The postmaster states that the time and labour which would be saved would be equivalent to at least one porter’s salary per annum, for the reason that the mail delivered in the chute would land upon the sorting table in the basement instead of having to be received by the porters at the rear of the post office piece by piece or in sacks, placed upon a truck, taken down one floor and then hauled a hundred feet back and then lifted item by item or sack by sack on to the sorting table.
Mr. GRAHAM: You will have to hire a new porter to watch the mail receiver.
[Mr. Rogers.]
Winnipeg—to fit up old immigration building as postal station, $50,000.
Mr. A. K. MACLEAN: What is that?
Mr. ROGERS: We have an immigration hall adjoining the Winnipeg station and it is not a suitable place for an immigration hall. We have to have an additional postal station and we have decided to fit up the immigration hall for the postal station, thus saving the building of a new one.
Mr. NESBITT:	What will you do with
the immigrants?
Mr. ROGERS: We have an old building alongside to use for as many immigrants as are coming in now. We’ are practising economy.
Mr. LEMIEUX: Why does it take $50,000 to fix this place up?
Mr. ROGERS:	It will take all of that.
The Canadian Pacific Railway Company have made great improvements there and we have to make certain improvements to
conform with the layout of the place.
Regina public buildings—Government’s share of cost of local improvement rates, $4,600.
Mr. GRAHAM:	Does the Government
pay the frontage tax or the local improvement rates in every city?
Mr. ROGERS: Every place, yes.
Sutherland—building in connection with Forest nursery station, $5,000.
Mr. NESBITT: What is that item for?
Mr. ROGERS:	For the erection of a
building to be used as a boarding house for the men employed in connection with the nursery station at Sutherland. The building is 28 by 32, two and a half stories, with rear wing 20 by 12, a frame building on stone foundation. The lowest tenderer was $3,365; contingencies and superintendence, $1,335; total, $5,000. The deputy minister strongly recommended that provision be made for the boarding house as it was urgently needed. Eight tenders were received.
Mr. NESBITT:	Is there $1,335 for
superintendence on a $3,365 building?
Mr. ROGERS:	That is for all the con-
tingencies, fitting it up inside and that sort of thing.
Calgary drill hall—further amount required, $100,000.
Mr. NESBITT: Are you erecting that building?
Mr. ROGERS: Yes.
Mr. NESBITT: There is $100,000 already in the Estimates.
Mr. ROGERS:	It will take $200,000.
There was some delay in going ahead with the work on account of arrangements having to be made with the city for the site. It is absolutely necessary to put up this building because more soldiers are being recruited in Calgary than in probably any other city in Canada, and there is no accommodation there of any kind.
Victoria immigration building—to provide for Government’s share of cost of laying a heavy asphalt pavement on Dallas road and Simcoe street, $1,345.02.
Mr. A. K. MACLEAN:	I suppose we
cannot criticise the present minister very much for these votes in aid of construction of sidewalks, because it has been the practice, I believe, for some years. I only rise to suggest the wisdom of a change in that policy hereafter. I think these towns may well construct the sidewalks around the buildings that the Canadian Government erects.
Mr. ROGERS: This has been the practice for so many years that it is now expected, and it will be unacceptable if a change is attempted.
Mr. MACLEAN: But it grows so rapidly.
Mr. ROGERS: The Dominion Government should keep pace with the enterprise of a growing town.
Mr. MACLEAN: But if the Government erects a building should not the town assume the responsibility for the sidewalk?
Mr. ROGERS: If in a town or city a person erects a store or dwelling, he contributes in taxation on that building for the maintenance of sidewalks and other public works and. services. But we pay no taxes, and surely it is only in keeping with our proper relation to the development of a growing town to provide as a local improvement the necessary sidewalk.
Harbours and rivers, Nova Scotia, $119,400.
Mr. GRAHAM: There is an item here for East river, improvements, a further amount required of 160,000. That is a large item.
Mr. ROGERS: That is for the lock gates and the completion of the great work we are doing on the East river for the development of that growing and thriving portion of our Dominion.
228i
' Mr. GRAHAM: What county is that in?
Mr. ROGERS: Pictou county, I believe.
Mr. MACDONALD: Will the minister please state why he has not awarded the contract for the construction of the lock gates? I understood -that tenders have been called for, and we -should like to see the work go on. If the gates are not put in during the summer, long delay will be caused.
Mr. ROGERS: We have called for tenders, but I did not like to award the contract until the vote had passed Parliament. As soon as the vote is passed we will take the matter up and get ahead.
Mr. A. W. CHISHOLM: There is an item here of $1,500 to construct a pile wharf at Whycocamagh. What is to be done there?
Mr. ROGERS: This is to reconstruct the wharf. Assistant Engineer McKeen reports that on September 2 last |he made an examination of the wharf and found it in a very bad condition, so bad -that he considered it dangerous to load any great weight upon it, unless extensive repairs were made. Eighty-seven out of 154 piles supporting the wharf were badly worm-eaten, and practically gave no support to th$ wharf, some of them being completely cut off.
Mr. McKENZIE: In connection with the item in this vote of $1,000 to repair the breakwater at Jamesville. I understand that the work now proposed to be carried out is much smaller than was at first desired, and that it will not fulfil the purpose of the engineer. I do not know whether the minister has before him the -information as to that. But .in connection with the construction of -that breakwater, I desire to call his attention now to a matter with regard to which I have had some correspondence with the department and on the subject of which I wrote him a letter today. Those carrying on the work engaged the scow of a man there and kept it for two months, but they decline to pay him except for the days during which they actually worked with the scow. It is no fault of the owner if they did not use the scow every -day during which they kept it, and all he asks is that they should pay him what other people pay him for the use of the scow, namely $3.50 a day. I do not know whether the minister thinks that that is too high a price to pay for a large scow, but that is another question and- one of which I am no judge. It is not fair that
•they should take the man’s scow and keep it for two months and then pay only for the days when they were at work. While I am on my feet, I desire to ask what is to be done under the item in this vote of $2,300 for repairs to ballast wharf at North Sydney.
Mr. ROGERS: As to the question of the scow to which the hon. gentleman has called .attention, when I receive his letter I shall be glad to have the matter looked into. It is the usual practice of the department to pay for a scow only for the days it is used. If there is any special reason to justify what the hon. gentleman recommends, I .shall be glad to give it consideration. As to the item with regard to North Sydney, this work is asked for by the district engineer. It is to build the outer face of a wharf where vessels lie for the discharge of ballast. The estimated cost is $2,270.
Mr. McKENZIE: That work is yet to be done?
Mr. ROGERS: To be done.
Progress reported.
QUEBEC, AND SAGUENAY AND LOTBINIERE AND MEGANTIC -RAILWAYS.
ACQUISITION OP BRANCH LINES.
Mr. ROGERS: This afternoon some hon. gentleman asked for certain information with regard to our railway legislation, in connection with lines of railway to be built below Quebec. I would ask the House to go back to the introduction of Bills, so that I can introduce a Bill intituled, “An Act to authorize the acquisition of lines of railway between the city of Quebec and Nairn Falls and between Lyster and St. Jean des Chaillons,” in order that it may be printed and place given to it on the Order Paper.
Mr. GRAHAM: This Bill has to be preceded by resolution.
Mr. ROGERS: Not necessarily.
Mr. PUGSLEY: It involves the expenditure of public moneys.
Mr. GRAHAM: My view is that any Bill involving the expenditure of money should be introduced by resolution. Notice of the resolution is given, the resolution goes through its various stages, and a Bill is introduced founded on the resolution.
Mr. ROGERS: In any case an Estimate will follow the Bill. .
Mr. DEPUTY SPEAKER: May I suggest that the motion to revert to this order of business be put, and that any discussion take place on that motion?
Motion agreed to, and the House proceeded to the order “Introduction of Bills.”
Mr. ROGERS moved for leave to introduce Bill No. 101, to authorize the acquisition of lines of railway between the city of Quebec and Nairn Falls and between Lyster and St. Jean des Chaillons.
Mr. GRAHAM: I still maintain that this Bill ought to be founded on a resolution. My hon. friend says that the Bill will be followed by an Estimate. That certainly is a roundabout proceeding; it is a new method of doing business in this House. Where we are providing by a Bill to do certain things surely that Bill, being a money Bill, ought to be founded on a resolution. The proposed aid to the Canadian Northern railway and Grand Trunk Pacific Railway Companies, not being brought in in the form of a Bill, need not necessarily have been proceeded by a resolution. This method, I may say, is very objectionable; a resolution would have been better. The procedure, however, suggested by my hon. friend is so contrary to all the practices of the House that I must protest against that way of doing it.
Mr. ROGERS: This Bill is founded on an Act passed in 1915 for the acquirement of branch line railways.
Mr. GRAHAM:	This one line cannot
apply to that.
Mr. ROGERS: No, it cannot apply to that, but this Bill applies to it, and is followed by an Estimate. That is why a resolution is unnecessary.
Mr. PUGSLEY: I think the hon. member for South Renfrew is right in the view that he takes, because the Bill certainly proposes a money obligation.
Mr. ROGERS: Not without an Estimate.
Mr. PUGSLEY: Oh, yes. It says this:
The Governor in Council may authorize and empower the Minister of Railways and Canals to acquire, under the provision of “An Act to amend the Government Railways Act and to authorize the purchase of certain railways ”, chapter 16 of the statutes of 1915, and upon such terms and conditions as the Governor in Council may approve, the railways described in the schedule hereto, together with such equipment, etc.
I Mr. McKenzie.]
The consideration to be paid tor each of the said railways and for any equipment, appurtenances and properties that may be acquired as aforesaid shall be the value thereof as determined by the Exchequer Court of Canada.
Mr. ROGERS: And to be followed by an Estimate.
Mr. PUGSLEY: It does not say that.
Mr. ROGERS: It has to be, in order to be of any value.
Mr. PUGSLEY: It is clear that if this Bill were passed it wouild authorize the Minister of Railways to purchase a road, the actual price paid to be fixed by the Exchequer Court of Canada. Rule 77 of the House says:
If any motion be made in the House for any public aid or charge upon the people, the consideration and debate thereof may not be presently entered upon, but shall be adjourned until such further day as the House thinks fit to appoint; and then it shall be referred to a Committee of the Whole House before any resolution or vote of the House do pass thereupon.
The meaning of that is that this Bill must be preceded by a resolution. Perhaps, however, it might be better to let the Bill be read the first time, and any objection could be taken on the second reading.
Mr. ROGERS: I simply made the motion for the benefit of hon. gentlemen opposite who wanted the information.
Mr. PUGSLEY: I withdraw the suggestion that I made, because I understand that the member for South Renfrew feels like urging his point of order.
Mr. DEPUTY SPEAKER: The hon. gentleman has referred to rule 77, which says that any motion for any public aid or charge upon the people shall be preceded by a resolution in committee. Further, it must be recommended by His Royal Highness the Governor General. I observe in looking over this Bill that, while it authorizes the purchase of a railway and provides the machinery, it makes no provision for the raising of funds. My mind goes back to a decision which was rendered by Mr. Speaker Sproule in the session of 1912, I think, on the question of the Highways Bill, which was an instance precisely in accord with this, if I mistake not. His decision at that time was that it was not a money Bill, inasmuch as any money to be expended would have to be provided either by Estimate or in some other manner authorised by Parliament.
Mr. GRAHAM: But he changed that decision.
Mr. DEPUTY SPEAKER: I think not. At all events, I think, having in view that decision, this cannot be regarded as a money Bill inasmuch as it does not provide for the expenditure of money, and the purchase cannot be completed until money is provided by Parliament by some constitutional means.
Mr. PUGSLEY:	In the Highway Bill
there was this difference, that the Bill provided that any moneys to be appropriated to the provinces should be voted bjr Parliament. But this Bill provides for the purchase of a railway, the price to be fixed by the Exchequer Court of Canada. That would authorize a positive agreement of purchase to be entered into, and if the Government should fail to carry out the agreement, I should think an action might be brought in the Exchequer Court and the price would be fixed by the Exchequer Court. The owner of the railway could recover the amount of the legal obligation. The Bill is clearly for the purpose of creating a charge. The Minister of Railways is authorized to make the agreement, the price to be fixed by the Exchequer Court. Then the Railway Company would commence suit in the Exchequer and would recover against the Crown the amount the Exchequer Court might fix as the value of their property.
Mr. DEPUTY SPEAKER: The Bill provides :	'
The Governor In Council may authorize and empower the Minister of Railways and Canals to acquire, under the provisions of an Act to amend the Government Railways Act, and to authorize the purchase of certain railways, chapter 16 of the Statutes, of 1916.
That is the so-called Branch Lines Act. This Bill, as I understand it, purports to bring the purchase or acquisition of this railway under the provisions of the Act of 1915, and if the objection of the hon. member for St. John (Mr. Pugsley) holds good in this case, it would hold good in the case of that Act, because that Act authorizes the purchase of certain branch lines of railways, but that purchase cannot be made complete until Parliament, by some constitutional means, afterwards provides the money. That, I submit places this Bill in exactly the same position as the Branch Lines Railway Bill, and for that reason as well as for the Teason previously indicated, without professing to have gone into the
matter exhaustively, I am quite clear in my own mind that the minister is in order in bringing in this Bill as it has been brought in, by notice on the Order Paper, rather than by resolution, as if it were a money Bill, or a Bill in respect of trade.
Mr. ROGERS: I was only putting the Bill forward to suit the convenience of the House in view of the fact that we all know that this legislation is coming forward, and I understood that hon. gentlemen opposite were anxious to know exactly what it was. I put it forward with a view to getting it printed at once, and we shall, of course, be glad to discuss it on further readings when it suits the 'convenience of hon. gentlemen opposite. There is no speoial haste; it is only because we are getting close to the end of the session and I wanted to give the hon. gentlemen opposite all the information the Bill contains.
Mr. PUGSLEY: It is very important that we should proceed correctly, because if this is a money Bill it cannot be amended in another branch of Parliament; if it is not it can be amended as the other branch may determine. Therefore it is very important .always to have the correct idea.
Mr. ROGERS: That is right. The Speaker has ruled.
Mr. A. K. MACLEAN: You had better be safe.
Motion agreed to and Bill read the first time.
- On motion of Mr. Rogers, the House adjourned at 11 .IS p.m.
