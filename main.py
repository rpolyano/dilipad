import os
from TransformerStage1 import TransformerStage1
from TransformerStage2 import TransformerStage2
from TransformerStage3 import TransformerStage3
from transformer_text_to_pm import Transformer as LegacyTransformerStage1
from transformer_text_to_pm import xmlTransformer as LegacyTransformerStage2
from transformer_text_to_pm import PMTransformer as LegacyTransformerStage3
if __name__ == "__main__":
    cwd = os.path.dirname(os.path.abspath(__file__))
    input_dir = cwd + "/Input"
    directory_st_1 = cwd + "/_stage_1" #TODO: Make if not exist
    directory_st_2 = cwd + "/_stage_2"
    directory_st_3 = cwd + "/_stage_3"

    for doc_name in os.listdir(input_dir): #TODO: Try walk
        if not doc_name.endswith(".txt") or doc_name.startswith("."):
            continue
        print doc_name
        
        doc = TransformerStage1(doc_name,input_dir,directory_st_1)
        doc.transform()

        doc = LegacyTransformerStage1(doc_name,input_dir,directory_st_1 + "_legacy")
        doc.preprocess_text_for_markup()
        doc.markup_entities()
        doc.markup_elements()

        doc_name = doc_name[:-3] + "xml"

        doc = TransformerStage2(doc_name, directory_st_1, directory_st_2)
        doc.transform()

        doc = LegacyTransformerStage2(doc_name, directory_st_1 + "_legacy", directory_st_2 + "_legacy");
        doc.transform()

        doc = TransformerStage3(doc_name, directory_st_2, directory_st_3)
        doc.transform()

        doc = LegacyTransformerStage3(doc_name, directory_st_2 + "_legacy", directory_st_3 + "_legacy");
        doc.transform()