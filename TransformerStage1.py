# -*- coding: utf-8 -*-
from __future__ import division
from Util import Util
import codecs
import os
import re
import math
from operator import itemgetter
from lxml import etree as Etree
from xml.sax.saxutils import escape
import traceback
import sys
from datetime import datetime as dt
from datetime import timedelta
from collections import Counter
from operator import itemgetter
from RegEx.Regex1 import Regex1 as ActiveRegEx
class TransformerStage1(object):
    """
    This class is used in the first stage of the conversion. It read a raw text document, codes the 
    most basic entities and elements and outputs a well-formed XML document.
    It takes as arguments the filename (f), input and output directory.
    """



    #Constants/Anchors
    ELEM_OTHER = [u"The Assistant Deputy Chairman:",
                               u"The Assistant Chairman:",
                               u"The Deputy Chairman:",
                               u"The Chairman:",
                               u"The Speaker:",
                               u"The Deputy Speaker:",
                               u"Madam Deputy Speaker:",
                               u"Madam Speaker:"]

    ELEM_MAIN_HEADERS = [u"Government Orders",
                             u"Statements by Members",
                             u"Oral Questions",
                             u"Routine Proceedings",
                             u"Oral Question Period",
                             u"Private Members' Business",
                             u"Adjournment Proceedings",
                             u"Statements by Members",
                             u"Statements Pursuant to",
                             u"Sitting Resumed"]

    ELEM_SUBTOPICS = [u"COMMITTEES OF THE HOUSE",
                          u"POINTS OF ORDER",
                          u"POINT OF ORDER",
                          u"WAYS AND MEANS",
                          u"PETITIONS",
                          u"QUESTIONS PASSED AS ORDERS FOR RETURNS",
                          u"QUESTIONS ON THE ORDER PAPER",
                          u"GOVERNMENT RESPONSE TO PETITIONS",
                          u"SUPPLEMENTARY ESTIMATES",
                          u"QUESTIONS ON THE ORDER PAPER",
                          u"PRESENCE IN GALLERY",
                          u"BUSINESS OF THE HOUSE",
                          u"PRIVILEGE",
                          u"GOVERNMENT RESPONSE TO PETITIONS",
                          u"MOTIONS FOR PAPERS",
                          u"THE BUDGET",
                          u"ORDER IN COUNCIL APPOINTMENTS",
                          u"SUPPLY",
                          u"INTERIM SUPPLY",
                          u"INTERPARLIAMENTARY DELEGATIONS"]

    ELEM_PRIMARY_STAGE_DIRECTIONS = [u"* * *", # May be interpreted as other weird characters (like diamonds or dots or
                                                #squares)
                                    u"(Motion",
                                    u"Prayers",
                                    u"Call in the members.",
                                    u"(Division",
                                    u"{Division", 
                                    u"(The House",
                                    u"{The House",
                                    u"YEAS",
                                    u"'YEAS",
                                    u"HOUSE OF COMMONS",
                                    u"PAIRED-MEMBERS",
                                    u"PAIRED MEMBERS",
                                    u"NAYS",
                                    u"SUSPENSION OF SITTING",
                                    #u"SITTING RESUMED",
                                    u"AFTER RECESS",
                                    u"The House met at",
                                    u"The House adjourned at"]

    ELEM_INTERVENTIONS = [u"Some hon. members:", #TODO: Abstract away case by making everything lower when looking for these
                                                    #things
                            u'Some hon. Members:', 
                            u"Some Hon. members:",
                            u"Some Hon. Members:",
                            u"A hon. member:",
                            u"A Hon. member:",
                            u"A Hon. Member:",
                            u"An Hon. Member:"]

    ELEM_VOTES = [u"(Division",u"(division"]

    def __init__(self, file, directory, output_directory):
        """
        Read the document.
        """
        self.text = Util.normalize_text(escape(codecs.open(os.path.join(directory, file),'r',encoding="utf-8").read()), "unicode")
        self.output = codecs.open(os.path.join(output_directory,file[:-4] + ".xml"),"w",encoding="utf-8")

    def transform(self):
        self.preprocess_text_for_markup()
        self.markup_entities()
        self.markup_elements()

    def preprocess_text_for_markup(self):
        """
        This method needs major editing.
        Here the text has to be prepared for markup, and it comprises multiple fixes that clean or structure the text data and enhance parsing.
        It glues lines together, puts important structural elements on a new line etc.
        See below for more detailed comments.
        I have deleted many elements, but it would be better to retain them and code them accordingly (timestamps, page number etc).
        """

        #Put elements indicating vote on new line
        for i in [u" YEAS",u" NAYS"]:
            self.text = self.text.replace(i, "\n" + i.strip())

        for i in [ur"\s\(Division"]:
            self.text = re.compile(i,re.UNICODE | re.MULTILINE).sub(ur"\n" + i.strip()[3:], self.text)

        #Glue hyphenated words
        self.text = re.compile(ur'\-\n(?=[a-z])',re.UNICODE | re.MULTILINE).sub(" ",self.text)

        self.text = re.compile(ur'\n(?=moved)',re.UNICODE | re.MULTILINE).sub(" ",self.text)
        self.text = re.compile(ur'\n.Division',re.UNICODE | re.MULTILINE).sub("\n(Division",self.text)

        #TODO: Put these regexes in constants

        #Delete standing order subtitles 
        self.text = re.compile(ActiveRegEx.PATTERN_STANDING_ORDER_SUBTITLES, re.M | re.UNICODE).sub("\n", self.text)

        #Delete stars
        self.text = re.compile(ur"\s\*\s\*\s\*\s",re.M | re.UNICODE).sub("\n",self.text)

        #Delete page numbers
        self.text = re.compile(ActiveRegEx.PATTERN_PAGE_NUMBERS,re.M | re.UNICODE).sub("\n", self.text)

        if ActiveRegEx.PATTERN_EXTRA_CLEAN != "":
            self.text = re.compile(ActiveRegEx.PATTERN_EXTRA_CLEAN, re.M | re.U).sub("\n", self.text)

        #FIXME: What is the point of this? Put hyphenated things on one line? 
        self.text =  re.compile(ur"\-\r?\n(?=[\sA-Za-z])", re.MULTILINE | re.UNICODE).sub(u"- ", self.text)

        #Remove time stamps
        #self.text = re.compile(ur'\s\[DOT\][0-9 ()\[\]{}]{3,10}\s', re.M | re.UNICODE).sub("\n", self.text);

        #Remove any OCR [DOT]s FIXME: Do this later
        #self.text = self.text.replace("[DOT]", "")

         #Removes MOST of the little headers at the tops of pages (grabs dates too) 
        #Explanation: second alternative looks for sentences with words like Word Word Other Word, first alternative looks for sentences ending with a 
        #lower case letter and the next sentence beginning with one like
        # some sentence that
        # Maybe a date here
        # Oral Questions
        # begins above the previous two lines and ends here
        #The second version is far less liekly to delete
        #self.text = re.compile(ur'((?:[a-z])\r?\n(?:[A-Za-z]{3,9}\s[1-9][1-9]?(?:,| )?[0-9]{4}\r?\n)?(?:(?:[A-Z][a-z\.0-9]+ ?)+[^\.]\r?\n)(?:[a-z]))|(\n(?:[A-Z][a-z\.0-9]+ ?)+[^\.]\r?\n)', re.MULTILINE | re.UNICODE).sub("\n", self.text);

        #Remove dates (at this point residual ones that weren't grabbed by the little headers regex above) at top of pages
        self.text = re.compile(ActiveRegEx.PATTERN_DATES, re.MULTILINE | re.UNICODE).sub("\n", self.text);

       



        for i in self.ELEM_MAIN_HEADERS:
            i = ur"\n{}\n".format(i)
            # -> make sure that chapter titles start on a new line
            pattern = re.compile(i, re.M | re.UNICODE)
            self.text = re.sub(pattern,"\n",self.text)
        
        self.text = self.text.replace(u"\nHOUSE OF COMMONS\n","\n")
        self.text = self.text.replace(u"MEASURE TO ENACT",u"Measure to Enact")
        self.text = self.text.replace(u"MEASURE TO AMEND",u"Measure to Amend")
        self.text = re.compile(ur"Mr\..?\n", re.M | re.UNICODE).sub(r"Mr. ", self.text) # TODO: Apply this to all Mr.  X and/or Madam X etc?
        self.text = self.text.split("\n") #FIXME: Seperate variable

        for i,line in enumerate(self.text):
            self.text[i] = line.strip();

    def markup_tables(self):
        in_table = False
        in_speech = False
        speach_speaker = ""
        self.text = self.text.split("\n")

        regex = re.compile(ActiveRegEx.PATTERN_TABLES, re.UNICODE)
        for i,line in enumerate(self.text):
            #if line.startswith("<speech>"):
            #    in_speech = True
            #    speach_speaker = self.text[i + 1]
            #elif line.endswith("</speech>"):
            #    in_speech = False
            #    speach_speaker = ""

            matches = regex.findall(line)
            if len(matches) > 0:
                matches = [re.compile(ActiveRegEx.PATTERN_XML_TAG).sub("", match).strip() for match in matches] #FIXME: RISKY
                tr = "<tr><td>" + "</td><td>".join(matches) + "</td></tr>\n"
                if not in_table:
                    #don't want one line or one/two column tables TODO: Check for column number consistancy
                    if i == len(self.text) - 1 or len(regex.findall(self.text[i + 1])) == 0 or len(matches) < 3:
                        continue
                    in_table = True
                    if in_speech:
                        self.text[i] = "</speech><table>" + tr
                    else:
                        self.text[i] = "<table>" + tr
                else:
                    self.text[i] = tr
            else:
                if in_table:
                    if in_speech:
                         self.text[i] = "</table>\n" + self.text[i] + "\n<speech>" + speach_speaker
                    else:
                        self.text[i] = "</table>\n" + self.text[i]
                    in_table = False

        self.text = "\n".join(self.text)

    def markup_entity_by_pattern(self, element_name='', pattern=None, ignore_capitals=False):
        """
        This method is integrated in "markup_entities".
        It codes strings conforming a regular expressions (defined in "pattern") with "element_name".
        """
        for i,line in enumerate(self.text):
            if ignore_capitals == True:
              results = re.compile(pattern,re.UNICODE | re.DOTALL | re.M | re.IGNORECASE).findall(line.strip())
              results = set(results)
            else:
              results = re.compile(pattern,re.UNICODE | re.DOTALL | re.M).findall(line.strip())
            for r in results:
                self.text[i] = line.replace(r,u"<{0}>{1}</{0}>".format(element_name,str(r).rstrip(":")))

    def markup_entity_by_pattern_with_subentities(self, tag_name, subentity_names, pattern):
        """
        Codes a string conforming to a pattern like markup_entity_by_pattern, but also codes
        every regex group as an element as well. subentity _text will not be wrapped in tags
        """
        text = ""
        repl = ""
        for i,subent in enumerate(subentity_names):
            if subent == "_text": 
                text += "\\" + str(i+1)
            else:
                repl += ur"<{0}>\{1}</{0}>\n".format(subent, str(i + 1))
        repl = "\n<" + tag_name + ">\n<p>" + text + "</p>\n" + repl + "</" + tag_name + ">\n" 
        text = u"\n".join(self.text)
        text = re.compile(pattern, re.UNICODE | re.MULTILINE).sub(repl, text)
        self.text = text.split("\n")
        



    def markup_entity_by_list(self,element_name="",elements_list=[],upper=False,whole_line=False):
        """
        Does the same as markup_entity_by_pattern, but needs a list of strings instead of a regular expressions as input.
        Use the "upper" attribute to uppercase all characters of a string (in case the OCR software wrongfully lowercased one character of a topic string).
        "whole_line": if True, this will code the whole input line, if the line starts with an element of the "elements_list".
        If whole_line=False, code only the element itself.
        """
        for i,line in enumerate(self.text):
            for element in elements_list:
                if upper == True:
                    element = element.upper()
                if self.text[i].startswith(element):
                    if whole_line == True:
                        self.text[i] = line.replace(self.text[i],u"<{0}>{1}</{0}>".format(element_name,self.text[i]))
                    else:
                        self.text[i] = line.replace(element,u"<{0}>{1}</{0}>".format(element_name,element))

    def markup_entities(self,exclude=""):
        """
        Code all entities such as person speaking and topic titles. 
        This information is used in the "markup_all_lines" method, to coded multiple lines as "speech" or non-speeche ("stage-direction)
        """
        self.markup_entity_by_pattern_with_subentities("stage-direction", ["question_num", "question_author", "_text"], ActiveRegEx.PATTERN_QUESTION)
        self.markup_entity_by_list('vote_heading', TransformerStage1.ELEM_VOTES, whole_line=True)
        self.markup_entity_by_pattern('speaker_entity',ActiveRegEx.PATTERN_INTERVENTION, ignore_capitals=True)
        self.markup_entity_by_pattern("speaker_entity",ActiveRegEx.PATTERN_MP_SPEAKER)
        self.markup_entity_by_pattern("time_stamp", ActiveRegEx.PATTERN_TIMESTAMP)
        
        ##self._markup_entity_by_pattern("speaker_entity",self.mp_speaker_pattern,print_output=False)
        self.markup_entity_by_pattern("speaker_entity",ActiveRegEx.PATTERN_OTHER_SPEAKER)
        self.markup_entity_by_list('stage-direction',TransformerStage1.ELEM_PRIMARY_STAGE_DIRECTIONS,whole_line=True)
        self.markup_entity_by_list('main_topic_title',TransformerStage1.ELEM_MAIN_HEADERS,upper=True,whole_line=True)
        
        ##self._markup_entity_by_list('intermediate_topic_title',self.subtopics)
        
        self.markup_entity_by_pattern('topic_title',ActiveRegEx.PATTERN_TOPIC)

    def markup_element_by_pattern(self,pattern,element_name,strip_text=False):
        """
        Does essentially the same as markup_entities_by_pattern, except that here multiple lines are marked up.
        Method is integrated in "markup_elements" (below)
        """
        elements = re.compile(pattern,re.UNICODE | re.DOTALL).findall(self.text)
        for el in list(set(elements)):
          if strip_text == True:
            self.text = self.text.replace(el,u"<{0}>{1}</{0}>".format(element_name,el.strip()))
          else:
            self.text = self.text.replace(el,u"<{0}>{1}</{0}>".format(element_name,el))

    def markup_elements(self):
        """
        code the whole document according to the key elements of the PM schema 
        -> vote_element, speech, topic and scene

        important here is "self.markup_all_lines()" which is explained below
        """
        self.text = self.set_first_topic_to_main()
        self.text = u"\n".join(self.text)
        ##self._markup_element_by_pattern(ur"<intervention_element>.+?(?:(?=<speaker_entity>|<stage-direction>|<topic_title>|<main_topic_title>|<intermediate_topic_title>|<intervention_element>|<vote_heading>|<stage-direction>|$))","intervention",strip_text=True)
        self.markup_element_by_pattern(ur"<vote_heading>.+?(?:(?=<speaker_entity>|<topic_title>|<main_topic_title>|<intermediate_topic_title>|<intervention_element>|$))","vote_element",strip_text=True)
        self.markup_element_by_pattern(ur"<speaker_entity>.+?(?:(?=<speaker_entity>|<stage-direction>|<topic_title>|<main_topic_title>|<intermediate_topic_title>|<vote_element>|$))","speech")
        self.markup_element_by_pattern(ur"<main_topic_title>.+?(?:(?=<main_topic_title>|$))","topic")
        
        ##self._markup_element_by_pattern(ur"<vote_element>.+?(?:(?=<intermediate_topic_title>|<topic>|</topic>|$))","subtopic")
        ##self._markup_element_by_pattern(ur"<topic_title>[^<>]+?(?:</topic_title>)[^<>]+(?:<speech>).+?(?:(?=<topic_title>|<subtopic>|</topic>|</subtopic>|</topic>))","scene")
        
        self.markup_all_lines()
        self.text = "\n".join(self.text)
        self.text = self.text.replace(u'>\n<',"><")
        self.markup_element_by_pattern(ur"<topic_title>[^<>]+?(?:</topic_title>)[^<>]*?(?:(?:<speech>)|(?:<stage-direction>)).+?(?:(?=<topic_title>|<subtopic>|</topic>|</subtopic>|</topic>|$))","scene")
        
        self.output.write(u"<proceedings>{}</proceedings>".format(self.text))
        self.output.close()


        

    def markup_all_lines(self):
        """
        This methods reads a text line by line and codes each as either speech or non-speech (stage-direction) based on the location in the existing structure.
        Also codes paragraphs. It makes sure (or should at least) that ALL text are enclosed in an XML node.
        """
        regex_split_by_tags = re.compile(ur'><',re.UNICODE | re.DOTALL)

        self.text = regex_split_by_tags.sub(ur'>\n<',self.text)
        self.markup_tables()
        self.text = regex_split_by_tags.sub(ur'>\n<',self.text)
        self.text = self.text.split("\n")
        
        in_speech = False
        for i,line in enumerate(self.text):
            line = line.strip()
            # -> traverse document differentatiating between <speech> and
            # <stage-direction>
            if line.startswith("<speech>"):
                in_speech = True
            elif line.endswith("</speech>"):
                in_speech = False

            if in_speech == True and not line.startswith(ur'<') and not line.endswith(ur">"):
                self.text[i] = line.replace(line,ur"<p>{}</p>".format(line.strip()))
            elif in_speech == True and not line.startswith('<') and line.endswith(ur"</speech>"):
                sent = line.split(ur'</speech>')[0]
                sent = ur"<p>{}</p>{}".format(sent,ur"</speech>")
                self.text[i] = sent
            elif in_speech == True and '</speaker_entity>' in line:
                line = line.split("</speaker_entity>")
                p = line[1].replace(line[1],ur"<p>{}</p>".format(line[1].strip()))
                self.text[i] = "</speaker_entity>".join([line[0],p])
            elif in_speech == False and not line.startswith(ur'<') and not line.endswith(ur">"):
                self.text[i] = line.replace(line,ur"<stage-direction><p>{}</p></stage-direction>".format(line.strip()))
            elif in_speech == False and not line.startswith('<') and line.endswith("</scene>"):
                sent = line.split(ur'</scene>')[0]
                sent = ur"<stage-direction><p>{}</p></stage-direction>{}".format(sent,ur"</scene>")
                self.text[i] = sent
        
        

    def set_first_topic_to_main(self):
        """
        Be sure that the first topic element is properly coded
        """
        before_chapter = True
        topic_changed = False

        for i,line in enumerate(self.text):
            if ur"<topic_title>" in line and before_chapter == True and topic_changed == False:
                self.text[i] = line.replace(ur"<topic_title>",ur"<main_topic_title>").replace(ur"</topic_title>",ur"</main_topic_title>")
                break
            elif '<main_topic_title>' in line:
                before_chapter = False
        return self.text