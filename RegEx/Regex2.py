class Regex2(object):
    """Second variation of regexes for parsing Canadian Parl Data
       Tested on 1915
    """
    
    #Patterns
    PATTERN_TIMESTAMP = ur"\s?\[DOT\][0-9 HIl()\[\]{}]{3,10}\s?"
    PATTERN_TOPIC = ur"(?<=\n)([A-Z0-9\. \,\-]+[^\.a-z])(?:\.?)(?=\n)"
    PATTERN_MP_SPEAKER = ur"^((?:Sir|M\.|Mr\.|Mr\,|Hon\.|The\sHon\.|Right\sHon\.|The\sRight\sHon\.|Miss|Mrs\.|Ms\.)\s(?:[A-Zdv][\-\w\.']{1,25}\s{0,1}){1,4}\s{0,1}(?:\(.+?\)){0,1}\s{0,1}(?:(?:moved:)|(?:moved)|:|;|\.))"
    PATTERN_OTHER_SPEAKER = ur'^((?:The|Madam)(?:\sActing)?(?:\sAssistant)?(?:\sDeputy)?\s(?:Speaker)?(?:Chairman)?(?:\s\(.{0,50}?\)){0,1}?(?:;|:))'
    PATTERN_INTERVENTION = ur"(?:some|a) hon. member.{0,1}:"
    PATTERN_QUESTION = ur'(?:.uestion\sN..\s([0-9ilI\,\|]{1,})\-)([^:]*):\r?\n([^\?\.]+(?:\?|\.))'
    PATTERN_STANDING_ORDER_SUBTITLES = ur"\n[A-Z0-9]{1}'{0,1}\.{0,1}\s{0,1}O\.{0,1}\s{0,1}[0-9]{1,4}\r?\n"
    PATTERN_PAGE_NUMBERS = ur"\n[0-9\-]{1,10}\r?\n"
    PATTERN_DATES = ur'(\n[A-Za-z]{3,9}\s[IXV1l]{1,6}, ?[0-9]{4})'
    PATTERN_TABLES = ur""
    PATTERN_XML_TAG = ur"(<\/?[^>]+>)"
    PATTERN_EXTRA_CLEAN = ur"\[(?:[^ ]+ ?){2,5}\.?\]"


