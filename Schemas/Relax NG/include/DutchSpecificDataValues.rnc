datatypes xsd = "http://www.w3.org/2001/XMLSchema-datatypes"
namespace pm = "http://www.politicalmashup.nl"
 
DutchDocEntityRelationType = (
"questioner" |   # steller van een kamervraag
"answerer"|      # beantwoorder van een kamervraag
"submitter"|     # indiener van iets
"addressee"|     # ontvanger van iets (een brief bijvoorbeeld)
"sender"|        # verzender van iets (brief)
"speaker"|       # spreker in een debate
"party"          # partij in de kamer (die deelneemt aan stemmingen)
)

# After each DutchDocumentType the unique identifier as defined in the XML from officielebekendmakingen.nl is shown                   
DutchDocumentType = (
"memorie van toelichting"|              # /kamerwrk[@publtype="meto"]
"brief"|                                # /kamerwrk[@publtype="brif"]
"amendement"|                           # /kamerwrk[@publtype="amen"]
"motie"|                                # /kamerwrk[@publtype="moti"]
"verslag"|                              # /kamerwrk[@publtype="moov"]
"memorie van antwoord"|                 # /kamerwrk[@publtype="mean"]
"kamervragen zonder antwoord" |         # /kamerwrk[@publtype="vrlo"]
"nota van verbetering"|                 # /kamerwrk[@publtype="novv"]
"rapport" |                             # /kamer`wrk[@publtype="rapport"]
"notitie"|
"nota"|
"mededeling"|
"nota  naar aanleiding van het verslag"|
"nota naar aanleiding van het eindverslag"|
"lijst van antwoorden"|
"verslag van een mondeling overleg"|
"verslag van een algemeen overleg"|
"verslag van een wetgevingsoverleg"|
"verslag van een notaoverleg"|
"verslag van een hoorzitting"|
"verslag van een rondetafelgesprek"|
"verslag van een schriftelijk overleg"|
"verslag van een werkbezoek"|
"voorlopig verslag"|
"eindverslag"|
"lijst van vragen"|
"lijst van antwoorden"|
"lijst van vragen en antwoorden"|
"Koninklijke boodschap"|
"voorstel van wet"|
"nota van wijziging"|
"advies Raad van State"|
"nader rapport"|
"kamervraag" |
"Dossier"
)

    # type vote
    # see http://www.tweedekamer.nl/hoe_werkt_het/de_tweede_kamer_vergadert/stemmingen/index.jsp
    # - fractie: the voorzitter assumes that the members of a party vote for the whole party
    # - hoofdelijk: each individual member of a party votes verbally
    # - schriftelijk: voting in writing, this only occurs when a person is nominated or chosen, ie the voorzitter
    # - zonder stemming: the outcome of the vote is determined without a specific voting session. i.e. a changed amendment outlaws a previous one
    # - unaniem: 
    # - van agenda afgevoerd: no stemming available, the voting session is postponed
    # - ingetrokken: no stemming available, subject is retracted
    # - bommelding: calamiteiten like a bomb threat interrupt the voting session
    # - other: when we know that we don't know what the type of voting session is
    # - (empty): when we know that we should know what the type of voting session is, but we have to fix it in the transformer. This option will not validate.

DutchVoteType = ('fractie'|'hoofdelijk'|'schriftelijk'|'zonder stemming'|'unaniem'|'van agenda afgevoerd'|'ingetrokken'|'bommelding'|'other')
DutchVotedOn = ('wetsvoorstel'|'voorstel van orde'|'motie'|'amendement'|'brief'|'unknown')

DutchIdentifier = (element pm:dossiernr {attribute pm:title {text}?,
                                         text}* #Vote-problems caused: + to *: dossiernr not required anymore, pick up no-dossiernrs with schematron.
                   & DocRef?
                   & element pm:rijkswetnr {text}? # Rijkswetnummer is a 'R1234' style separate number reference for legislating pertaining to the entire Dutch Kingdom (each rijkswet always matches one unique dossiernumber).
                   & element pm:sdukey {text}? 
                   & element pm:ondernr {text}* #Vote-problems caused: ? to *: multiple ondernr's possible, pick up no-dossiernrs with schematron.
                   & element pm:deelnr {text}? 
                   & element pm:questionnr {text}? 
                   & element pm:ordernr {text}?
                   )
DutchVoteInfo =
    element pm:dossiernummer {text}* #Vote-problems caused: + to *: dossiernr not required anymore, pick up no-dossiernrs with schematron.
    & element pm:rijkswetnummer {text}* # Rijkswetnummer is a 'R1234' style separate number reference.
    & element pm:stuknummer {text}* 
    & element pm:ondernummer {text}* 
    & element pm:part {text}*  # e.g. I or III
    & element pm:submitted-by { Actor*  }
    & element pm:introduction { text? }? # Actual introductory text that cause the vote detection.
    & element pm:outcome { text? }? # Actual text wherein the outcome is discussed. 
    
 
