namespace dc = "http://purl.org/dc/elements/1.1/"
namespace dcterms = "http://purl.org/dc/terms/"
namespace pm = "http://www.politicalmashup.nl"
namespace pmx = "http://www.politicalmashup.nl/extra"
namespace xsi = "http://www.w3.org/2001/XMLSchema-instance"

PoliticalMashupUri = "http://www.politicalmashup.nl"
include "toolbox.rnc"

# The meta element is the second child of root. It contains meta data following the Dublin Core standard. Its content can be divided in three categories:
#
# 1. Meta data which is descriptive of the collection of documents this specific document is part of. This data is the same for all documents in this collection.
#    The transformers for the PoliticalMashup documents get this data from transformers/country-descriptions.xml
#
# 2. Meta data which is decriptive of the content of the document. This data is retrieved from the source document
#
# 3. Meta data which is descriptive of the formatting of the document. This is only dc:format, which is always "text/xml"

start = Meta

Meta =
    element meta {
        Id? & # Data should contain [identifier].meta
        DcContributor &  
        DcCoverage & 
        DcCreator &  
        DcLanguage &
        DcPublisher &
        DcRights &
        DcDate &
        DcDescription &
        DcIdentifier &
        DcRelation &  
        DcSource & 
        DcSubject &
        DcTitle &
        DcType &
        DcFormat 

    }

# 1. Meta data descriptive of the document collection

# http://dublincore.org/documents/dces/#contributor
#
# The URI of the organization that formatted the document. For all PoliticalMashup documents: http://www.politicalmashup.nl

DcContributor = element dc:contributor { PoliticalMashupUri }

# http://dublincore.org/documents/dces/#coverage
#
# The geographical applicability of the document
#
# For dutch proceedings: <dc:coverage><country dcterms:ISO3166-1="NL">Netherlands</country></dc:coverage>

DcCoverage = element dc:coverage {  
             mixed { external "country.rnc"? }
         }+

# http://dublincore.org/documents/dces/#creator
#
# The URI of the organization that created the content of the document.
#
# For Dutch proceedings 1930-1995: http://www.statengeneraaldigitaal.nl
# For Dutch proceedings 1995-current: Tweede Kamer der Staten-Generaal
# For Dutch verenigde vergadering: Verenigde Vergadering der Staten-Generaal
# For Dutch eerste kamer: http://www.politicalmashup.nl ???
# For Dutch political parties: http://www.rug.nl/dnpp
# For Dutch politicians: http://www.parlement.com

DcCreator =  element dc:creator { PoliticalMashupUri | text }+  # constant "http://www.politicalmashup.nl"

# http://dublincore.org/documents/dces/#language
#
# The language(s) used within the document, specified within pm:language elements
#
# For Dutch proceedings: <dc:language><pm:language dcterms:ISO639-2="dut">Dutch; Flemish</pm:language></dc:language>

DcLanguage = element dc:language { LanguageElement+ }

# http://dublincore.org/documents/dces/#publisher
#
# The organisation responsible for making the document available
#
# Should be the base URL (only domain information, e.g. "http://abc.somewebsite.nl/") where the source documents were retrieved.
# N.B. these URL's are known to change, so they need not necessarily reflect the current URL. 
# Dutch proceedings after 1995: https://zoek.officielebekendmakingen.nl
# Dutch proceedings before 1995: http://www.statengeneraaldigitaal.nl
# Dutch politicians: http://www.politicalmashup.nl
# Dutch political parties: http://www.politicalmashup.nl

DcPublisher =  element dc:publisher { text }

# http://dublincore.org/documents/dces/#rights
#
# IPR information pertaining to the document

DcRights = element dc:rights { text } 


# 2. Meta data descriptive of the content of the document

# http://dublincore.org/documents/dces/#date
#
# The meaning of dc:date depends on the context:
# For parliamentary proceedings: the day the meeting was held
# For political parties: the day the party was founded; //pm:party/pm:history/pm:formation/@pm:date
# For politicians: the first membership day; //pm:member/pm:memberships/pm:membership[1]/pm:period/@pm:from

DcDate =  element dc:date {
             (xsd:date # yyyy-mm-dd
             | 
               (element pm:parliamentary-year { xsd:string { pattern = "\d{4}/\d{4}" }},
                element pm:start-year { xsd:string { pattern = "\d{4}" }},
                element pm:end-year { xsd:string { pattern = "\d{4}" }},
                element pm:parliamentary-year-fulltext {text}?)
             | (element pm:date-question { xsd:date } &
                element pm:date-response { xsd:date }? &
                element pm:response-duration { xsd:duration | xsd:integer }?) # number of days
             | 
               empty),
               attribute pm:meeting-start-time {xsd:dateTime}?,
               attribute pm:meeting-end-time {xsd:dateTime}?
         }

# http://dublincore.org/documents/dces/#description
# 
# A descriptive text for the document
# For proceedings, often the title of the document:
# Example: 'Handelingen Tweede Kamer OCV / UCV 1990-1991 04 februari 1991, Paginas 1-34'
# For political parties: 'Biographic information of a political party.'
# For politicians: 'Biographic information of a politician.'

DcDescription =  element dc:description { text | empty }

# http://dublincore.org/documents/dces/#identifier
# 
# The unique resolvable identifier of the document
# 
# For PoliticalMashup documents: always of the format [nl|be|de|dk|se|no].([a-zA-Z]+\.)*[d|p|m].[a-zA-Z0-9]+
# The string before the second-to-last dot identifies the collection. The first letters are a country code
# The string after the last dot identifies the document within the collection
# The letter before the last dot identifies the type of document:
# d: proceedings
# p: political party
# m: politician (member of parliament)
# Examples: nl.proc.sgd.d.199019910000901, nl.p.pvv, nl.m.02207
# Question: what is xsi:type and IdentifierType used for?

DcIdentifier = 
        element dc:identifier { attribute xsi:type  { IdentifierType }?,
                                 attribute id {xsd:anyURI}?,
                                 attribute pm:source {xsd:anyURI}?, # source-identifier and source mention used if we know an explicitly used 'foreign' identifier
                                 attribute pm:source-identifier {xsd:anyURI}?,
#            (text | DutchIdentifier )
            (text) # Removed DutchIdentifier, no elements in dc:identifier are used anymore
            }

IdentifierType = xsd:string  # pattern = "pm:nl-dossiernr-ondernr"



# http://dublincore.org/documents/dces/#source
# 
# A list of all sources of information used in this document 
# 
# dc:source elements all embedded in one dc:source. 
#
# Sources of information can be referenced in one of two ways: 
# 1. Directly as Link to an external information source
# 2. As a reference to a transformer step described in DocInfo. 
#    Specifiy with pm:method whether the data added in this transformer step was collected in an automated way or manually (i.e. scraped from internet or put together by a human)   
#
# Explain in a pm:comment element how the source was used to create the document
# Try to list all sources. If there are multiple sources, mention the actual used source for transformation with pm:used-source="true"

DcSource = element dc:source {
             element dc:source {
                (Link | DocInfoRef)
                & attribute pm:used-source { xsd:boolean }?  
                & Comment*}*  
         }

DocInfoRef =  (attribute pm:transformer-ref{text} & attribute pm:method { MethodType })

# http://dublincore.org/documents/dces/#subject
#
# The topic of the document
#
# For proceedings, this contains pm elements with detailed information to classify the document

DcSubject = element dc:subject {(
             element pm:house { House, text }* & # 'text' is the country specific name, e.g. <pm:house pm:house="senate">Eerste Kamer der ..</pm:house>; A document might be a collaboration, having two houses (eerste + tweede, not "verenigde", hence the *
             element pm:sub-type { SubType, text }? & # 'text' is the document specific text, SubType the matchable attribute (cf. <pm:house/@pm:house>text..<
             element pm:legislative-period { text }?& # e.g. the dutch vergaderjaren: "2008-2009", or "BZ 1995"
             element pm:session-number { text }?& # Used for (chronologically) numbered meetings; often available.
             element pm:item-number { text }?& # Used for (chronologically) numbered sub-sections (items) in meetings; sometimes available.
             element pm:keywords  { Entity* }?& # only if mentioned in some meta-data
             element pm:categories { Entity* }?& # only if mentioned in some meta-data
             element dcterms:abstract { mixed { Entity*  }  }? & # only if mentioned in some meta-data
             element pm:publication-date {xsd:date}? & # only if a publication date (other than meeting date) is specifically mentioned
             element pm:dossier {DutchIdentifier}?  # dossier information of the actual dossier piece described (cf. dossier in dc:relation, which could for instance be the encompassing dossier)
             & DcSubjectPmxContent
             )
         }
# Add to extra content below in importing definition.
DcSubjectPmxContent = empty

# http://dublincore.org/documents/dces/#title
#
# Title of the document
# 
# For Dutch political parties: 'Biographic information of: $PartyName', where $PartyName = //pm:party/pm:name/text()
#                               example: 'Biographic information of: Partij voor de Vrijheid'
# For Dutch politicians: 'Biographic information of: $MemberName', where $MemberName = //pm:member/pm:name/pm:full/text()
#                               example: 'Biographic information of: Prof.Mr.dr. J.P. (Jan Peter) Balkenende'

DcTitle = element dc:title { text } 

# http://dublincore.org/documents/dces/#type
#
# Type of document. Options are:
# Verbatim Proceedings: For all parliamentary meeting notes
# Parliamentary Documents: Parliamentery documents not covered by any of the other catogeries. Use xsi:type attribute to specify further (e.g. amendement, etc).
# Written Questions: "kamervragen" as handed over to politicians for review.
# Policital Parties: Decription of a political party
# Members: Description of a politician/member of parliament
# Verbatim Proceedings: all parliamentary meeting notes (house proceedings, commision meetings etc.). Use this in most cases.
# Debate Summary: A summary of a debate

DcType=  element dc:type {
             attribute xsi:type {DutchDocumentType}?, DocumentType}

# 3. Meta data which is descriptive of the formatting of the document. This is only dc:format, which is always "text/xml"

# http://dublincore.org/documents/dces/#format
#
# The format of the document. Always text/xml

DcFormat = element dc:format { FormatType }
