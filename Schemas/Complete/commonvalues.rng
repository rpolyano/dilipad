<?xml version="1.0" encoding="UTF-8"?>
<grammar xmlns="http://relaxng.org/ns/structure/1.0">
  <!-- commonvalues.rnc contains data types used to restrict attribute or element values -->
  <!-- names.rnc -->
  <define name="NameType">
    <choice>
      <value>long</value>
      <!-- name written out -->
      <value>short</value>
      <!-- acronym -->
      <value>callingname</value>
      <!-- callingname -->
      <value>ocr</value>
      <!-- ocr spelling variations -->
      <value>normalised</value>
      <!-- normalised form (lowercase, no spaces, dashes etc, for name matching) -->
      <value>marriage</value>
      <value>spelling</value>
      <value>source-error</value>
    </choice>
  </define>
  <define name="HonorificType">
    <choice>
      <value>academic</value>
      <value>honorary</value>
      <value>professional</value>
      <value>religious</value>
      <value>generational</value>
      <value>other</value>
    </choice>
  </define>
  <define name="PositionType">
    <choice>
      <value>prefix</value>
      <value>suffix</value>
    </choice>
  </define>
  <!-- member.rnc -->
  <define name="GenderType">
    <choice>
      <value>male</value>
      <value>female</value>
      <value>transgender</value>
    </choice>
  </define>
  <!-- links.rnc -->
  <define name="LinkType">
    <choice>
      <value>wikipedia</value>
      <value>dbpedia</value>
      <value>party</value>
      <value>member</value>
      <value>document</value>
      <value>trusted</value>
      <value>photo</value>
      <value>html</value>
      <value>pdf</value>
      <value>xml</value>
      <value>html-frames</value>
      <value>metadata</value>
      <value>rss</value>
      <value>twitter</value>
      <value>youtube</value>
      <value>linkedin</value>
      <value>blog</value>
      <value>flickr</value>
      <value>logo</value>
      <value>other</value>
    </choice>
  </define>
  <!-- proceedings.rnc -->
  <define name="TopicType">
    <value>topic</value>
  </define>
  <define name="SpeakerType">
    <value>speaker</value>
  </define>
  <!-- datetime.rnc -->
  <define name="PresentType">
    <value>present</value>
  </define>
  <!-- docinfo.rnc -->
  <define name="ResultType">
    <choice>
      <value>pass</value>
      <value>fail</value>
      <value>test</value>
    </choice>
  </define>
  <!-- mediaattributes.rnc -->
  <define name="MediaType">
    <choice>
      <value>audio</value>
      <value>video</value>
      <value>other</value>
    </choice>
  </define>
  <!-- meta.rnc -->
  <define name="MethodType">
    <choice>
      <value>manual</value>
      <value>script</value>
    </choice>
  </define>
  <define name="DocumentType">
    <choice>
      <value>Verbatim Proceedings</value>
      <value>Parliamentary Documents</value>
      <value>Written Questions</value>
      <value>Political Parties</value>
      <value>Members</value>
      <value>Debate Summary</value>
    </choice>
  </define>
  <define name="DocumentSubType">
    <choice>
      <value>amendment</value>
      <value>motion</value>
      <value>other</value>
    </choice>
  </define>
  <define name="FormatType">
    <value>text/xml</value>
  </define>
  <!-- politicalattributes.rnc -->
  <define name="HouseType">
    <choice>
      <value>commons</value>
      <value>senate</value>
      <value>other</value>
    </choice>
  </define>
  <define name="BodyType">
    <choice>
      <value>commons</value>
      <value>senate</value>
      <value>government</value>
      <value>other</value>
    </choice>
  </define>
  <define name="RoleType">
    <choice>
      <value>mp</value>
      <value>chair</value>
      <value>government</value>
      <value>other</value>
    </choice>
  </define>
  <define name="NotAvailableType">
    <value>not-available</value>
  </define>
  <!-- relation.rnc -->
  <define name="VoteType">
    <value>vote</value>
  </define>
  <!-- speech.rnc -->
  <define name="InterruptionType">
    <value>interruption</value>
  </define>
  <!-- stagedirection.rnc -->
  <define name="StatusType">
    <choice>
      <value>present</value>
      <value>absent</value>
      <value>other</value>
    </choice>
  </define>
  <!-- tagged.rnc -->
  <define name="TaggedTypeType">
    <choice>
      <value>named-entity</value>
      <value>date</value>
      <value>reference</value>
      <value>document-structure</value>
      <value>quote</value>
    </choice>
  </define>
  <!-- vote.rnc -->
  <define name="OutcomeType">
    <choice>
      <value>accepted</value>
      <value>rejected</value>
      <value>draw</value>
      <value>not-available</value>
    </choice>
  </define>
  <define name="TransformerProblemType">
    <value>transformer-problem</value>
  </define>
  <!-- parliamentary-documents.rnc -->
  <define name="HeaderBlockType">
    <value>header</value>
  </define>
  <define name="TitleBlockType">
    <value>title</value>
  </define>
  <define name="ExplanationBlockType">
    <value>explanation</value>
  </define>
  <define name="SignedByBlockType">
    <value>signed-by</value>
  </define>
  <define name="AppendixBlockType">
    <value>appendix</value>
  </define>
  <define name="ContentBlockType">
    <value>content</value>
  </define>
  <define name="SectionBlockType">
    <value>section</value>
  </define>
</grammar>
