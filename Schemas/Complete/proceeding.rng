<?xml version="1.0" encoding="UTF-8"?>
<grammar xmlns:pm="http://www.politicalmashup.nl" xmlns="http://relaxng.org/ns/structure/1.0">
  <include href="toolbox.rng"/>
  <include href="pmstructure.rng"/>
  <!--
    Schema for parliamentary proceedings.
    
    Proceedings consist of one or more Topics, which are usually the different points on the agenda.
    
    Topics may be subdivided in Scenes. A Scene is when a speaker stands in front of the speaker chair. This main speaker is identified with a MemberRef in Scene.
    
    Whenever a new person walks up to the speaker chair, a new Scene is created.
    A Scene (or if there is no such subdivision, a Topic) is subdivided in Speech elements. 
    
    Whenever there is a different speaker (for example with interruptions), there is a new Speech element.
    
    A Scene must at least have one Speech by the speaker identified in Scene.
    
    All meta data (everything outside the spoken word) that can be extracted from the source document is contained in StageDirections. 
    A StageDirection may contain for instance a Pagebreak, a change in chairman (Chair), the results of a Vote, or a reference to a parliamentary document under discussion (Paper).
    
    For UK data, a Scene represents not the time a speaker is in front of the speaker chair, but is used as a subdivision of topics in subtopics.
    To mark the difference, attribute pm:type in Scene is set to 'topic' instead of 'speaker' for the conventional use of Scene.
    
    All spoken text is contained in Paragraph (p) elements. 
  -->
  <define name="Content">
    <ref name="Proceedings"/>
  </define>
  <define name="Proceedings">
    <element name="pm:proceedings">
      <ref name="ProceedingsContent"/>
    </element>
  </define>
  <define name="ProceedingsContent">
    <interleave>
      <ref name="Id"/>
      <optional>
        <ref name="DcRelation"/>
      </optional>
      <!-- If there is for some reason another proceedings document covering the same meeting, then declare that relationship in an owl:sameAs element here -->
      <ref name="CommonAttributes"/>
      <oneOrMore>
        <ref name="Topic"/>
      </oneOrMore>
    </interleave>
  </define>
  <define name="Topic">
    <element name="pm:topic">
      <ref name="TopicContent"/>
    </element>
  </define>
  <define name="TopicContent">
    <interleave>
      <ref name="Id"/>
      <ref name="CommonAttributes"/>
      <optional>
        <ref name="Title"/>
      </optional>
      <optional>
        <ref name="Type"/>
      </optional>
      <choice>
        <zeroOrMore>
          <choice>
            <ref name="Scene"/>
            <ref name="StageDirection"/>
          </choice>
        </zeroOrMore>
        <zeroOrMore>
          <choice>
            <ref name="Speech"/>
            <ref name="StageDirection"/>
          </choice>
        </zeroOrMore>
      </choice>
    </interleave>
  </define>
  <define name="Scene">
    <element name="pm:scene">
      <ref name="SceneContent"/>
    </element>
  </define>
  <define name="SceneContent">
    <interleave>
      <ref name="Id"/>
      <ref name="CommonAttributes"/>
      <optional>
        <ref name="Title"/>
      </optional>
      <choice>
        <interleave>
          <attribute name="pm:type">
            <ref name="SpeakerType"/>
          </attribute>
          <ref name="SpeakerAttributes"/>
        </interleave>
        <attribute name="pm:type">
          <ref name="TopicType"/>
        </attribute>
      </choice>
      <zeroOrMore>
        <choice>
          <ref name="Speech"/>
          <ref name="StageDirection"/>
        </choice>
      </zeroOrMore>
    </interleave>
  </define>
</grammar>
<!--
  type =speaker: when topics are partioned into speeches by speakers as in NL and NO. 
  type =topic: when a topic is subdivided into subtopics (as in UK)     
  if type ='speaker' or no type at all, then Speakerattributes
-->
